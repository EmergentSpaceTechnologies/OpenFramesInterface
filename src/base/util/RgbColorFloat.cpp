//$Id: RgbColorFloat.cpp mruschmann $
//------------------------------------------------------------------------------
//                                  RgbColorFloat
//------------------------------------------------------------------------------
// OpenFramesInterface Plugin for GMAT (General Mission Analysis Tool)
//
// Copyright (c) 2022 Emergent Space Technologies, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Developed by Emergent Space Technologies, Inc. under contract number
// NNX16CG16C
//
// Author: Matthew Ruschmann, Emergent Space Technologies, Inc.
// Created: August 21, 2017
/**
 *  RgbColorFloat class
 *  Sits on top of GMAT's RgbColor class to provide floating point output in the
 *  range of zero to one.
 */
//------------------------------------------------------------------------------

#include "RgbColorFloat.hpp"


//---------------------------------
// public methods
//---------------------------------

//------------------------------------------------------------------------------
// RgbColorFloat()
//------------------------------------------------------------------------------
RgbColorFloat::RgbColorFloat() :
   RgbColor()
{
   // nothing to do here, all performed in base constructors
}

//------------------------------------------------------------------------------
// RgbColorFloat(const Byte red, const Byte green, const Byte blue, const Byte alpha = 0)
//------------------------------------------------------------------------------
RgbColorFloat::RgbColorFloat(const Byte red, const Byte green, const Byte blue, const Byte alpha) :
   RgbColor(red, green, blue, alpha)
{
   // nothing to do here, all performed in base constructors
}

//------------------------------------------------------------------------------
// RgbColorFloat(const UnsignedInt intColor)
//------------------------------------------------------------------------------
RgbColorFloat::RgbColorFloat(const UnsignedInt intColor) :
   RgbColor(intColor)
{
   // nothing to do here, all performed in base constructors
}

//------------------------------------------------------------------------------
// RgbColor(const RgbColor &rgbColor)
//------------------------------------------------------------------------------
RgbColorFloat::RgbColorFloat(const RgbColorFloat &RgbColorFloat) :
   RgbColor(RgbColorFloat)
{
   // nothing to do here, all performed in base constructors
}

//------------------------------------------------------------------------------
// RgbColorFloat& operator=(const RgbColor &rgbColor)
//------------------------------------------------------------------------------
RgbColorFloat& RgbColorFloat::operator=(const RgbColorFloat &RgbColorFloat)
{
   RgbColor::operator=(RgbColorFloat);
   return *this;
}

//------------------------------------------------------------------------------
// virtual ~RgbColorFloat()
//------------------------------------------------------------------------------
RgbColorFloat::~RgbColorFloat()
{
}


//------------------------------------------------------------------------------
// float RedFloat()
//------------------------------------------------------------------------------
float RgbColorFloat::RedFloat()
{
   return Integer255ToFloat1(Red());
}

//------------------------------------------------------------------------------
// float GreenFloat()
//------------------------------------------------------------------------------
float RgbColorFloat::GreenFloat()
{
   return Integer255ToFloat1(Green());
}

//------------------------------------------------------------------------------
// float BlueFloat()
//------------------------------------------------------------------------------
float RgbColorFloat::BlueFloat()
{
   return Integer255ToFloat1(Blue());
}

//------------------------------------------------------------------------------
// float AlphaFloat()
//------------------------------------------------------------------------------
float RgbColorFloat::AlphaFloat()
{
   return Integer255ToFloat1(Alpha());
}


//------------------------------------------------------------------------------
// float Integer255ToFloat1(Integer input)
//------------------------------------------------------------------------------
float RgbColorFloat::Integer255ToFloat1(Integer input)
{
   return (static_cast<float>(input) / 255.0);
}
