//$Id: GmatPluginFunctions.cpp 9460 2011-04-21 22:03:28Z ravimathur $
//------------------------------------------------------------------------------
//                            GmatPluginFunctions
//------------------------------------------------------------------------------
// OpenFramesInterface Plugin for GMAT (General Mission Analysis Tool)
//
// Copyright (c) 2022 Emergent Space Technologies, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Developed by Emergent Space Technologies, Inc. under contract number
// NNX16CG16C
//
// Author: Ravi Mathur, Emergent Space Technologies, Inc.
// Created: December 16, 2014
/**
 * Implementation for library code interfaces.
 *
 * This is prototype code.
 */
//------------------------------------------------------------------------------

#include "GmatPluginFunctions.hpp"
#include "OpenFramesInterfaceFactory.hpp"
#include "OpenFramesViewFactory.hpp"
#include "OpenFramesVectorFactory.hpp"
#include "OpenFramesSensorMaskFactory.hpp"
#include "OpenFramesInterface.hpp"
#include "OpenFramesGuiFactory.hpp"

#ifdef OFI_USE_WXWIDGETS
#include "OpenFramesEventHandler.hpp"
#endif

#include "Factory.hpp"
#include "MessageInterface.hpp"

extern "C"
{
   //---------------------------------------------------------------------------
   // Integer GetFactoryCount()
   //---------------------------------------------------------------------------
   /**
    * Returns the number of plug-in factories in this module
    *
    * @return The number of factories
    */
   //---------------------------------------------------------------------------
   Integer GetFactoryCount()
   {
     return 4;
   }

   //---------------------------------------------------------------------------
   // Factory* GetFactoryPointer(Integer index)
   //---------------------------------------------------------------------------
   /**
    * Retrieves a pointer to a specific factory
    *
    * @param index The index to the Factory
    *
    * @return The Factory pointer
    */
   //---------------------------------------------------------------------------
   Factory* GetFactoryPointer(Integer index)
   {
      Factory* factory = nullptr;

      switch (index)
      {
         case 0:
            factory = new OpenFramesInterfaceFactory;
            break;

         case 1:
            factory = new OpenFramesViewFactory;
            break;

         case 2:
            factory = new OpenFramesVectorFactory;
            break;
            
         case 3:
            factory = new OpenFramesSensorMaskFactory;
            break;

         default:
            break;
      }

      return factory;
   }

   //---------------------------------------------------------------------------
   // GUI PLUGIN iNTERFACE INTERFACES
   //---------------------------------------------------------------------------


   //---------------------------------------------------------------------------
   // std::string GetGuiToolkitName()
   //---------------------------------------------------------------------------
   /**
    * Sanity check method to ensure that the right toolkit is used
    *
    * @return "wxWidgets"  That's what is used here.
    */
   //---------------------------------------------------------------------------
   std::string GetGuiToolkitName()
   {
#ifdef OFI_USE_WXWIDGETS
      return "wxWidgets";
#elif OFI_USE_QT
      return "Qt";
#endif
   }

   //------------------------------------------------------------------------------
   // Integer GetMenuEntryCount()
   //------------------------------------------------------------------------------
   /**
    * Accesses the number of menu entries the plugin supports
    *
    * @return The number of entries
    */
   //------------------------------------------------------------------------------
   Integer GetMenuEntryCount()
   {
#ifdef OFI_USE_WXWIDGETS
      return 1;
#elif OFI_USE_QT
      return 0;
#endif
   }

   //------------------------------------------------------------------------------
   // Gmat::PluginResource* GetMenuEntry(Integer index)
   //------------------------------------------------------------------------------
   /**
    * Get a PluginResource that will handle a menu entry
    *
    * @param index The menu entry to be handled
    *
    * @return A plugin resource that GMAT can use to handle the menu entry
    */
   //------------------------------------------------------------------------------
   Gmat::PluginResource* GetMenuEntry(Integer index)
   {
      Gmat::PluginResource *rec = nullptr;

#ifdef OFI_USE_WXWIDGETS
      switch(index)
      {
      case 0:
      {
         rec = new Gmat::PluginResource;
         // The name of the top level folder in the Resource tree; empty for top level
         rec->parentNodeName = "Output";
         // Subfolder name in the tree; empty string attaches subtype to parent
         rec->nodeName = "";
         // Object type name -- this is the name of the item you can "Create" from script
         rec->subtype = "OpenFramesInterface";
         // Object type ID for created objects
         rec->type = static_cast<Gmat::ObjectType>(GmatType::GetTypeId("OpenFramesInterface"));

         rec->toolkit = "wxWidgets";
         rec->widgetType = "OpenFramesPanel";
         rec->trigger = -1;
         rec->firstId = rec->lastId = -1;

         // Wire up the event handler
         OpenFramesEventHandler *theHandler = new OpenFramesEventHandler();
         rec->handler = theHandler;

         break;
      }

      default:
         break;
      }
#endif

      return rec;
   }

   //---------------------------------------------------------------------------
   // Integer GetGuiElementCount()
   //---------------------------------------------------------------------------
   /**
    * Accesses the number of GuiFactory objects this plugin supports
    *
    * @return The number of factories
    */
   //---------------------------------------------------------------------------
   Integer GetGuiFactoryCount()
   {
      return 1;
   }


   //---------------------------------------------------------------------------
   // GuiFactory* GetGuiFactory()
   //---------------------------------------------------------------------------
   /**
    * Retrieves a GUI factory
    *
    * @param whichOne Index for the factory
    *
    * @return An instance of the factory
    */
   //---------------------------------------------------------------------------
   GuiFactory* GetGuiFactory(const Integer whichOne)
   {
      GuiFactory *retval = nullptr;

      if (whichOne == 0)
      {
         retval = new OpenFramesGuiFactory();
      }

      return retval;
   }

   //--------------------------------------------------------------------------
   // void SetMessageReceiver(MessageReceiver* mr)
   //--------------------------------------------------------------------------
   /**
    * Sets the messaging interface used for GMAT messages
    *
    * @param mr The message receiver
    *
    * @note This function is deprecated, and may not be needed in future builds
    */
   //---------------------------------------------------------------------------
   void SetMessageReceiver(MessageReceiver* mr)
   {
      MessageInterface::SetMessageReceiver(mr);
   }

   //--------------------------------------------------------------------------
   // void ProcessEvent(Integer event)
   //--------------------------------------------------------------------------
   /**
    * Processes a GUI event
    *
    * @param event The identifier or the event being reported
    */
   void ProcessEvent(Integer event)
   {

   }
};
