//------------------------------------------------------------------------------
//                           PluginEventHandler
//------------------------------------------------------------------------------
// OpenFramesInterface Plugin for GMAT (General Mission Analysis Tool)
//
// Copyright (c) 2022 Emergent Space Technologies, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Developed by Emergent Space Technologies, Inc. under contract number
// NNX16CG16C
//
// Author: Matthew Ruschmann, Emergent Space Technologies, Inc.
// Created: August 16, 2017
/**
 * Handle GUI events for the OpenFramesInterface GMAT plugin
 */
//------------------------------------------------------------------------------

#ifndef OPENFRAMESEVENTHANDLER_hpp
#define OPENFRAMESEVENTHANDLER_hpp

#include "OpenFramesInterface_defs.hpp"
#include "GmatEventHandler.hpp"

class GuiInterface;

class OpenFramesInterface_API OpenFramesEventHandler : public GmatEventHandler
{
public:
   OpenFramesEventHandler();
   virtual ~OpenFramesEventHandler();
   OpenFramesEventHandler(const OpenFramesEventHandler& peh);
   OpenFramesEventHandler& operator=(const OpenFramesEventHandler& peh);

   virtual bool ConnectEvent(Gmat::PluginResource &rec, void *parent,
         Integer messageID = -1);
};

#endif
