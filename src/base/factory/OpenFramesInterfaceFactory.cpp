//$Id$
//------------------------------------------------------------------------------
//                            OpenFramesInterfaceFactory
//------------------------------------------------------------------------------
// OpenFramesInterface Plugin for GMAT (General Mission Analysis Tool)
//
// Copyright (c) 2022 Emergent Space Technologies, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Developed by Emergent Space Technologies, Inc. under contract number
// NNX16CG16C
//
// Author: Ravi Mathur, Emergent Space Technologies, Inc.
// Created: December 15, 2014
/**
 *  Implementation code for the OpenFramesInterfaceFactory class, responsible for
 *  creating OpenFramesInterface Subscriber objects.
 */
//------------------------------------------------------------------------------
#include "OpenFramesInterfaceFactory.hpp"
#include "OpenFramesInterface.hpp"


//---------------------------------
//  public methods
//---------------------------------

//------------------------------------------------------------------------------
//  CreateObject(const std::string &ofType, const std::string &withName)
//------------------------------------------------------------------------------
/**
 * This method creates and returns an object of the requested Subscriber class
 * in generic way.
 *
 * @param <ofType> the Subscriber object to create and return.
 * @param <withName> the name to give the newly-created Subscriber object.
 *
 */
//------------------------------------------------------------------------------
GmatBase* OpenFramesInterfaceFactory::CreateObject(const std::string &ofType,
	                                                const std::string &withName)
{
   return CreateSubscriber(ofType, withName);
}

//------------------------------------------------------------------------------
//  CreateSubscriber(const std::string &ofType, const std::string &withName,
//                   const std::string &fileName)
//------------------------------------------------------------------------------
/**
 * This method creates and returns an object of the requested Subscriber class
 *
 * @param <ofType>   the Subscriber object to create and return.
 * @param <withName> the name to give the newly-created Subscriber object.
 *
 */
//------------------------------------------------------------------------------
Subscriber* OpenFramesInterfaceFactory::CreateSubscriber(const std::string &ofType,
                                                         const std::string &withName)
{
   if (ofType == "OpenFramesInterface")
   {
      OpenFramesInterface *obj = new OpenFramesInterface(ofType, withName);
      return obj;
   }

   return nullptr;
}


//------------------------------------------------------------------------------
//  OpenFramesInterfaceFactory()
//------------------------------------------------------------------------------
/**
 * This method creates an object of the class OpenFramesInterfaceFactory
 * (default constructor).
 *
 *
 */
//------------------------------------------------------------------------------
OpenFramesInterfaceFactory::OpenFramesInterfaceFactory() :
Factory(GmatType::RegisterType("OpenFramesInterface"))
{
   if (creatables.empty())
   {
      creatables.push_back("OpenFramesInterface");
   }
}


//------------------------------------------------------------------------------
//  OpenFramesInterfaceFactory(const OpenFramesInterfaceFactory& fact)
//------------------------------------------------------------------------------
/**
 * This method creates an object of the class OpenFramesInterfaceFactory
 * (copy constructor).
 *
 * @param <fact> the factory object to copy to "this" factory.
 */
//------------------------------------------------------------------------------
OpenFramesInterfaceFactory::OpenFramesInterfaceFactory(const OpenFramesInterfaceFactory& fact) :
Factory(fact)
{
  if (creatables.empty())
  {
    creatables.push_back("OpenFramesInterface");
  }
}


//------------------------------------------------------------------------------
// OpenFramesInterfaceFactory& operator= (const OpenFramesInterfaceFactory& fact)
//------------------------------------------------------------------------------
/**
 * Assignment operator for the OpenFramesInterfaceFactory class.
 *
 * @param <fact> the OpenFramesInterfaceFactory object whose data to assign to "this"
 *               factory.
 *
 * @return "this" OpenFramesInterfaceFactory with data of input factory fact.
 */
//------------------------------------------------------------------------------
OpenFramesInterfaceFactory& OpenFramesInterfaceFactory::operator= (const OpenFramesInterfaceFactory& fact)
{
   if (this != &fact)
   {
      Factory::operator=(fact);
   }
   return *this;
}


//------------------------------------------------------------------------------
// ~OpenFramesInterfaceFactory()
//------------------------------------------------------------------------------
/**
 * Destructor for the OpenFramesInterfaceFactory base class.
 *
 */
//------------------------------------------------------------------------------
OpenFramesInterfaceFactory::~OpenFramesInterfaceFactory()
{
   // deletes handled by Factory destructor
}

