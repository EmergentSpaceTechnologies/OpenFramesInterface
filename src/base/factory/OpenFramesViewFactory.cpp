//$Id$
//------------------------------------------------------------------------------
//                            OpenFramesViewFactory
//------------------------------------------------------------------------------
// OpenFramesInterface Plugin for GMAT (General Mission Analysis Tool)
//
// Copyright (c) 2022 Emergent Space Technologies, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Developed by Emergent Space Technologies, Inc. under contract number
// NNX16CG16C
//
// Author: Matthew Ruschmann, Emergent Space Technologies, Inc.
// Created: October 15, 2017
/**
 *  Implementation code for the OpenFramesViewFactory class, responsible for
 *  creating OpenFramesInterface Generic Objects.
 */
//------------------------------------------------------------------------------
#include "OpenFramesViewFactory.hpp"
#include "OpenFramesView.hpp"


//---------------------------------
//  public methods
//---------------------------------

//------------------------------------------------------------------------------
//  CreateObject(const std::string &ofType, const std::string &withName)
//------------------------------------------------------------------------------
/**
 * This method creates and returns an object of the requested Generic Object
 * class in generic way.
 *
 * @param <ofType> the Generic Object to create and return.
 * @param <withName> the name to give the newly-created Generic Object.
 *
 */
//------------------------------------------------------------------------------
GmatBase* OpenFramesViewFactory::CreateObject(const std::string &ofType,
	                                                const std::string &withName)
{
   if (ofType == "OpenFramesView")
   {
      return new OpenFramesView(ofType, withName);
   }

   return nullptr;
}


//------------------------------------------------------------------------------
//  OpenFramesViewFactory()
//------------------------------------------------------------------------------
/**
 * This method creates an object of the class OpenFramesViewFactory
 * (default constructor).
 *
 *
 */
//------------------------------------------------------------------------------
OpenFramesViewFactory::OpenFramesViewFactory() :
Factory(GmatType::RegisterType("OpenFramesView"))
{
   if (creatables.empty())
   {
      creatables.push_back("OpenFramesView");
   }
}


//------------------------------------------------------------------------------
//  OpenFramesViewFactory(const OpenFramesViewFactory& fact)
//------------------------------------------------------------------------------
/**
 * This method creates an object of the class OpenFramesViewFactory
 * (copy constructor).
 *
 * @param <fact> the factory object to copy to "this" factory.
 */
//------------------------------------------------------------------------------
OpenFramesViewFactory::OpenFramesViewFactory(const OpenFramesViewFactory& fact) :
Factory(fact)
{
  if (creatables.empty())
  {
    creatables.push_back("OpenFramesView");
  }
}


//------------------------------------------------------------------------------
// OpenFramesViewFactory& operator= (const OpenFramesViewFactory& fact)
//------------------------------------------------------------------------------
/**
 * Assignment operator for the OpenFramesViewFactory class.
 *
 * @param <fact> the OpenFramesViewFactory object whose data to assign to "this"
 *               factory.
 *
 * @return "this" OpenFramesViewFactory with data of input factory fact.
 */
//------------------------------------------------------------------------------
OpenFramesViewFactory& OpenFramesViewFactory::operator= (const OpenFramesViewFactory& fact)
{
   if (this != &fact)
   {
      Factory::operator=(fact);
   }
   return *this;
}


//------------------------------------------------------------------------------
// ~OpenFramesViewFactory()
//------------------------------------------------------------------------------
/**
 * Destructor for the OpenFramesViewFactory base class.
 *
 */
//------------------------------------------------------------------------------
OpenFramesViewFactory::~OpenFramesViewFactory()
{
   // deletes handled by Factory destructor
}
