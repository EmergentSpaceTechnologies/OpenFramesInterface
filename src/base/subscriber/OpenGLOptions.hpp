//$Id$
//------------------------------------------------------------------------------
//                                  OpenGLOptions
//------------------------------------------------------------------------------
// OpenFramesInterface Plugin for GMAT (General Mission Analysis Tool)
//
// Copyright (c) 2022 Emergent Space Technologies, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Developed by Emergent Space Technologies, Inc. under contract number
// NNX16CG16C
//
// Author: Matthew Ruschmann, Emergent Space Technologies, Inc.
// Created: December 7, 2017
/**
 *  @class OpenGLOptions
 *  Conveniently wraps up all options associated with OpenGL canvas.
 */
//------------------------------------------------------------------------------

#ifndef OPENGLOPTIONS_H
#define OPENGLOPTIONS_H

#include "OpenFramesInterface_defs.hpp"

#include <set>
#include <string>


class OpenFramesInterface_API OpenGLOptions
{
public:
   typedef std::set<Integer> MSAASampleSet; // Set of available MSAA samples

   OpenGLOptions();
   virtual ~OpenGLOptions();

   OpenGLOptions(const OpenGLOptions &source);
   OpenGLOptions& operator=(const OpenGLOptions &rhs);

   const int* GetGLArgs() const { return mGLArgs.data(); }
   bool GetEnableMSAA() const { return (mGLArgs[mEnableMSAA] == 1); } ///< get @ref mEnableMSAA
   bool SetEnableMSAA(bool value); ///< set @ref mEnableMSAA
   Integer GetMSAASamples() const { return mGLArgs[mMSAASamples]; } ///< get @ref mMSAASamples
   Integer SetMSAASamples(Integer value); ///< set @ref mMSAASamples
   const MSAASampleSet& GetAvailableMSAASamples() const { return mAvailableMSAASamples; } ///< get @ref mAvailableMSAASamples
   bool GetEnableVR() const { return mEnableVR; } ///< get @ref mEnableVR
   void SetEnableVR(bool value) { mEnableVR = value; } ///< set @ref mEnableVR

   bool EqualTo(const OpenGLOptions &options) const;

private:
   /// Enable virtual reality
   bool mEnableVR;
   /// Display attribute list for initializing wxGLCanvas
   std::vector<int> mGLArgs;
   /// Index of multisample anti-aliasing flag in mGLArgs
   unsigned int mEnableMSAA;
   /// Index of MSAA samples value in mGLArgs
   unsigned int mMSAASamples;
   /// Available MSAA samples
   static MSAASampleSet mAvailableMSAASamples;
};

#endif
