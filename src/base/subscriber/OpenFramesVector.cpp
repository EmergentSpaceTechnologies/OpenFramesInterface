//$Id$
//------------------------------------------------------------------------------
//                                  OpenFramesVector
//------------------------------------------------------------------------------
// OpenFramesInterface Plugin for GMAT (General Mission Analysis Tool)
//
// Copyright (c) 2022 Emergent Space Technologies, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http ://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Developed by Emergent Space Technologies, Inc. under contract number
// NNX16CG16C
//
// Author: Yasir Majeed Khan, Emergent Space Technologies, Inc.
// Created: October 5, 2018
/**
*  OpenFramesVector class
*  Manages parameters associated with an OpenFrames vector.
*/
//------------------------------------------------------------------------------

#include "OpenFramesVector.hpp"

#include "SubscriberException.hpp"
#include "TextParser.hpp"


/// Names of parameters for OpenFramesVector
const std::string
OpenFramesVector::PARAMETER_TEXT[OpenFramesVectorParamCount - GmatBaseParamCount] =
{
  "VectorColor",
  "SourceObject",
  "VectorType",
  "DestinationObject",
  "BFStartPoint",
  "BFDirection",
  "VectorLabel",
  "VectorLengthType",
  "VectorLength",
};


/// Gmat interpreter types for the parameters in @ref OpenFramesVector::PARAMETER_TEXT
const Gmat::ParameterType
OpenFramesVector::PARAMETER_TYPE[OpenFramesVectorParamCount - GmatBaseParamCount] =
{
  Gmat::COLOR_TYPE,       //"VectorColor"
  Gmat::OBJECT_TYPE,      //"SourceObject"
  Gmat::OBJECT_TYPE,      //"VectorType"
  Gmat::OBJECT_TYPE,      //"DestinationObject"
  Gmat::RVECTOR_TYPE,     //"BFStartPoint"
  Gmat::RVECTOR_TYPE,     //"BFDirection"
  Gmat::STRING_TYPE,      //"VectorLabel"
  Gmat::STRING_TYPE,      //"VectorLengthType"
  Gmat::REAL_TYPE,        //"VectorLength"
};


/// String that represents On
const std::string OpenFramesVector::ON_STRING = "On";


/// String that represents OFF
const std::string OpenFramesVector::OFF_STRING = "Off";

//------------------------------------------------------------------------------
// OpenFramesVector(const std::string &type, const std::string &name)
//------------------------------------------------------------------------------
/**
*  Constructor
*/
//------------------------------------------------------------------------------
OpenFramesVector::OpenFramesVector(const std::string &type, const std::string &name) :
GmatBase(GmatType::GetTypeId("OpenFramesVector"), type, name),
mVectorColor("Red"),
mSourceObject("None Selected"),
mVectorType("Body-Fixed"),
mDestinationObject("None Selected"),
mBFStartPoint(3, 0.0, 0.0, 0.0), // origin
mBFDirection(3, 1.0, 0.0, 0.0),  // x-axis
mVectorLabel("DefaultVector"),
mVectorLengthType("Auto"),
mVectorLength(1.0)
{
  objectTypeNames.push_back("OpenFramesVector");
  objectTypes.push_back(GmatBase::type);

  parameterCount = OpenFramesVectorParamCount;
}


//------------------------------------------------------------------------------
// ~OpenFramesVector(void)
//------------------------------------------------------------------------------
/**
* Destructor.
*/
//------------------------------------------------------------------------------
OpenFramesVector::~OpenFramesVector(void)
{
}


//------------------------------------------------------------------------------
// OpenFramesVector(const OpenFramesVector &rf)
//------------------------------------------------------------------------------
/**
* Copy Constructor
*/
//------------------------------------------------------------------------------
OpenFramesVector::OpenFramesVector(const OpenFramesVector &dc) :
GmatBase(dc)
{
  mVectorColor = dc.mVectorColor;
  mSourceObject = dc.mSourceObject;
  mVectorType = dc.mVectorType;
  mDestinationObject = dc.mDestinationObject;
  mBFStartPoint = dc.mBFStartPoint;
  mBFDirection = dc.mBFDirection;
  mVectorLabel = dc.mVectorLabel;
  mVectorLengthType = dc.mVectorLengthType;
  mVectorLength = dc.mVectorLength;
 
  parameterCount = OpenFramesVectorParamCount;
}


//------------------------------------------------------------------------------
// OpenFramesVector& OpenFramesVector::operator=(const OpenFramesVector& dc)
//------------------------------------------------------------------------------
/**
* Assignment operator
*/
//------------------------------------------------------------------------------
OpenFramesVector& OpenFramesVector::operator=(const OpenFramesVector& dc)
{
  if (this == &dc) {
    return *this;
  }

  GmatBase::operator=(dc);

  mVectorColor = dc.mVectorColor;
  mSourceObject = dc.mSourceObject;
  mVectorType = dc.mVectorType;
  mDestinationObject = dc.mDestinationObject;
  mBFStartPoint = dc.mBFStartPoint;
  mBFDirection = dc.mBFDirection;
  mVectorLabel = dc.mVectorLabel;
  mVectorLengthType = dc.mVectorLengthType;
  
  return *this;
}


//------------------------------------------------------------------------------
//  bool Validate()
//------------------------------------------------------------------------------
/**
* Performs any pre-run validation that the object needs.
*
* @return true unless validation fails.
*/
//------------------------------------------------------------------------------
bool OpenFramesVector::Validate()
{
  return GmatBase::Validate();
}


//------------------------------------------------------------------------------
// virtual bool Initialize()
/**
* Initialize an instance of the interface, or reinitailize an instance
*
* @return true for all logic paths
*/
//------------------------------------------------------------------------------
bool OpenFramesVector::Initialize()
{
  return GmatBase::Initialize();
}


//------------------------------------------------------------------------------
//  GmatBase* Clone(void) const
//------------------------------------------------------------------------------
/**
* This method returns a clone of the OpenFramesVector.
*
* @return clone of the OpenFramesVector.
*/
//------------------------------------------------------------------------------
GmatBase* OpenFramesVector::Clone() const
{
  return (new OpenFramesVector(*this));
}


//---------------------------------------------------------------------------
// void Copy(const GmatBase* orig)
//---------------------------------------------------------------------------
/**
* Sets this object to match another one.
*
* @param orig The original that is being copied.
*/
//---------------------------------------------------------------------------
void OpenFramesVector::Copy(const GmatBase* orig)
{
  operator=(*((OpenFramesVector *)(orig)));
}


//------------------------------------------------------------------------------
// virtual bool TakeAction(const std::string &action,
//                         const std::string &actionData = "");
//------------------------------------------------------------------------------
/**
* This method performs action.
*
* @param <action> action to perform
* @param <actionData> action data associated with action
* @return true if action successfully performed
*
*/
//------------------------------------------------------------------------------
bool OpenFramesVector::TakeAction(const std::string &action,
  const std::string &actionData)
{
  return GmatBase::TakeAction(action, actionData);
}


//---------------------------------------------------------------------------
//  bool RenameRefObject(const Gmat::ObjectType type,
//                       const std::string &oldName, const std::string &newName)
//---------------------------------------------------------------------------
bool OpenFramesVector::RenameRefObject(const UnsignedInt type,
  const std::string &oldName,
  const std::string &newName)
{
  bool success = true;

  if (type == Gmat::SPACECRAFT || type == Gmat::GROUND_STATION ||
    type == Gmat::CALCULATED_POINT || type == Gmat::CELESTIAL_BODY || type == Gmat::SPACE_POINT)
  {
    bool renamed = false;
    TextParser parser;

    StringArray mSourceObjectParts = parser.SeparateBy(mSourceObject, ".");
    if (mSourceObjectParts.size() > 0 && mSourceObjectParts[0] == oldName)
    {
      mSourceObject.replace(0, mSourceObjectParts[0].size(), newName);
      renamed = true;
    }

    StringArray mVectorTypeParts = parser.SeparateBy(mVectorType, ".");
    if (mVectorTypeParts.size() > 0 && mVectorTypeParts[0] == oldName)
    {
      mVectorType.replace(0, mVectorTypeParts[0].size(), newName);
      renamed = true;
    }

    StringArray mDestinationObjectParts = parser.SeparateBy(mDestinationObject, ".");
    if (mDestinationObjectParts.size() > 0 && mDestinationObjectParts[0] == oldName)
    {
      mDestinationObject.replace(0, mDestinationObjectParts[0].size(), newName);
      renamed = true;
    }
    
    if (!renamed)
      success = GmatBase::RenameRefObject(type, oldName, newName);
  }
  else
  {
    success = GmatBase::RenameRefObject(type, oldName, newName);
  }

  return success;
}


//------------------------------------------------------------------------------
// std::string GetParameterText(const Integer id) const
//------------------------------------------------------------------------------
std::string OpenFramesVector::GetParameterText(const Integer id) const
{
  if (id >= GmatBaseParamCount && id < OpenFramesVectorParamCount)
    return PARAMETER_TEXT[id - GmatBaseParamCount];
  else
    return GmatBase::GetParameterText(id);

}


//------------------------------------------------------------------------------
// Integer GetParameterID(const std::string &str) const
//------------------------------------------------------------------------------
Integer OpenFramesVector::GetParameterID(const std::string &str) const
{
  for (int i = GmatBaseParamCount; i < OpenFramesVectorParamCount; i++)
  {
    if (str == PARAMETER_TEXT[i - GmatBaseParamCount])
      return i;
  }

  return GmatBase::GetParameterID(str);
}


//------------------------------------------------------------------------------
// Gmat::ParameterType GetParameterType(const Integer id) const
//------------------------------------------------------------------------------
Gmat::ParameterType OpenFramesVector::GetParameterType(const Integer id) const
{
  if (id >= GmatBaseParamCount && id < OpenFramesVectorParamCount)
    return PARAMETER_TYPE[id - GmatBaseParamCount];
  else
    return GmatBase::GetParameterType(id);
}


//------------------------------------------------------------------------------
// std::string GetParameterTypeString(const Integer id) const
//------------------------------------------------------------------------------
std::string OpenFramesVector::GetParameterTypeString(const Integer id) const
{
  return GmatBase::PARAM_TYPE_STRING[GetParameterType(id)];
}


//---------------------------------------------------------------------------
//  bool IsParameterReadOnly(const Integer id) const
//---------------------------------------------------------------------------
bool OpenFramesVector::IsParameterReadOnly(const Integer id) const
{
  return GmatBase::IsParameterReadOnly(id);
}

//---------------------------------------------------------------------------
// bool IsSquareBracketAllowedInSetting(const Integer id) const
//---------------------------------------------------------------------------
bool OpenFramesVector::IsSquareBracketAllowedInSetting(const Integer id) const
{
  if (id == VECTOR_COLOR)
    return true;
  else
    return GmatBase::IsSquareBracketAllowedInSetting(id);
}


//------------------------------------------------------------------------------
// std::string GetStringParameter(const Integer id) const
//------------------------------------------------------------------------------
std::string OpenFramesVector::GetStringParameter(const Integer id) const
{
  switch (id)
  {
  case VECTOR_COLOR:
    return mVectorColor;
  case SOURCE_OBJECT:
    return mSourceObject;
  case VECTOR_TYPE:
    return mVectorType;
  case DESTINATION_OBJECT:
    return mDestinationObject;
  case VECTOR_LABEL:
    return mVectorLabel;
  case VECTOR_LENGTH_TYPE:
    return mVectorLengthType;
  default:
    return GmatBase::GetStringParameter(id);
  }
}


//------------------------------------------------------------------------------
// std::string GetStringParameter(const std::string &label) const
//------------------------------------------------------------------------------
std::string OpenFramesVector::GetStringParameter(const std::string &label) const
{
  return GetStringParameter(GetParameterID(label));
}


//------------------------------------------------------------------------------
// bool SetStringParameter(const Integer id, const std::string &value)
//------------------------------------------------------------------------------
bool OpenFramesVector::SetStringParameter(const Integer id, const std::string &value)
{
  switch (id)
  {
  case VECTOR_COLOR:
  {
    mVectorColor = value;
    return true;
  }
  case SOURCE_OBJECT:
  {
    mSourceObject = value;
    return true;
  }
  case VECTOR_TYPE:
  {
    mVectorType = value;
    return true;
  }
  case DESTINATION_OBJECT:
  {
    mDestinationObject = value;
    return true;
  }
  case VECTOR_LABEL:
  {
    mVectorLabel = value;
    return true;
  }
  case VECTOR_LENGTH_TYPE:
  {
    mVectorLengthType = value;
    return true;
  }
  default:
    return GmatBase::SetStringParameter(id, value);
  }
}


//------------------------------------------------------------------------------
// bool SetStringParameter(const std::string &label, const std::string &value)
//------------------------------------------------------------------------------
bool OpenFramesVector::SetStringParameter(const std::string &label,
  const std::string &value)
{
  return SetStringParameter(GetParameterID(label), value);
}


//---------------------------------------------------------------------------
//  std::string GetOnOffParameter(const Integer id) const
//---------------------------------------------------------------------------
std::string OpenFramesVector::GetOnOffParameter(const Integer id) const
{
  
    return GmatBase::GetOnOffParameter(id);
}


//------------------------------------------------------------------------------
// std::string OrbitView::GetOnOffParameter(const std::string &label) const
//------------------------------------------------------------------------------
std::string OpenFramesVector::GetOnOffParameter(const std::string &label) const
{
  return GetOnOffParameter(GetParameterID(label));
}


//---------------------------------------------------------------------------
//  bool SetOnOffParameter(const Integer id, const std::string &value)
//---------------------------------------------------------------------------
bool OpenFramesVector::SetOnOffParameter(const Integer id, const std::string &value)
{
    return GmatBase::SetOnOffParameter(id, value);
}


//------------------------------------------------------------------------------
// bool SetOnOffParameter(const std::string &label, const std::string &value)
//------------------------------------------------------------------------------
bool OpenFramesVector::SetOnOffParameter(const std::string &label,
  const std::string &value)
{
  return SetOnOffParameter(GetParameterID(label), value);
}


//---------------------------------------------------------------------------
//  Real GetRealParameter(const Integer id) const
//---------------------------------------------------------------------------
Real OpenFramesVector::GetRealParameter(const Integer id) const
{
  switch (id)
  {
  case VECTOR_LENGTH:
    return mVectorLength;

  default:
    return GmatBase::GetRealParameter(id);
  }
}

//------------------------------------------------------------------------------
// Real OrbitView::GetRealParameter(const std::string &label) const
//------------------------------------------------------------------------------
Real OpenFramesVector::GetRealParameter(const std::string &label) const
{
  return GetRealParameter(GetParameterID(label));
}

//---------------------------------------------------------------------------
//  Real GetRealParameter(const Integer id, const Integer index) const
//---------------------------------------------------------------------------
Real OpenFramesVector::GetRealParameter(const Integer id, const Integer index) const
{
  switch (id)
  {
  case BF_START_POINT:
    if (index < mBFStartPoint.GetSize())
      return mBFStartPoint.GetElement(index);
    else
      return 0.0;
  case BF_DIRECTION:
    if (index < mBFDirection.GetSize())
      return mBFDirection.GetElement(index);
    else
      return 0.0;
  default:
    return GmatBase::GetRealParameter(id, index);
  }
  
}


//------------------------------------------------------------------------------
// Real OrbitView::GetRealParameter(const std::string &label, const Integer index) const
//------------------------------------------------------------------------------
Real OpenFramesVector::GetRealParameter(const std::string &label,
  const Integer index) const
{
  return GetRealParameter(GetParameterID(label), index);
}



//---------------------------------------------------------------------------
//  Real SetRealParameter(const Integer id, const Real value)
//---------------------------------------------------------------------------
Real OpenFramesVector::SetRealParameter(const Integer id, const Real value)
{
  switch (id)
  {
  case VECTOR_LENGTH:
  {
    if(value > 0.0)
    {
      mVectorLength = value;
      return value;
    }
    else
    {
      std::string valueStr = std::to_string(value);
      SubscriberException se;
      se.SetDetails(errorMessageFormat.c_str(), valueStr.c_str(),
        GetParameterText(id).c_str(), "Real Value > 0.0");
      throw se;
    }
  }

  default:
    return GmatBase::SetRealParameter(id, value);
  }
}

//------------------------------------------------------------------------------
// Real SetRealParameter(const std::string &label, const Real value);
//------------------------------------------------------------------------------
Real OpenFramesVector::SetRealParameter(const std::string &label, const Real value)
{
  return SetRealParameter(GetParameterID(label), value);
}



//---------------------------------------------------------------------------
//  Real SetRealParameter(const Integer id, const Real value,
//                        const Integer index)
//---------------------------------------------------------------------------
Real OpenFramesVector::SetRealParameter(const Integer id, const Real value,
  const Integer index)
{
  switch (id)
  {
  case BF_START_POINT:
    if (index < 3)
    {
      mBFStartPoint.SetElement(index, value);
      return value;
    }
    else
    {
      std::string value = std::to_string(index);
      SubscriberException se;
      se.SetDetails(errorMessageFormat.c_str(), value.c_str(),
        GetParameterText(id).c_str(), "Invalid row and/or column");
      throw se;
    }
  case BF_DIRECTION:
    if (index < 3)
    {
      mBFDirection.SetElement(index, value);
      return value;
    }
    else
    {
      std::string value = std::to_string(index);
      SubscriberException se;
      se.SetDetails(errorMessageFormat.c_str(), value.c_str(),
        GetParameterText(id).c_str(), "Invalid row and/or column");
      throw se;
    }
  default:
    return GmatBase::SetRealParameter(id, value, index);
  }
}


//------------------------------------------------------------------------------
// Real SetRealParameter(const std::string &label, const Real value,
//                       const Integer index);
//------------------------------------------------------------------------------
Real OpenFramesVector::SetRealParameter(const std::string &label,
  const Real value, const Integer index)
{
  return SetRealParameter(GetParameterID(label), value, index);
}


//---------------------------------------------------------------------------
// const Rvector& GetRvectorParameter(const Integer id) const
//---------------------------------------------------------------------------
const Rvector &OpenFramesVector::GetRvectorParameter(const Integer id) const
{
  switch (id)
  {
  case BF_START_POINT:
    return mBFStartPoint;
  case BF_DIRECTION:
    return mBFDirection;
  default:
    return GmatBase::GetRvectorParameter(id);
  }
}


//------------------------------------------------------------------------------
// const Rvector& GetRvectorParameter(const std::string &label) const
//------------------------------------------------------------------------------
const Rvector &OpenFramesVector::GetRvectorParameter(const std::string &label) const
{
  return GetRvectorParameter(GetParameterID(label));
}


//---------------------------------------------------------------------------
//  const Rvector& SetRvectorParameter(const Integer id, const Rmatrix &value)
//---------------------------------------------------------------------------
const Rvector &OpenFramesVector::SetRvectorParameter(const Integer id,
  const Rvector &value)
{
  switch (id)
  {
  case BF_START_POINT:
    if (value.GetSize() == 3)
    {
      mBFStartPoint = value;
      return mBFStartPoint;
    }
    else
    {
      std::string val = std::to_string(value.GetSize());
      SubscriberException se;
      se.SetDetails(errorMessageFormat.c_str(), val.c_str(),
        "BFStartPoint", "Invalid size of source matrix");
      throw se;
    }
  case BF_DIRECTION:
    if (value.GetSize() == 3)
    {
      mBFDirection = value;
      return mBFDirection;
    }
    else
    {
      std::string val = std::to_string(value.GetSize());
      SubscriberException se;
      se.SetDetails(errorMessageFormat.c_str(), val.c_str(),
        "BFDirection", "Invalid size of source matrix");
      throw se;
    }
  default:
    return GmatBase::SetRvectorParameter(id, value);
  }
}


//------------------------------------------------------------------------------
// const Rvector& SetRvectorParameter(const std::string &label, const Rmatrix &value)
//------------------------------------------------------------------------------
const Rvector &OpenFramesVector::SetRvectorParameter(const std::string &label,
  const Rvector &value)
{
  return SetRvectorParameter(GetParameterID(label), value);
}


//------------------------------------------------------------------------------
// bool IsParameterCloaked(const Integer id) const
//------------------------------------------------------------------------------
bool OpenFramesVector::IsParameterCloaked(const Integer id) const
{
  switch (id)
  {
    // Vector length cloaked if length type is Auto
    case VECTOR_LENGTH:
    {
      if (mVectorLengthType == "Auto") return true;
      else return false;
    }

    // Destination object cloaked if vector type is not Relative Position
    case DESTINATION_OBJECT:
    {
      if (mVectorType != "Relative Position") return true;
      else return false;
    }

    // Body-fixed parameters cloaked if vector type is not Body-Fixed
    case BF_START_POINT:
    case BF_DIRECTION:
    {
      if (mVectorType != "Body-Fixed") return true;
      else return false;
    }

    default:
      return GmatBase::IsParameterCloaked(id);
  }
}


//------------------------------------------------------------------------------
// bool IsParameterCloaked(const std::string &label) const
//------------------------------------------------------------------------------
bool OpenFramesVector::IsParameterCloaked(const std::string &label) const
{
  return IsParameterCloaked(GetParameterID(label));
}


//---------------------------------------------------------------------------
// Gmat::ObjectType GetPropertyObjectType(const Integer id) const
//---------------------------------------------------------------------------
UnsignedInt OpenFramesVector::GetPropertyObjectType(const Integer id) const
{
  return GmatBase::GetPropertyObjectType(id);
}


//------------------------------------------------------------------------------
// std::string OpenFramesVector::StringArrayToString(const StringArray &array)
//------------------------------------------------------------------------------
/**
* Converts a boolean to "On" or "Off" string
*
* @param onIfTrue The boolean to convert
* @return "On" if input is true, "Off" otherwise
*/
//---------------------------------------------------------------
std::string OpenFramesVector::BooleanToOnOff(bool onIfTrue)
{
  return (onIfTrue) ? ON_STRING : OFF_STRING;
};
