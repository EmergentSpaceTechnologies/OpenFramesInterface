//$Id$
//------------------------------------------------------------------------------
//                                  OpenFramesSensorMask
//------------------------------------------------------------------------------
// OpenFramesInterface Plugin for GMAT (General Mission Analysis Tool)
//
// Copyright (c) 2022 Emergent Space Technologies, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Developed by Emergent Space Technologies, Inc. under SBIR contract 80NSSC19C0044
//
// Author: Ravi Mathur, Emergent Space Technologies, Inc.
// Created: January 10, 2020
/**
*  @class OpenFramesSensorMask
*  An interface for tracking configuration parameters associated with an
*  OpenFrames::SensorMask
*/
//------------------------------------------------------------------------------

#ifndef OPENFRAMESSENSORMASK_hpp
#define OPENFRAMESSENSORMASK_hpp

#include "OpenFramesInterface_defs.hpp"

#include "GmatBase.hpp"


class OpenFramesInterface_API OpenFramesSensorMask : public GmatBase
{
public:
   OpenFramesSensorMask(const std::string &typeName, const std::string &name);

   virtual ~OpenFramesSensorMask(void);

   OpenFramesSensorMask(const OpenFramesSensorMask&);
   OpenFramesSensorMask& operator=(const OpenFramesSensorMask&);

   /// Source object for this sensor mask (e.g. Spacecraft, Ground Station, etc...)
   void SetSensorMaskSource(const std::string& src) { mSensorMaskSource = src; }
   const std::string& GetSensorMaskSource() const { return mSensorMaskSource; }
   
   /// Hardware object for this sensor mask (must have FieldOfView attached)
   void SetSensorMaskHardware(const std::string& hw) { mSensorMaskHardware = hw; }
   const std::string& GetSensorMaskHardware() const { return mSensorMaskHardware; }
   
   /// Sensor mask label
   void SetSensorMaskLabel(const std::string& lbl) { mSensorMaskLabel = lbl; }
   const std::string& GetSensorMaskLabel() const { return mSensorMaskLabel; }
   
   /// Length type for sensor mask principal axis
   enum MaskLengthType
   {
      AUTO,   // Automatically compute mask axis length
      MANUAL, // Manually specify mask axis length
      NUM_MASK_LENGTH_TYPES
   };
   MaskLengthType GetMaskLengthType(const std::string& typeStr) const;
   const std::string& GetMaskLengthTypeString(MaskLengthType type) const;
   void SetSensorMaskAxisLengthType(MaskLengthType type);
   void SetSensorMaskAxisLengthType(const std::string& typeStr);
   MaskLengthType GetSensorMaskAxisLengthType() const { return mSensorMaskLengthType; }
   
   /// Length of sensor mask principal axis
   /// Only applies if length type is MANUAL
   void SetSensorMaskAxisLength(Real length);
   Real GetSensorMaskAxisLength() const { return mSensorMaskLength; }
   
   // methods inhereted from GmatBase
   virtual bool         Validate();
   virtual bool         Initialize();

   virtual GmatBase*    Clone(void) const;
   virtual void         Copy(const GmatBase* orig);
   virtual bool         TakeAction(const std::string &action,
                                   const std::string &actionData);

   virtual bool         RenameRefObject(const UnsignedInt type,
                                        const std::string &oldName,
                                        const std::string &newName);

   // methods for parameters
   virtual std::string  GetParameterText(const Integer id) const;
   virtual Integer      GetParameterID(const std::string &str) const;
   virtual Gmat::ParameterType GetParameterType(const Integer id) const;
   virtual std::string  GetParameterTypeString(const Integer id) const;
   virtual bool         IsParameterReadOnly(const Integer id) const;

   virtual std::string  GetStringParameter(const Integer id) const;
   virtual bool         SetStringParameter(const Integer id, const std::string &value);
   using GmatBase::GetStringParameter; // Unhide other function signatures
   using GmatBase::SetStringParameter;

   virtual Real         GetRealParameter(const Integer id) const;
   virtual Real         SetRealParameter(const Integer id, const Real value);
   using GmatBase::GetRealParameter;
   using GmatBase::SetRealParameter;

   virtual const StringArray& GetPropertyEnumStrings(const Integer id) const;
   using GmatBase::GetPropertyEnumStrings;
   
   virtual bool         IsParameterCloaked(const Integer id) const;

   // for GUI population
   virtual UnsignedInt  GetPropertyObjectType(const Integer id) const;

   DEFAULT_TO_NO_CLONES

protected:

   /// The name of the mask source object
   std::string mSensorMaskSource;
   /// The name of the mask hardware
   std::string mSensorMaskHardware;
   /// The label of the mask
   std::string mSensorMaskLabel;
   /// The mask length type
   MaskLengthType mSensorMaskLengthType;
   /// The length of the mask
   Real mSensorMaskLength;

   /// An emumeration of parameters for OpenFramesSensorMask, starting at the end
   /// of the @ref OrbitPlot::OrbitPlotParamCount enumeration.
   enum
   {
      SOURCE = GmatBaseParamCount,     ///< Source of mask
      HARDWARE,                        ///< Hardware of mask
      LABEL,                           ///< The label of the mask
      LENGTH_TYPE,                     ///< The type of length of mask
      LENGTH,                          ///< The length of mask
      OpenFramesSensorMaskParamCount,  ///< Count of the parameters for this class
   };

private:

   static const std::string
      PARAMETER_TEXT[OpenFramesSensorMaskParamCount - GmatBaseParamCount];
   static const Gmat::ParameterType
      PARAMETER_TYPE[OpenFramesSensorMaskParamCount - GmatBaseParamCount];
   static const StringArray MaskLengthTypeStrings;
};

#endif
