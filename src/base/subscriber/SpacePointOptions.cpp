//$Id$
//------------------------------------------------------------------------------
//                                  SpacePointOptions
//------------------------------------------------------------------------------
// OpenFramesInterface Plugin for GMAT (General Mission Analysis Tool)
//
// Copyright (c) 2022 Emergent Space Technologies, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Developed by Emergent Space Technologies, Inc. under contract number
// NNX16CG16C
//
// Author: Matthew Ruschmann, Emergent Space Technologies, Inc.
// Created: August 24, 2017
/**
 *  OFSpacecraft class
 *  Conveniently wraps a Spacepoint with associated options.
 */
//------------------------------------------------------------------------------

#include "SpacePointOptions.hpp"

#include "SpacePoint.hpp"

/**
 * Constructor
 *
 * @param name A name for the instance
 */
SpacePointOptions::SpacePointOptions(const std::string &name) :
   mName(name),
   mSpacePoint(nullptr),
   mDrawObject(true),
   mDrawTrajectory(true),
   mDrawAxes(false),
   mDrawXYPlane(false),
   mDrawLabel(true),
   mDrawLabelPropagate(false),
   mDrawCenter(true),
   mDrawEnds(true),
   mDrawVelocity(false),
   mDrawGrid(false),
   mDrawLineWidth(2.0),
   mDrawMarkerSize(10U),
   mDrawFontSize(14U),
   mDrawFontPosition("Top-Right"),
   mCurrentOrbitColor(GmatColor::WHITE),
   mCurrentTargetColor(GmatColor::WHITE)
{
   // Nothing else to do here
}


/**
 * Destructor
 */
SpacePointOptions::~SpacePointOptions()
{
   // Nothing to do here
}


/**
 * Copy Constructor
 *
 * @param source The object to copy
 */
SpacePointOptions::SpacePointOptions(const SpacePointOptions &source) :
   mName(source.GetName()),
   mSpacePoint(const_cast<SpacePoint *>(source.GetSpacePointConst())),
   mModelFile(source.GetModelFile()),
   mDrawObject(source.GetDrawObject()),
   mDrawTrajectory(source.GetDrawTrajectory()),
   mDrawAxes(source.GetDrawAxes()),
   mDrawXYPlane(source.GetDrawXYPlane()),
   mDrawLabel(source.GetDrawLabel()),
   mDrawLabelPropagate(source.GetUsePropagateLabel()),
   mDrawCenter(source.GetDrawCenter()),
   mDrawEnds(source.GetDrawEnds()),
   mDrawVelocity(source.GetDrawVelocity()),
   mDrawGrid(source.GetDrawGrid()),
   mDrawLineWidth(source.GetDrawLineWidth()),
   mDrawMarkerSize(source.GetDrawMarkerSize()),
   mDrawFontSize(source.GetDrawFontSize()),
   mDrawFontPosition(source.GetDrawFontPosition()),
   mCurrentOrbitColor(source.GetCurrentOrbitColor()),
   mCurrentTargetColor(source.GetCurrentTargetColor())
{
   // Nothing else to do here
}


/**
 * Assignment operator
 *
 * @param rhs The object to copy
 */
SpacePointOptions &SpacePointOptions::operator=(const SpacePointOptions &rhs)
{
   mName = rhs.GetName();
   mSpacePoint = const_cast<SpacePoint *>(rhs.GetSpacePointConst());
   mModelFile = rhs.GetModelFile();
   mDrawObject = rhs.GetDrawObject();
   mDrawTrajectory = rhs.GetDrawTrajectory();
   mDrawAxes = rhs.GetDrawAxes();
   mDrawXYPlane = rhs.GetDrawXYPlane();
   mDrawLabel = rhs.GetDrawLabel();
   mDrawLabelPropagate = rhs.GetUsePropagateLabel();
   mDrawCenter = rhs.GetDrawCenter();
   mDrawEnds = rhs.GetDrawEnds();
   mDrawVelocity = rhs.GetDrawVelocity();
   mDrawGrid = rhs.GetDrawGrid();
   mDrawLineWidth = rhs.GetDrawLineWidth();
   mDrawMarkerSize = rhs.GetDrawMarkerSize();
   mDrawFontSize = rhs.GetDrawFontSize();
   mDrawFontPosition = rhs.GetDrawFontPosition();
   mCurrentOrbitColor = rhs.GetCurrentOrbitColor();
   mCurrentTargetColor = rhs.GetCurrentTargetColor();
   return *this;
}


/**
 * Set the contained SpacePoint pointer
 *
 * @param value The object to point to
 */
void SpacePointOptions::SetSpacePoint(SpacePoint *value)
{
   mSpacePoint = value;
   ColorsFromMySpacePoint();
}


/**
 * Shortcut for setting options to hide the object
 */
void SpacePointOptions::Hide()
{
   SetDrawObject(false);
   SetDrawTrajectory(false);
   SetDrawAxes(false);
   SetDrawXYPlane(false);
   SetDrawLabel(false);
   SetUsePropagateLabel(false);
   SetDrawCenter(false);
   SetDrawEnds(false);
   SetDrawVelocity(false);
   SetDrawGrid(false);
}


/**
 * Set the default and current orbit colors and target colors from the spacepoint
 * pointer
 */
void SpacePointOptions::ColorsFromMySpacePoint()
{
   if (GetSpacePoint() != nullptr)
   {
      SetCurrentOrbitColor(GetSpacePoint()->GetCurrentOrbitColor());
      SetCurrentTargetColor(GetSpacePoint()->GetCurrentTargetColor());
   }
}
