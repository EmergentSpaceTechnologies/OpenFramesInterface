//$Id$
//------------------------------------------------------------------------------
//                                  OpenFramesView
//------------------------------------------------------------------------------
// OpenFramesInterface Plugin for GMAT (General Mission Analysis Tool)
//
// Copyright (c) 2022 Emergent Space Technologies, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http ://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Developed by Emergent Space Technologies, Inc. under contract number
// NNX16CG16C
//
// Author: Matthew Ruschmann, Emergent Space Technologies, Inc.
// Created: October 15, 2017
/**
 *  OpenFramesView class
 *  Manages parameters associated with an OpenFrames view.
 */
//------------------------------------------------------------------------------

#include "OpenFramesView.hpp"

#include "SubscriberException.hpp"
#include "TextParser.hpp"


/// Names of parameters for OpenFramesView
const std::string
      OpenFramesView::PARAMETER_TEXT[OpenFramesViewParamCount - GmatBaseParamCount] =
{
   "ViewFrame",
   "ViewTrajectory",
   "InertialFrame",
   "LookAtFrame",
   "ShortestAngle",
   "SetDefaultLocation",
   "DefaultEye",
   "DefaultCenter",
   "DefaultUp",
   "SetCurrentLocation",
   "CurrentEye",
   "CurrentCenter",
   "CurrentUp",
   "FOVy",
   // These are deprecated and are mapped to DefaultEye, etc..
   "SetCameraLocation",
   "CameraEye",
   "CameraCenter",
   "CameraUp",
};


/// Gmat interpreter types for the parameters in @ref OpenFramesView::PARAMETER_TEXT
const Gmat::ParameterType
      OpenFramesView::PARAMETER_TYPE[OpenFramesViewParamCount - GmatBaseParamCount] =
{
   Gmat::OBJECT_TYPE,       //"ViewFrame"
   Gmat::ON_OFF_TYPE,       //"ViewTrajectory"
   Gmat::ON_OFF_TYPE,       //"InertialFrame"
   Gmat::OBJECT_TYPE,       //"LookAtFrame"
   Gmat::ON_OFF_TYPE,       //"ShortestAngle"
   Gmat::ON_OFF_TYPE,       //"SetDefaultLocation"
   Gmat::RVECTOR_TYPE,      //"DefaultEye"
   Gmat::RVECTOR_TYPE,      //"DefaultCenter"
   Gmat::RVECTOR_TYPE,      //"DefaultUp"
   Gmat::ON_OFF_TYPE,       //"SetCurrentLocation"
   Gmat::RVECTOR_TYPE,      //"CurrentEye"
   Gmat::RVECTOR_TYPE,      //"CurrentCenter"
   Gmat::RVECTOR_TYPE,      //"CurrentUp"
   Gmat::REAL_TYPE,         //"FOVy"
   // These are deprecated and are mapped to DefaultEye, etc..
   Gmat::ON_OFF_TYPE,       //"SetCameraLocation"
   Gmat::RVECTOR_TYPE,      //"CameraEye"
   Gmat::RVECTOR_TYPE,      //"CameraCenter"
   Gmat::RVECTOR_TYPE,      //"CameraUp"
};


/// String that represents On
const std::string OpenFramesView::ON_STRING = "On";


/// String that represents OFF
const std::string OpenFramesView::OFF_STRING = "Off";


/// String that represents the root reference frame
const std::string OpenFramesView::ROOT_FRAME_STRING = "CoordinateSystem";



//------------------------------------------------------------------------------
// OpenFramesView(const std::string &type, const std::string &name)
//------------------------------------------------------------------------------
/**
 *  Constructor
 */
//------------------------------------------------------------------------------
OpenFramesView::OpenFramesView(const std::string &type, const std::string &name) :
   GmatBase(GmatType::GetTypeId("OpenFramesView"), type, name),
   mViewFrame(OpenFramesView::ROOT_FRAME_STRING),
   mUseAbsoluteFrame(false),
   mUseTrajectory(false),
   mUseShortestAngle(false),
   mUseDefault(false),
   mDefaultEye(3),
   mDefaultCenter(3),
   mDefaultUp(3),
   mUseCurrent(false),
   mCurrentEye(3),
   mCurrentCenter(3),
   mCurrentUp(3),
   mFOVy(45.0)
{
   objectTypeNames.push_back("OpenFramesView");
   objectTypes.push_back(GmatBase::type);

   parameterCount = OpenFramesViewParamCount;

   mDefaultEye.SetElement(0, 0.0);
   mDefaultEye.SetElement(1, -1.0);
   mDefaultEye.SetElement(2, 0.0);
   mDefaultCenter.SetElement(0, 0.0);
   mDefaultCenter.SetElement(1, 0.0);
   mDefaultCenter.SetElement(2, 0.0);
   mDefaultUp.SetElement(0, 0.0);
   mDefaultUp.SetElement(1, 0.0);
   mDefaultUp.SetElement(2, 1.0);

   mCurrentEye.SetElement(0, 0.0);
   mCurrentEye.SetElement(1, -1.0);
   mCurrentEye.SetElement(2, 0.0);
   mCurrentCenter.SetElement(0, 0.0);
   mCurrentCenter.SetElement(1, 0.0);
   mCurrentCenter.SetElement(2, 0.0);
   mCurrentUp.SetElement(0, 0.0);
   mCurrentUp.SetElement(1, 0.0);
   mCurrentUp.SetElement(2, 1.0);
}


//------------------------------------------------------------------------------
// ~OpenFramesView(void)
//------------------------------------------------------------------------------
/**
 * Destructor.
 */
//------------------------------------------------------------------------------
OpenFramesView::~OpenFramesView(void)
{
}


//------------------------------------------------------------------------------
// OpenFramesView(const OpenFramesView &rf)
//------------------------------------------------------------------------------
/**
 * Copy Constructor
 */
//------------------------------------------------------------------------------
OpenFramesView::OpenFramesView(const OpenFramesView &dc) :
   GmatBase(dc)
{
   mViewFrame = dc.mViewFrame;
   mLookAtFrame = dc.mLookAtFrame;
   mUseAbsoluteFrame = dc.mUseAbsoluteFrame;
   mUseTrajectory = dc.mUseTrajectory;
   mUseShortestAngle = dc.mUseShortestAngle;
   mUseDefault = dc.mUseDefault;
   mDefaultEye = dc.mDefaultEye;
   mDefaultCenter = dc.mDefaultCenter;
   mDefaultUp = dc.mDefaultUp;
   mUseCurrent = dc.mUseCurrent;
   mCurrentEye = dc.mCurrentEye;
   mCurrentCenter = dc.mCurrentCenter;
   mCurrentUp = dc.mCurrentUp;
   mFOVy = dc.mFOVy;

   parameterCount = OpenFramesViewParamCount;
}


//------------------------------------------------------------------------------
// OpenFramesView& OpenFramesView::operator=(const OpenFramesView& dc)
//------------------------------------------------------------------------------
/**
 * Assignment operator
 */
//------------------------------------------------------------------------------
OpenFramesView& OpenFramesView::operator=(const OpenFramesView& dc)
{
   if (this == &dc) {
      return *this;
   }

   GmatBase::operator=(dc);

   mViewFrame = dc.mViewFrame;
   mLookAtFrame = dc.mLookAtFrame;
   mUseAbsoluteFrame = dc.mUseAbsoluteFrame;
   mUseTrajectory = dc.mUseTrajectory;
   mUseShortestAngle = dc.mUseShortestAngle;
   mUseDefault = dc.mUseDefault;
   mDefaultEye = dc.mDefaultEye;
   mDefaultCenter = dc.mDefaultCenter;
   mDefaultUp = dc.mDefaultUp;
   mUseCurrent = dc.mUseCurrent;
   mCurrentEye = dc.mCurrentEye;
   mCurrentCenter = dc.mCurrentCenter;
   mCurrentUp = dc.mCurrentUp;

   return *this;
}


//------------------------------------------------------------------------------
//  bool Validate()
//------------------------------------------------------------------------------
/**
 * Performs any pre-run validation that the object needs.
 *
 * @return true unless validation fails.
 */
//------------------------------------------------------------------------------
bool OpenFramesView::Validate()
{
   return GmatBase::Validate();
}


//------------------------------------------------------------------------------
// virtual bool Initialize()
/**
 * Initialize an instance of the interface, or reinitailize an instance
 *
 * @return true for all logic paths
 */
//------------------------------------------------------------------------------
bool OpenFramesView::Initialize()
{
   return GmatBase::Initialize();
}


//------------------------------------------------------------------------------
//  GmatBase* Clone(void) const
//------------------------------------------------------------------------------
/**
 * This method returns a clone of the OpenFramesView.
 *
 * @return clone of the OpenFramesView.
 */
//------------------------------------------------------------------------------
GmatBase* OpenFramesView::Clone() const
{
   return (new OpenFramesView(*this));
}


//---------------------------------------------------------------------------
// void Copy(const GmatBase* orig)
//---------------------------------------------------------------------------
/**
 * Sets this object to match another one.
 *
 * @param orig The original that is being copied.
 */
//---------------------------------------------------------------------------
void OpenFramesView::Copy(const GmatBase* orig)
{
   operator=(*((OpenFramesView *)(orig)));
}


//------------------------------------------------------------------------------
// virtual bool TakeAction(const std::string &action,
//                         const std::string &actionData = "");
//------------------------------------------------------------------------------
/**
 * This method performs action.
 *
 * @param <action> action to perform
 * @param <actionData> action data associated with action
 * @return true if action successfully performed
 *
 */
//------------------------------------------------------------------------------
bool OpenFramesView::TakeAction(const std::string &action,
                                const std::string &actionData)
{
   return GmatBase::TakeAction(action, actionData);
}


//---------------------------------------------------------------------------
//  bool RenameRefObject(const Gmat::ObjectType type,
//                       const std::string &oldName, const std::string &newName)
//---------------------------------------------------------------------------
bool OpenFramesView::RenameRefObject(const UnsignedInt type,
                                     const std::string &oldName,
                                     const std::string &newName)
{
   bool success = true;

   if (type == Gmat::SPACECRAFT || type == Gmat::GROUND_STATION ||
       type == Gmat::CALCULATED_POINT || type == Gmat::CELESTIAL_BODY || type == Gmat::SPACE_POINT)
   {
      bool renamed = false;
      TextParser parser;

      StringArray mViewFrameParts = parser.SeparateBy(mViewFrame, ".");
      if (mViewFrameParts.size() > 0 && mViewFrameParts[0] == oldName)
      {
         mViewFrame.replace(0, mViewFrameParts[0].size(), newName);
         renamed = true;
      }

      StringArray mLookAtFrameParts = parser.SeparateBy(mLookAtFrame, ".");
      if (mLookAtFrameParts.size() > 0 && mLookAtFrame == oldName)
      {
         mLookAtFrame.replace(0, mLookAtFrameParts[0].size(), newName);
         renamed = true;
      }

      if (!renamed)
         success = GmatBase::RenameRefObject(type, oldName, newName);
   }
   else
   {
      success = GmatBase::RenameRefObject(type, oldName, newName);
   }

   return success;
}


//------------------------------------------------------------------------------
// std::string GetParameterText(const Integer id) const
//------------------------------------------------------------------------------
std::string OpenFramesView::GetParameterText(const Integer id) const
{
   if (id >= GmatBaseParamCount && id < OpenFramesViewParamCount)
      return PARAMETER_TEXT[id - GmatBaseParamCount];
   else
      return GmatBase::GetParameterText(id);

}


//------------------------------------------------------------------------------
// Integer GetParameterID(const std::string &str) const
//------------------------------------------------------------------------------
Integer OpenFramesView::GetParameterID(const std::string &str) const
{
   for (int i = GmatBaseParamCount; i < OpenFramesViewParamCount; i++)
   {
      if (str == PARAMETER_TEXT[i - GmatBaseParamCount])
         return i;
   }

   return GmatBase::GetParameterID(str);
}


//------------------------------------------------------------------------------
// Gmat::ParameterType GetParameterType(const Integer id) const
//------------------------------------------------------------------------------
Gmat::ParameterType OpenFramesView::GetParameterType(const Integer id) const
{
   if (id >= GmatBaseParamCount && id < OpenFramesViewParamCount)
      return PARAMETER_TYPE[id - GmatBaseParamCount];
   else
      return GmatBase::GetParameterType(id);
}


//------------------------------------------------------------------------------
// std::string GetParameterTypeString(const Integer id) const
//------------------------------------------------------------------------------
std::string OpenFramesView::GetParameterTypeString(const Integer id) const
{
   return GmatBase::PARAM_TYPE_STRING[GetParameterType(id)];
}


//---------------------------------------------------------------------------
//  bool IsParameterReadOnly(const Integer id) const
//---------------------------------------------------------------------------
bool OpenFramesView::IsParameterReadOnly(const Integer id) const
{
   return GmatBase::IsParameterReadOnly(id);
}


//------------------------------------------------------------------------------
// std::string GetStringParameter(const Integer id) const
//------------------------------------------------------------------------------
std::string OpenFramesView::GetStringParameter(const Integer id) const
{
   switch (id)
   {
   case VIEW_FRAME:
      return mViewFrame;
   case LOOK_AT_FRAME:
      return mLookAtFrame;
   default:
      return GmatBase::GetStringParameter(id);
   }
}


//------------------------------------------------------------------------------
// std::string GetStringParameter(const std::string &label) const
//------------------------------------------------------------------------------
std::string OpenFramesView::GetStringParameter(const std::string &label) const
{
   return GetStringParameter(GetParameterID(label));
}


//------------------------------------------------------------------------------
// bool SetStringParameter(const Integer id, const std::string &value)
//------------------------------------------------------------------------------
bool OpenFramesView::SetStringParameter(const Integer id, const std::string &value)
{
   switch (id)
   {
   case VIEW_FRAME:
   {
      mViewFrame = value;
      return true;
   }
   case LOOK_AT_FRAME:
   {
      mLookAtFrame = value;
      return true;
   }
   default:
      return GmatBase::SetStringParameter(id, value);
   }
}


//------------------------------------------------------------------------------
// bool SetStringParameter(const std::string &label, const std::string &value)
//------------------------------------------------------------------------------
bool OpenFramesView::SetStringParameter(const std::string &label,
                                        const std::string &value)
{
   return SetStringParameter(GetParameterID(label), value);
}


//---------------------------------------------------------------------------
//  std::string GetOnOffParameter(const Integer id) const
//---------------------------------------------------------------------------
std::string OpenFramesView::GetOnOffParameter(const Integer id) const
{
   switch (id)
   {
   case INERTIAL_FRAME:
      return BooleanToOnOff(mUseAbsoluteFrame);
   case VIEW_TRAJECTORY:
      return BooleanToOnOff(mUseTrajectory);
   case SHORTEST_ANGLE:
      return BooleanToOnOff(mUseShortestAngle);
   case SPECIFY_DEFAULT:
   case SPECIFY_CAMERA:
      return BooleanToOnOff(mUseDefault);
   case SPECIFY_CURRENT:
      return BooleanToOnOff(mUseCurrent);
   default:
      return GmatBase::GetOnOffParameter(id);
   }
}


//------------------------------------------------------------------------------
// std::string OrbitView::GetOnOffParameter(const std::string &label) const
//------------------------------------------------------------------------------
std::string OpenFramesView::GetOnOffParameter(const std::string &label) const
{
   return GetOnOffParameter(GetParameterID(label));
}


//---------------------------------------------------------------------------
//  bool SetOnOffParameter(const Integer id, const std::string &value)
//---------------------------------------------------------------------------
bool OpenFramesView::SetOnOffParameter(const Integer id, const std::string &value)
{
   switch (id)
   {
   case INERTIAL_FRAME:
      mUseAbsoluteFrame = (value == ON_STRING);
      return true;
   case VIEW_TRAJECTORY:
      mUseTrajectory = (value == ON_STRING);
      return true;
   case SHORTEST_ANGLE:
      mUseShortestAngle = (value == ON_STRING);
      return true;
   case SPECIFY_DEFAULT:
   case SPECIFY_CAMERA:
      mUseDefault = (value == ON_STRING);
      return true;
   case SPECIFY_CURRENT:
      mUseCurrent = (value == ON_STRING);
      return true;
   default:
      return GmatBase::SetOnOffParameter(id, value);
   }
}


//------------------------------------------------------------------------------
// bool SetOnOffParameter(const std::string &label, const std::string &value)
//------------------------------------------------------------------------------
bool OpenFramesView::SetOnOffParameter(const std::string &label,
                                  const std::string &value)
{
   return SetOnOffParameter(GetParameterID(label), value);
}

//---------------------------------------------------------------------------
//  Real GetRealParameter(const Integer id) const
//---------------------------------------------------------------------------
Real OpenFramesView::GetRealParameter(const Integer id) const
{
  switch (id)
  {
    case FOV_Y:
      return mFOVy;

    default:
      return GmatBase::GetRealParameter(id);
  }
}

//------------------------------------------------------------------------------
// Real OrbitView::GetRealParameter(const std::string &label) const
//------------------------------------------------------------------------------
Real OpenFramesView::GetRealParameter(const std::string &label) const
{
  return GetRealParameter(GetParameterID(label));
}

//---------------------------------------------------------------------------
//  Real GetRealParameter(const Integer id, const Integer index) const
//---------------------------------------------------------------------------
Real OpenFramesView::GetRealParameter(const Integer id, const Integer index) const
{
   switch (id)
   {
   case DEFAULT_EYE:
   case CAMERA_EYE:
      if (index < mDefaultEye.GetSize())
         return mDefaultEye.GetElement(index);
      else
         return 0.0;
   case DEFAULT_CENTER:
   case CAMERA_CENTER:
      if (index < mDefaultCenter.GetSize())
         return mDefaultCenter.GetElement(index);
      else
         return 0.0;
   case DEFAULT_UP:
   case CAMERA_UP:
      if (index < mDefaultUp.GetSize())
         return mDefaultUp.GetElement(index);
      else
         return 0.0;
   case CURRENT_EYE:
      if (index < mCurrentEye.GetSize())
         return mCurrentEye.GetElement(index);
      else
         return 0.0;
   case CURRENT_CENTER:
      if (index < mCurrentCenter.GetSize())
         return mCurrentCenter.GetElement(index);
      else
         return 0.0;
   case CURRENT_UP:
      if (index < mCurrentUp.GetSize())
         return mCurrentUp.GetElement(index);
      else
         return 0.0;
   default:
      return GmatBase::GetRealParameter(id, index);
   }
}


//------------------------------------------------------------------------------
// Real OrbitView::GetRealParameter(const std::string &label, const Integer index) const
//------------------------------------------------------------------------------
Real OpenFramesView::GetRealParameter(const std::string &label,
                                      const Integer index) const
{
   return GetRealParameter(GetParameterID(label), index);
}

//---------------------------------------------------------------------------
//  Real SetRealParameter(const Integer id, const Real value)
//---------------------------------------------------------------------------
Real OpenFramesView::SetRealParameter(const Integer id, const Real value)
{
  switch (id)
  {
    case FOV_Y:
      if ((value > 0.0) && (value < 180.0))
      {
        mFOVy = value;
        return value;
      }
      else
      {
        std::string valueStr = std::to_string(value);
        SubscriberException se;
        se.SetDetails(errorMessageFormat.c_str(), valueStr.c_str(),
          GetParameterText(id).c_str(), "0.0 < Real Value < 180.0");
        throw se;
      }

    default:
      return GmatBase::SetRealParameter(id, value);
  }
}

//------------------------------------------------------------------------------
// Real SetRealParameter(const std::string &label, const Real value);
//------------------------------------------------------------------------------
Real OpenFramesView::SetRealParameter(const std::string &label, const Real value)
{
  return SetRealParameter(GetParameterID(label), value);
}

//---------------------------------------------------------------------------
//  Real SetRealParameter(const Integer id, const Real value,
//                        const Integer index)
//---------------------------------------------------------------------------
Real OpenFramesView::SetRealParameter(const Integer id, const Real value,
                                      const Integer index)
{
   switch (id)
   {
   case DEFAULT_EYE:
   case CAMERA_EYE:
      if (index < 3)
      {
         mDefaultEye.SetElement(index, value);
         return value;
      }
      else
      {
         std::string value = std::to_string(index);
         SubscriberException se;
         se.SetDetails(errorMessageFormat.c_str(), value.c_str(),
                       GetParameterText(id).c_str(), "Invalid row and/or column");
         throw se;
      }
   case DEFAULT_CENTER:
   case CAMERA_CENTER:
      if (index < 3)
      {
         mDefaultCenter.SetElement(index, value);
         return value;
      }
      else
      {
         std::string value = std::to_string(index);
         SubscriberException se;
         se.SetDetails(errorMessageFormat.c_str(), value.c_str(),
                       GetParameterText(id).c_str(), "Invalid row and/or column");
         throw se;
      }
   case DEFAULT_UP:
   case CAMERA_UP:
      if (index < 3)
      {
         mDefaultUp.SetElement(index, value);
         return value;
      }
      else
      {
         std::string value = std::to_string(index);
         SubscriberException se;
         se.SetDetails(errorMessageFormat.c_str(), value.c_str(),
                       GetParameterText(id).c_str(), "Invalid row and/or column");
         throw se;
      }
   case CURRENT_EYE:
      if (index < 3)
      {
         mCurrentEye.SetElement(index, value);
         return value;
      }
      else
      {
         std::string value = std::to_string(index);
         SubscriberException se;
         se.SetDetails(errorMessageFormat.c_str(), value.c_str(),
                       GetParameterText(id).c_str(), "Invalid row and/or column");
         throw se;
      }
   case CURRENT_CENTER:
      if (index < 3)
      {
         mCurrentCenter.SetElement(index, value);
         return value;
      }
      else
      {
         std::string value = std::to_string(index);
         SubscriberException se;
         se.SetDetails(errorMessageFormat.c_str(), value.c_str(),
                       GetParameterText(id).c_str(), "Invalid row and/or column");
         throw se;
      }
   case CURRENT_UP:
      if (index < 3)
      {
         mCurrentUp.SetElement(index, value);
         return value;
      }
      else
      {
         std::string value = std::to_string(index);
         SubscriberException se;
         se.SetDetails(errorMessageFormat.c_str(), value.c_str(),
                       GetParameterText(id).c_str(), "Invalid row and/or column");
         throw se;
      }
   default:
      return GmatBase::SetRealParameter(id, value, index);
   }
}


//------------------------------------------------------------------------------
// Real SetRealParameter(const std::string &label, const Real value,
//                       const Integer index);
//------------------------------------------------------------------------------
Real OpenFramesView::SetRealParameter(const std::string &label,
                                      const Real value, const Integer index)
{
   return SetRealParameter(GetParameterID(label), value, index);
}


//---------------------------------------------------------------------------
// const Rvector& GetRvectorParameter(const Integer id) const
//---------------------------------------------------------------------------
const Rvector &OpenFramesView::GetRvectorParameter(const Integer id) const
{
   switch (id)
   {
   case DEFAULT_EYE:
   case CAMERA_EYE:
      return mDefaultEye;
   case DEFAULT_CENTER:
   case CAMERA_CENTER:
      return mDefaultCenter;
   case DEFAULT_UP:
   case CAMERA_UP:
      return mDefaultUp;
   case CURRENT_EYE:
      return mCurrentEye;
   case CURRENT_CENTER:
      return mCurrentCenter;
   case CURRENT_UP:
      return mCurrentUp;
   default:
      return GmatBase::GetRvectorParameter(id);
   }
}


//------------------------------------------------------------------------------
// const Rvector& GetRvectorParameter(const std::string &label) const
//------------------------------------------------------------------------------
const Rvector &OpenFramesView::GetRvectorParameter(const std::string &label) const
{
   return GetRvectorParameter(GetParameterID(label));
}


//---------------------------------------------------------------------------
//  const Rvector& SetRvectorParameter(const Integer id, const Rmatrix &value)
//---------------------------------------------------------------------------
const Rvector &OpenFramesView::SetRvectorParameter(const Integer id,
                                                   const Rvector &value)
{
   switch (id)
   {
   case DEFAULT_EYE:
   case CAMERA_EYE:
      if (value.GetSize() == 3)
      {
         mDefaultEye = value;
         return mDefaultEye;
      }
      else
      {
         std::string val = std::to_string(value.GetSize());
         SubscriberException se;
         se.SetDetails(errorMessageFormat.c_str(), val.c_str(),
                       "ViewEye", "Invalid size of source matrix");
         throw se;
      }
   case DEFAULT_CENTER:
   case CAMERA_CENTER:
      if (value.GetSize() == 3)
      {
         mDefaultCenter = value;
         return mDefaultCenter;
      }
      else
      {
         std::string val = std::to_string(value.GetSize());
         SubscriberException se;
         se.SetDetails(errorMessageFormat.c_str(), val.c_str(),
                       "ViewCenter", "Invalid size of source matrix");
         throw se;
      }
   case DEFAULT_UP:
   case CAMERA_UP:
      if (value.GetSize() == 3)
      {
         mDefaultUp = value;
         return mDefaultUp;
      }
      else
      {
         std::string val = std::to_string(value.GetSize());
         SubscriberException se;
         se.SetDetails(errorMessageFormat.c_str(), val.c_str(),
                       "ViewUp", "Invalid size of source matrix");
         throw se;
      }
   case CURRENT_EYE:
      if (value.GetSize() == 3)
      {
         mCurrentEye = value;
         return mCurrentEye;
      }
      else
      {
         std::string val = std::to_string(value.GetSize());
         SubscriberException se;
         se.SetDetails(errorMessageFormat.c_str(), val.c_str(),
                       "ViewEye", "Invalid size of source matrix");
         throw se;
      }
   case CURRENT_CENTER:
      if (value.GetSize() == 3)
      {
         mCurrentCenter = value;
         return mCurrentCenter;
      }
      else
      {
         std::string val = std::to_string(value.GetSize());
         SubscriberException se;
         se.SetDetails(errorMessageFormat.c_str(), val.c_str(),
                       "ViewCenter", "Invalid size of source matrix");
         throw se;
      }
   case CURRENT_UP:
      if (value.GetSize() == 3)
      {
         mCurrentUp = value;
         return mCurrentUp;
      }
      else
      {
         std::string val = std::to_string(value.GetSize());
         SubscriberException se;
         se.SetDetails(errorMessageFormat.c_str(), val.c_str(),
                       "ViewUp", "Invalid size of source matrix");
         throw se;
      }
   default:
      return GmatBase::SetRvectorParameter(id, value);
   }
}


//------------------------------------------------------------------------------
// const Rvector& SetRvectorParameter(const std::string &label, const Rmatrix &value)
//------------------------------------------------------------------------------
const Rvector &OpenFramesView::SetRvectorParameter(const std::string &label,
                                                   const Rvector &value)
{
   return SetRvectorParameter(GetParameterID(label), value);
}


//------------------------------------------------------------------------------
// bool IsParameterCloaked(const Integer id) const
//------------------------------------------------------------------------------
bool OpenFramesView::IsParameterCloaked(const Integer id) const
{
   if (id == SHORTEST_ANGLE)
      return mLookAtFrame.size() == 0; 
   if (id == DEFAULT_EYE || id == DEFAULT_CENTER || id == DEFAULT_UP)
      return ( !mUseDefault );
   if (id == CURRENT_EYE || id == CURRENT_CENTER || id == CURRENT_UP)
      return ( !mUseCurrent );
   if (id == SPECIFY_CAMERA || id == CAMERA_EYE || id == CAMERA_CENTER || id == CAMERA_UP)
      return ( true ); // deprecated
   else
      return GmatBase::IsParameterCloaked(id);
}


//------------------------------------------------------------------------------
// bool IsParameterCloaked(const std::string &label) const
//------------------------------------------------------------------------------
bool OpenFramesView::IsParameterCloaked(const std::string &label) const
{
   return IsParameterCloaked(GetParameterID(label));
}


//---------------------------------------------------------------------------
// Gmat::ObjectType GetPropertyObjectType(const Integer id) const
//---------------------------------------------------------------------------
UnsignedInt OpenFramesView::GetPropertyObjectType(const Integer id) const
{
   if (id == VIEW_FRAME)
      return Gmat::SPACE_POINT;

   if (id == LOOK_AT_FRAME)
      return Gmat::SPACE_POINT;

   return GmatBase::GetPropertyObjectType(id);
}


//------------------------------------------------------------------------------
// std::string OpenFramesView::StringArrayToString(const StringArray &array)
//------------------------------------------------------------------------------
/**
 * Converts a boolean to "On" or "Off" string
 *
 * @param onIfTrue The boolean to convert
 * @return "On" if input is true, "Off" otherwise
 */
//---------------------------------------------------------------
std::string OpenFramesView::BooleanToOnOff(bool onIfTrue)
{
   return (onIfTrue) ? ON_STRING : OFF_STRING;
};
