//$Id$
//------------------------------------------------------------------------------
//                                  OpenGLOptions
//------------------------------------------------------------------------------
// OpenFramesInterface Plugin for GMAT (General Mission Analysis Tool)
//
// Copyright (c) 2022 Emergent Space Technologies, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Developed by Emergent Space Technologies, Inc. under contract number
// NNX16CG16C
//
// Author: Matthew Ruschmann, Emergent Space Technologies, Inc.
// Created: August 24, 2017
/**
 *  OFSpacecraft class
 *  Conveniently wraps a Spacepoint with associated options.
 */
//------------------------------------------------------------------------------

#include "OpenGLOptions.hpp"

#ifdef OFI_USE_WXWIDGETS

#include <wx/glcanvas.h>

#if defined(__WXMSW__) &&  !wxCHECK_VERSION(3,1,0)
// IsDisplaySupported() for wxWidgets 3.0.x returns false for all
// WX_GL_SAMPLES!=0. Therefore, windows specific code was copied from
// wxWidgets 3.1.0.

#ifndef WGL_ARB_pixel_format
#define WGL_ARB_pixel_format
#define WGL_DRAW_TO_WINDOW_ARB            0x2001
#define WGL_DRAW_TO_BITMAP_ARB            0x2002
#define WGL_ACCELERATION_ARB              0x2003
#define WGL_NEED_PALETTE_ARB              0x2004
#define WGL_NEED_SYSTEM_PALETTE_ARB       0x2005
#define WGL_SWAP_LAYER_BUFFERS_ARB        0x2006
#define WGL_SWAP_METHOD_ARB               0x2007
#define WGL_NUMBER_OVERLAYS_ARB           0x2008
#define WGL_NUMBER_UNDERLAYS_ARB          0x2009
#define WGL_SUPPORT_GDI_ARB               0x200F
#define WGL_SUPPORT_OPENGL_ARB            0x2010
#define WGL_DOUBLE_BUFFER_ARB             0x2011
#define WGL_STEREO_ARB                    0x2012
#define WGL_PIXEL_TYPE_ARB                0x2013
#define WGL_COLOR_BITS_ARB                0x2014
#define WGL_RED_BITS_ARB                  0x2015
#define WGL_GREEN_BITS_ARB                0x2017
#define WGL_BLUE_BITS_ARB                 0x2019
#define WGL_ALPHA_BITS_ARB                0x201B
#define WGL_ACCUM_BITS_ARB                0x201D
#define WGL_ACCUM_RED_BITS_ARB            0x201E
#define WGL_ACCUM_GREEN_BITS_ARB          0x201F
#define WGL_ACCUM_BLUE_BITS_ARB           0x2020
#define WGL_ACCUM_ALPHA_BITS_ARB          0x2021
#define WGL_DEPTH_BITS_ARB                0x2022
#define WGL_STENCIL_BITS_ARB              0x2023
#define WGL_AUX_BUFFERS_ARB               0x2024
#define WGL_NO_ACCELERATION_ARB           0x2025
#define WGL_GENERIC_ACCELERATION_ARB      0x2026
#define WGL_FULL_ACCELERATION_ARB         0x2027
#define WGL_SWAP_EXCHANGE_ARB             0x2028
#define WGL_SWAP_COPY_ARB                 0x2029
#define WGL_TYPE_RGBA_ARB                 0x202B
#define WGL_TYPE_COLORINDEX_ARB           0x202C
#endif

#ifndef WGL_ARB_multisample
#define WGL_ARB_multisample
#define WGL_SAMPLE_BUFFERS_ARB            0x2041
#define WGL_SAMPLES_ARB                   0x2042
#endif

// this macro defines a variable of type "name_t" called "name" and initializes
// it with the pointer to WGL function "name" (which may be NULL)
#define wxDEFINE_WGL_FUNC(name) \
    name##_t name = (name##_t)wglGetProcAddress(#name)

// A dummy window, needed at FindMatchingPixelFormat()
class wxGLdummyWin : public wxWindow
{
public:
    wxGLdummyWin()
    {
        hdc = 0;
        CreateBase(NULL, wxID_ANY);
        DWORD msflags = WS_CLIPSIBLINGS | WS_CLIPCHILDREN;
        if( MSWCreate(wxApp::GetRegisteredClassName(wxT("wxGLCanvas"), -1, CS_OWNDC),
                      NULL, wxDefaultPosition, wxDefaultSize, msflags, 0) )
        {
            hdc = ::GetDC(GetHWND());
        }
    }
    ~wxGLdummyWin()
    {
        if ( hdc )
            ::ReleaseDC(GetHWND(), hdc);
    }
    HDC hdc;
};

// Fills PIXELFORMATDESCRIPTOR struct
static void SetPFDForAttributes(PIXELFORMATDESCRIPTOR& pfd, const int* attrsListWGL)
{
    // Some defaults
    pfd.nSize =  sizeof(PIXELFORMATDESCRIPTOR);
    pfd.nVersion = 1;
    pfd.iPixelType = PFD_TYPE_RGBA;
    pfd.iLayerType = PFD_MAIN_PLANE; // For very early MSW OpenGL

    // We can meet some WGL_XX values not managed by wx. But the user
    // may require them. Allow here those that are also used for pfd.
    // Color shift and transparency are not handled.
    for ( int arg = 0; attrsListWGL[arg]; )
    {
        switch ( attrsListWGL[arg++] )
        {
            case WGL_DRAW_TO_WINDOW_ARB:
                if ( attrsListWGL[arg++] )
                    pfd.dwFlags |= PFD_DRAW_TO_WINDOW;
                break;

            case WGL_DRAW_TO_BITMAP_ARB:
                if ( attrsListWGL[arg++] )
                    pfd.dwFlags |= PFD_DRAW_TO_BITMAP;
                break;

            case WGL_ACCELERATION_ARB:
                if ( attrsListWGL[arg++] == WGL_GENERIC_ACCELERATION_ARB )
                    pfd.dwFlags |= PFD_GENERIC_ACCELERATED;
                break;

            case WGL_NEED_PALETTE_ARB:
                if ( attrsListWGL[arg++] )
                    pfd.dwFlags |= PFD_NEED_PALETTE;
                break;

            case WGL_NEED_SYSTEM_PALETTE_ARB:
                if ( attrsListWGL[arg++] )
                    pfd.dwFlags |= PFD_NEED_SYSTEM_PALETTE;
                break;

            case WGL_SWAP_LAYER_BUFFERS_ARB:
                if ( attrsListWGL[arg++] )
                    pfd.dwFlags |= PFD_SWAP_LAYER_BUFFERS;
                break;

            case WGL_SWAP_METHOD_ARB:
                if ( attrsListWGL[arg++] == WGL_SWAP_EXCHANGE_ARB )
                    pfd.dwFlags |= PFD_SWAP_EXCHANGE;
                else if ( attrsListWGL[arg] == WGL_SWAP_COPY_ARB )
                    pfd.dwFlags |= PFD_SWAP_COPY;
                break;

            case WGL_NUMBER_OVERLAYS_ARB:
                pfd.bReserved &= 240;
                pfd.bReserved |= attrsListWGL[arg++] & 15;
                break;

            case WGL_NUMBER_UNDERLAYS_ARB:
                pfd.bReserved &= 15;
                pfd.bReserved |= attrsListWGL[arg++] & 240;
                break;

            case WGL_SUPPORT_GDI_ARB:
                if ( attrsListWGL[arg++] )
                    pfd.dwFlags |= PFD_SUPPORT_GDI;
                break;

            case WGL_SUPPORT_OPENGL_ARB:
                if ( attrsListWGL[arg++] )
                    pfd.dwFlags |= PFD_SUPPORT_OPENGL;
                break;

            case WGL_DOUBLE_BUFFER_ARB:
                if ( attrsListWGL[arg++] )
                    pfd.dwFlags |= PFD_DOUBLEBUFFER;
                break;

            case WGL_STEREO_ARB:
                if ( attrsListWGL[arg++] )
                    pfd.dwFlags |= PFD_STEREO;
                break;

            case WGL_PIXEL_TYPE_ARB:
                if ( attrsListWGL[arg++] == WGL_TYPE_RGBA_ARB )
                    pfd.iPixelType = PFD_TYPE_RGBA;
                else
                    pfd.iPixelType = PFD_TYPE_COLORINDEX;
                break;

            case WGL_COLOR_BITS_ARB:
                pfd.cColorBits = attrsListWGL[arg++];
                break;

            case WGL_RED_BITS_ARB:
                pfd.cRedBits = attrsListWGL[arg++];
                break;

            case WGL_GREEN_BITS_ARB:
                pfd.cGreenBits = attrsListWGL[arg++];
                break;

            case WGL_BLUE_BITS_ARB:
                pfd.cBlueBits = attrsListWGL[arg++];
                break;

            case WGL_ALPHA_BITS_ARB:
                pfd.cAlphaBits = attrsListWGL[arg++];
                break;

            case WGL_ACCUM_BITS_ARB:
                pfd.cAccumBits = attrsListWGL[arg++];
                break;

            case WGL_ACCUM_RED_BITS_ARB:
                pfd.cAccumRedBits = attrsListWGL[arg++];
                break;

            case WGL_ACCUM_GREEN_BITS_ARB:
                pfd.cAccumGreenBits = attrsListWGL[arg++];
                break;

            case WGL_ACCUM_BLUE_BITS_ARB:
                pfd.cAccumBlueBits = attrsListWGL[arg++];
                break;

            case WGL_ACCUM_ALPHA_BITS_ARB:
                pfd.cAccumAlphaBits = attrsListWGL[arg++];
                break;

            case WGL_DEPTH_BITS_ARB:
                pfd.cDepthBits = attrsListWGL[arg++];
                break;

            case WGL_STENCIL_BITS_ARB:
                pfd.cStencilBits = attrsListWGL[arg++];
                break;

            case WGL_AUX_BUFFERS_ARB:
                pfd.cAuxBuffers = attrsListWGL[arg++];
                break;

            default:
                // ignore
                break;
        }
    }
}

/* static */
bool ParseAttribList(const int *attribList, wxVector<int>& dispAttrs)
{
    bool needsArb = false;
    // Some attributes are usually needed
    dispAttrs.push_back(WGL_DRAW_TO_WINDOW_ARB);
    dispAttrs.push_back(GL_TRUE);
    dispAttrs.push_back(WGL_SUPPORT_OPENGL_ARB);
    dispAttrs.push_back(GL_TRUE);
    dispAttrs.push_back(WGL_ACCELERATION_ARB);
    dispAttrs.push_back(WGL_FULL_ACCELERATION_ARB);

    if ( !attribList )
    {
        // Default visual attributes used in wx versions before wx3.1
        //dispAttrs.AddDefaultsForWXBefore31();
        dispAttrs.push_back(0);
        return false;
    }

    int src = 0;
    int minColo[4] = {-1, -1, -1, -1};
    int minAcum[4] = {-1, -1, -1, -1};
    int num = 0;
    while ( attribList[src] )
    {
        // Check a non zero-terminated list. This may help a bit with malformed lists.
        if ( ++num > 200 )
        {
            wxFAIL_MSG("The attributes list is not zero-terminated");
        }

        switch ( attribList[src++] )
        {
            // Pixel format attributes

            case WX_GL_RGBA:
                dispAttrs.push_back(WGL_PIXEL_TYPE_ARB);
                dispAttrs.push_back(WGL_TYPE_RGBA_ARB);
                break;

            case WX_GL_BUFFER_SIZE:
            {
                int val = attribList[src++];
                if ( val >= 0 )
                {
                    dispAttrs.push_back(WGL_COLOR_BITS_ARB);
                    dispAttrs.push_back(val);
                }
                break;
            }

            case WX_GL_LEVEL:
            {
                int val = attribList[src++];
                if ( val >= 0 )
                {
                    dispAttrs.push_back(WGL_NUMBER_OVERLAYS_ARB);
                    dispAttrs.push_back(val);
                }
                else if ( val < 0 )
                {
                    dispAttrs.push_back(WGL_NUMBER_UNDERLAYS_ARB);
                    dispAttrs.push_back(-val);
                }
                if ( val < -1 || val > 1 )
                {
                    needsArb = true;
                }
                break;
            }

            case WX_GL_DOUBLEBUFFER:
                dispAttrs.push_back(WGL_DOUBLE_BUFFER_ARB);
                dispAttrs.push_back(GL_TRUE);
                break;

            case WX_GL_STEREO:
                dispAttrs.push_back(WGL_STEREO_ARB);
                dispAttrs.push_back(GL_TRUE);
                break;

            case WX_GL_AUX_BUFFERS:
            {
                int val = attribList[src++];
                if ( val >= 0 )
                {
                    dispAttrs.push_back(WGL_AUX_BUFFERS_ARB);
                    dispAttrs.push_back(val);
                }
                break;
            }

            case WX_GL_MIN_RED:
                minColo[0] = attribList[src++];
                break;

            case WX_GL_MIN_GREEN:
                minColo[1] = attribList[src++];
                break;

            case WX_GL_MIN_BLUE:
                minColo[2] = attribList[src++];
                break;

            case WX_GL_MIN_ALPHA:
                minColo[3] = attribList[src++];
                break;

            case WX_GL_DEPTH_SIZE:
            {
                int val = attribList[src++];
                if ( val >= 0 )
                {
                    dispAttrs.push_back(WGL_DEPTH_BITS_ARB);
                    dispAttrs.push_back(val);
                }
                break;
            }

            case WX_GL_STENCIL_SIZE:
            {
                int val = attribList[src++];
                if ( val >= 0 )
                {
                    dispAttrs.push_back(WGL_STENCIL_BITS_ARB);
                    dispAttrs.push_back(val);
                }
                break;
            }

            case WX_GL_MIN_ACCUM_RED:
                minAcum[0] = attribList[src++];
                break;

            case WX_GL_MIN_ACCUM_GREEN:
                minAcum[1] = attribList[src++];
                break;

            case WX_GL_MIN_ACCUM_BLUE:
                minAcum[2] = attribList[src++];
                break;

            case WX_GL_MIN_ACCUM_ALPHA:
                minAcum[3] = attribList[src++];
                break;

            case WX_GL_SAMPLE_BUFFERS:
            {
                int val = attribList[src++];
                if ( val >= 0 )
                {
                    dispAttrs.push_back(WGL_SAMPLE_BUFFERS_ARB);
                    dispAttrs.push_back(val);
                    needsArb = true;
                }
                break;
            }

            case WX_GL_SAMPLES:
            {
                int val = attribList[src++];
                if ( val >= 0 )
                {
                    dispAttrs.push_back(WGL_SAMPLES_ARB);
                    dispAttrs.push_back(val);
                    needsArb = true;
                }
                break;
            }

            default:
                wxFAIL_MSG("Unexpected value in attributes list");
                return false;
        }
    }

    // Set color and accumulation
    if ( minColo[0] >= 0 || minColo[1] >= 0 || minColo[2] >= 0 || minColo[3] >= 0 )
    {
        int mRed = minColo[0];
        int mGreen = minColo[1];
        int mBlue = minColo[2];
        int mAlpha = minColo[3];
        int colorBits = 0;
        if ( mRed >= 0)
        {
            dispAttrs.push_back(WGL_RED_BITS_ARB);
            dispAttrs.push_back(mRed);
            colorBits += mRed;
        }
        if ( mGreen >= 0)
        {
            dispAttrs.push_back(WGL_GREEN_BITS_ARB);
            dispAttrs.push_back(mGreen);
            colorBits += mGreen;
        }
        if ( mBlue >= 0)
        {
            dispAttrs.push_back(WGL_BLUE_BITS_ARB);
            dispAttrs.push_back(mBlue);
            colorBits += mBlue;
        }
        if ( mAlpha >= 0)
        {
            dispAttrs.push_back(WGL_ALPHA_BITS_ARB);
            dispAttrs.push_back(mAlpha);
            // doesn't count in colorBits
        }
        if ( colorBits )
        {
            dispAttrs.push_back(WGL_COLOR_BITS_ARB);
            dispAttrs.push_back(colorBits);
        }
    }
    if ( minAcum[0] >= 0 || minAcum[1] >= 0 || minAcum[2] >= 0 || minAcum[3] >= 0 )
    {
        int mRed = minAcum[0];
        int mGreen = minAcum[1];
        int mBlue = minAcum[2];
        int mAlpha = minAcum[3];
        int colorBits = 0;
        int acumBits = 0;
        if ( mRed >= 0)
        {
            dispAttrs.push_back(WGL_ACCUM_RED_BITS_ARB);
            dispAttrs.push_back(mRed);
            acumBits += mRed;
        }
        if ( mGreen >= 0)
        {
            dispAttrs.push_back(WGL_ACCUM_GREEN_BITS_ARB);
            dispAttrs.push_back(mGreen);
            acumBits += mGreen;
        }
        if ( mBlue >= 0)
        {
            dispAttrs.push_back(WGL_ACCUM_BLUE_BITS_ARB);
            dispAttrs.push_back(mBlue);
            acumBits += mBlue;
        }
        if ( mAlpha >= 0)
        {
            dispAttrs.push_back(WGL_ACCUM_ALPHA_BITS_ARB);
            dispAttrs.push_back(mAlpha);
            acumBits += mAlpha;
        }
        if ( acumBits )
        {
            dispAttrs.push_back(WGL_ACCUM_BITS_ARB);
            dispAttrs.push_back(acumBits);
        }
    }

    // The attributes lists must be zero-terminated
    dispAttrs.push_back(0);

    return needsArb;
}

/* static */
int FindMatchingPixelFormat(const int* attribList,
                            PIXELFORMATDESCRIPTOR* ppfd)
{
    // WGL_XX attributes
    wxVector<int> dispAttrs;
    bool needsArb = ParseAttribList(attribList, dispAttrs);
    const int* attrsListWGL = &*dispAttrs.begin();
    if ( !attrsListWGL )
    {
        wxFAIL_MSG("wxGLAttributes object is empty.");
        return 0;
    }

    // The preferred way is using wglChoosePixelFormatARB. This is not a MSW
    // function, we must ask the GPU driver for a pointer to it. We need a
    // rendering context for this operation. Create a dummy one.
    // Notice that we don't create a wxGLContext on purpose.
    // We meet another issue:
    // Before creating any context, we must set a pixel format for its hdc:
    //   https://msdn.microsoft.com/en-us/library/dd374379%28v=vs.85%29.aspx
    // but we can't set a pixel format more than once:
    //   https://msdn.microsoft.com/en-us/library/dd369049%28v=vs.85%29.aspx
    // To cope with this we need a dummy hidden window.
    //
    // Having this dummy window allows also calling IsDisplaySupported()
    // without creating a wxGLCanvas.
    wxGLdummyWin* dummyWin = new wxGLdummyWin();
    HDC dummyHDC = dummyWin->hdc;
    if ( !dummyHDC )
    {
        dummyWin->Destroy();
        wxFAIL_MSG("Can't create dummy window");
        return 0;
    }
    // Dummy context
    PIXELFORMATDESCRIPTOR dpfd; //any one is valid
    ::SetPixelFormat(dummyHDC, 1, &dpfd); // pixelformat=1, any one is valid
    HGLRC dumctx = ::wglCreateContext(dummyHDC);
    if ( !dumctx )
    {
        dummyWin->Destroy();
        // A fatal error!
        wxFAIL_MSG("wglCreateContext failed!");
        return 0;
    }

    ::wglMakeCurrent(dummyHDC, dumctx);

    typedef BOOL (WINAPI * wglChoosePixelFormatARB_t)
                 (HDC hdc,
                  const int *piAttribIList,
                  const FLOAT *pfAttribFList,
                  UINT nMaxFormats,
                  int *piFormats,
                  UINT *nNumFormats
                 );

    wxDEFINE_WGL_FUNC(wglChoosePixelFormatARB); // get a pointer to it

    // If wglChoosePixelFormatARB is not supported but  the attributes require
    // it, then fail.
    if ( !wglChoosePixelFormatARB && needsArb )
    {
        wxLogLastError("wglChoosePixelFormatARB unavailable");
        // Delete the dummy objects
        ::wglMakeCurrent(NULL, NULL);
        ::wglDeleteContext(dumctx);
        dummyWin->Destroy();
        return 0;
    }

    int pixelFormat = 0;

    if ( !wglChoosePixelFormatARB && !needsArb )
    {
        // Old way
        if ( !ppfd )
        {
            // We have been called from IsDisplaySupported()
            PIXELFORMATDESCRIPTOR pfd;
            SetPFDForAttributes(pfd, attrsListWGL);
            pixelFormat = ::ChoosePixelFormat(dummyHDC, &pfd);
        }
        else
        {
            SetPFDForAttributes(*ppfd, attrsListWGL);
            pixelFormat = ::ChoosePixelFormat(dummyHDC, ppfd);
        }
        // We should ensure that we have got what we have asked for. This can
        // be done using DescribePixelFormat() and comparing attributes.
        // Nevertheless wglChoosePixelFormatARB exists since 2001, so it's
        // very unlikely that ChoosePixelFormat() is used. So, do nothing.
    }
    else
    {
        // New way, using wglChoosePixelFormatARB
        // 'ppfd' is used at wxGLCanvas::Create(). See explanations there.
        if ( ppfd )
            SetPFDForAttributes(*ppfd, attrsListWGL);

        UINT numFormats = 0;

        // Get the first good match
        if ( !wglChoosePixelFormatARB(dummyHDC, attrsListWGL, NULL,
                                      1, &pixelFormat, &numFormats) )
        {
            wxLogLastError("wglChoosePixelFormatARB. Is the list zero-terminated?");
            numFormats = 0;
        }

        // Although TRUE is returned if no matching formats are found (see
        // https://www.opengl.org/registry/specs/ARB/wgl_pixel_format.txt),
        // pixelFormat is not initialized in this case so we need to check
        // for numFormats being not 0 explicitly (however this is not an error
        // so don't call wxLogLastError() here).
        if ( !numFormats )
            pixelFormat = 0;
    }

    // Delete the dummy objects
    ::wglMakeCurrent(NULL, NULL);
    ::wglDeleteContext(dumctx);
    dummyWin->Destroy();

    return pixelFormat;
}

#endif

#endif // OFI_USE_WXWIDGETS

/**
 * Static data members
 * All available OpenGL MSAA samples
 * Indicate static variable is uninitialized
 */
OpenGLOptions::MSAASampleSet OpenGLOptions::mAvailableMSAASamples = {-1};

/**
 * Constructor
 *
 * @param name A name for the instance
 */
OpenGLOptions::OpenGLOptions() :
   mEnableVR(false)
{

   // Initialize display attribute list
   mGLArgs.resize(9);
#ifdef OFI_USE_WXWIDGETS
   mGLArgs = { WX_GL_RGBA, WX_GL_DOUBLEBUFFER,
      WX_GL_SAMPLE_BUFFERS, 1, // Enable MSAA
      WX_GL_SAMPLES, 0,        // MSAA samples, this will be iterated upon
      WX_GL_DEPTH_SIZE, 24,    // 24-bit depth buffer
      0 };
#endif // OFI_USE_WXWIDGETS
   mEnableMSAA = 3;  // MSAA sample flag is index 3 of attribute list
   mMSAASamples = 5; // MSAA samples is index 5 of attribute list
   
   if(mAvailableMSAASamples.count(-1) == 1) // Initialize
   {
      mAvailableMSAASamples.clear();
      std::vector<int> testMSAASamples = {2, 4, 8, 16}; // Common MSAA samples
      
      // Check available MSAA samples
      for(Integer i = 0; i < testMSAASamples.size(); ++i)
      {
         int currMSAASamples = testMSAASamples[i];
         mGLArgs[mMSAASamples] = currMSAASamples;
#ifdef OFI_USE_WXWIDGETS
#if defined(__WXMSW__) && !wxCHECK_VERSION(3,1,0)
         // IsDisplaySupported() for wxWidgets 3.0.x returns false for all
         // WX_GL_SAMPLES!=0. Therefore, windows specific code was copied from
         // wxWidgets 3.1.0.
         if (FindMatchingPixelFormat(mGLArgs.data(), nullptr) > 0)
            mAvailableMSAASamples.insert(currMSAASamples);
#else
         if(wxGLCanvas::IsDisplaySupported(mGLArgs.data()))
            mAvailableMSAASamples.insert(currMSAASamples);
#endif
#endif // OFI_USE_WXWIDGETS

      }
   }

   // Disable MSAA if not available
   if(mAvailableMSAASamples.empty())
   {
      mGLArgs[mEnableMSAA] = false;
      mGLArgs[mMSAASamples] = 0;
   }
   else // Select first available MSAA sample
   {
      mGLArgs[mEnableMSAA] = true;
      mGLArgs[mMSAASamples] = *mAvailableMSAASamples.begin();
   }
}


/**
 * Destructor
 */
OpenGLOptions::~OpenGLOptions()
{
   // Nothing to do here
}


/**
 * Copy Constructor
 *
 * @param source The object to copy
 */
OpenGLOptions::OpenGLOptions(const OpenGLOptions &source) :
   mEnableVR(source.mEnableVR),
   mGLArgs(source.mGLArgs),
   mEnableMSAA(source.mEnableMSAA),
   mMSAASamples(source.mMSAASamples)
{
   // Nothing else to do here
}


/**
 * Assignment operator
 *
 * @param rhs The object to copy
 */
OpenGLOptions &OpenGLOptions::operator=(const OpenGLOptions &rhs)
{
   mEnableVR = rhs.mEnableVR;
   mGLArgs = rhs.mGLArgs;
   mEnableMSAA = rhs.mEnableMSAA;
   mMSAASamples = rhs.mMSAASamples;
   return *this;
}


/**
 * Enable/Disable MSAA
 *
 * @param value The desired MSAA state
 * @return Whether MSAA is enabled or disabled
 */
bool OpenGLOptions::SetEnableMSAA(bool value)
{
   // Make sure MSAA is available before enabling
   if(mAvailableMSAASamples.empty()) return (mGLArgs[mEnableMSAA] = false);
   else return (mGLArgs[mEnableMSAA] = value);
}


/**
 * Set desired number of MSAA samples
 *
 * @param value The desired MSAA samples
 * @return The set MSAA samples (regardless of whether MSAA is enabled)
 */
Integer OpenGLOptions::SetMSAASamples(Integer value)
{
   // Make sure requested samples are available
   if(mAvailableMSAASamples.find(value) == mAvailableMSAASamples.end()) return mGLArgs[mMSAASamples];
   else return (mGLArgs[mMSAASamples] = value);
}

/**
 * Equality check
 *
 * @param options The options to check
 */
bool OpenGLOptions::EqualTo(const OpenGLOptions &options) const
{
   return ( GetEnableMSAA() == options.GetEnableMSAA()
      && GetMSAASamples() == options.GetMSAASamples()
      && mEnableVR == options.GetEnableVR() );
}
