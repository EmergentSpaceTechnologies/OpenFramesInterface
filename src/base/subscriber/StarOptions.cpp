//$Id$
//------------------------------------------------------------------------------
//                                  StarOptions
//------------------------------------------------------------------------------
// OpenFramesInterface Plugin for GMAT (General Mission Analysis Tool)
//
// Copyright (c) 2022 Emergent Space Technologies, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Developed by Emergent Space Technologies, Inc. under contract number
// NNX16CG16C
//
// Author: Matthew Ruschmann, Emergent Space Technologies, Inc.
// Created: August 24, 2017
/**
 *  OFSpacecraft class
 *  Conveniently wraps a Spacepoint with associated options.
 */
//------------------------------------------------------------------------------

#include "StarOptions.hpp"

/// Default star catalog name
const std::string StarOptions::DefaultStarCatalog = "inp_StarsHYGv3.txt";

/// Default star options when disabled
const StarOptions StarOptions::DISABLED(StarOptions::DefaultStarCatalog,
                                        false, false, -2.0f, 6.0f, 40000U, 1.0f,
                                        10.0f, 0.5f);

/// Star options for printing
const StarOptions StarOptions::PRINTER(StarOptions::DefaultStarCatalog, 
                                       false, true, -2.0f, 6.0f, 40000U, 1.0f,
                                       10.0f, 0.5f);

/// Default star options for monitors
const StarOptions StarOptions::MONITOR(StarOptions::DefaultStarCatalog, 
                                       true, false, -2.0f, 6.0f, 40000U, 1.0f,
                                       10.0f, 0.5f);

/// Default star options for virtual reality
const StarOptions StarOptions::VIRTUAL_REALITY(StarOptions::DefaultStarCatalog, 
                                               true, false, -2.0f, 6.0f, 40000U,
                                               1.0f, 5.0f, 0.1f);


/**
 * Constructor
 *
 * @param name A name for the instance
 */
StarOptions::StarOptions() 
   : mStarCatalog(StarOptions::DefaultStarCatalog),
   mEnableStars(false),
   mPrintOverride(false),
   mMinStarMag(-2.0f),
   mMaxStarMag(6.0f),
   mStarCount(100000U),
   mMinStarPixels(1.0f),
   mMaxStarPixels(10.0f),
   mMinStarDimRatio(0.5f)
{
   // Nothing else to do here
}


StarOptions::StarOptions(const std::string& starCatalog,
   bool enableStars, bool printOverride, float minStarMag,
   float maxStarMag, UnsignedInt starCount, float minStarPixels,
   float maxStarPixels, float minStarDimRatio) :
   mStarCatalog(starCatalog),
   mEnableStars(enableStars),
   mPrintOverride(printOverride),
   mMinStarMag(minStarMag),
   mMaxStarMag(maxStarMag),
   mStarCount(starCount),
   mMinStarPixels(minStarPixels),
   mMaxStarPixels(maxStarPixels),
   mMinStarDimRatio(minStarDimRatio)
{
   // Nothing else to do here
}


/**
 * Destructor
 */
StarOptions::~StarOptions()
{
   // Nothing to do here
}


/**
 * Copy Constructor
 *
 * @param source The object to copy
 */
StarOptions::StarOptions(const StarOptions &source) 
   : mStarCatalog(source.GetStarCatalog()),
   mEnableStars(source.GetEnableStars()),
   mPrintOverride(source.GetPrintOverride()),
   mMinStarMag(source.GetMinStarMag()),
   mMaxStarMag(source.GetMaxStarMag()),
   mStarCount(source.GetStarCount()),
   mMinStarPixels(source.GetMinStarPixels()),
   mMaxStarPixels(source.GetMaxStarPixels()),
   mMinStarDimRatio(source.GetMinStarDimRatio())
{
   // Nothing else to do here
}


/**
 * Assignment operator
 *
 * @param rhs The object to copy
 */
StarOptions &StarOptions::operator=(const StarOptions &rhs)
{
   mStarCatalog = rhs.GetStarCatalog();
   mEnableStars = rhs.GetEnableStars();
   mPrintOverride = rhs.GetPrintOverride();
   mMinStarMag = rhs.GetMinStarMag();
   mMaxStarMag = rhs.GetMaxStarMag();
   mStarCount = rhs.GetStarCount();
   mMinStarPixels = rhs.GetMinStarPixels();
   mMaxStarPixels = rhs.GetMaxStarPixels();
   mMinStarDimRatio = rhs.GetMinStarDimRatio();
   return *this;
}


/**
 * Equality check
 *
 * @param options The options to check
 */
bool StarOptions::EqualTo(const StarOptions &options) const
{
   return (mStarCatalog == options.GetStarCatalog()
      && mEnableStars == options.GetEnableStars()
      && mPrintOverride == options.GetPrintOverride()
      && mMinStarMag == options.GetMinStarMag()
      && mMaxStarMag == options.GetMaxStarMag()
      && mStarCount == options.GetStarCount()
      && mMinStarPixels == options.GetMinStarPixels()
      && mMaxStarPixels == options.GetMaxStarPixels()
      && mMinStarDimRatio == options.GetMinStarDimRatio() );
}
