//$Id: TrajectoryDealer.hpp rmathur $
//------------------------------------------------------------------------------
//                                  TrajectoryDealer
//------------------------------------------------------------------------------
// OpenFramesInterface Plugin for GMAT (General Mission Analysis Tool)
//
// Copyright (c) 2022 Emergent Space Technologies, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Developed by Emergent Space Technologies, Inc. under contract number
// NNX16CG16C
//
// Author: Matthew Ruschmann, Emergent Space Technologies, Inc.
// Created: February 6, 2018
/**
 *  @class TrajectoryDealer
 *  Singleton that manages trajectories so that multiple windows can use the
 *  same trajectory object, which reduces memory usage for large trajectories.
 */
//------------------------------------------------------------------------------

#ifndef TRAJECTORYDEALER_H
#define TRAJECTORYDEALER_H

#include "OpenFramesInterface_defs.hpp"

#include <OpenFrames/Trajectory.hpp>

#include <osg/ref_ptr>

#include <string>
#include <map>
#include <utility>


class TrajectoryListener;
class GmatBase;


class OpenFramesInterface_API TrajectoryDealer
{
public:
   static TrajectoryDealer &Instance();

   TrajectoryDealer();
   virtual ~TrajectoryDealer();

   void AddState(const std::string &bodyName, const std::string &frameName, const std::string &propName, GmatBase *propID,
                 Real t, const Real* posvel, const Real* att, bool isSolving, bool mergeAllSegments);
   void FinalizeOrbitTrajectory(const std::string &bodyName, const std::string &frameName);
   void FinalizeSolverTrajectory(const std::string &bodyName, const std::string &frameName);
   void RemoveLastSolverTrajectory(const std::string &bodyName, const std::string &frameName, Integer numTrajToRemove);

   /// Clears all trajectories
   void ClearAllTrajectories();

   /// Register a listener for new trajectories
   void RegisterListener(TrajectoryListener &listener);
   /// Register a listener for new trajectories
   void UnregisterListener(TrajectoryListener &listener);

private:
   /// A tuple of name and frame to identify segments
   typedef std::pair<std::string, std::string> SegmentIdentifier;
   /// Information about a segment and its associated data
   class SegmentData
   {
   public:
      /// Names of propagators associated with this segment (Could be multiple in a loop)
      std::string propName;
      /// The trajectory containing data from this segment
      osg::ref_ptr<OpenFrames::Trajectory> traj;
      /// A unique ID for the propagate command associated with this segment
      std::vector<GmatBase *> ids;
      /// True if this isDataEnd has been seen for this segment (May be opened back up in a loop)
      bool finalized;
      /// True if segments should all be merged
      bool merge;
   };

   void CombineAllSegments(const std::string &bodyName, const std::string &frameName,
                            std::vector<TrajectoryDealer::SegmentData> &segments);
   void CopyPointToEndOfTrajectory(OpenFrames::Trajectory *source, int index, OpenFrames::Trajectory *destination);
   bool IsSegmentShownInAllListeners(const std::string &bodyName, const std::string &frameName,
                                     const SegmentData &segment);
   bool IsTrajectoryEndpointsConnected(const SegmentData &first, const SegmentData &second) const;
   bool IsTrajectoryEndpointAt(const SegmentData &segment, double time, const double *pos) const;
   bool SameDirectionAs(int &previousFlag, const SegmentData &segment);
   void ConcatenateSegments(SegmentData &thief, SegmentData &prey);
   std::vector<SegmentData> &FindSegmentsFor(const std::string &bodyName, const std::string &frameName);
   std::vector<SegmentData> &FindSolutionsFor(const std::string &bodyName, const std::string &frameName);

   /// A vector of all listeners
   std::vector<TrajectoryListener *> mListeners;
   /// A map for tracking all trajectories that are available
   std::map<SegmentIdentifier, std::vector<SegmentData>> mSegments;
   /// A map for tracking all trajectories that are available
   std::map<SegmentIdentifier, std::vector<SegmentData>> mSolutions;
   /// A count of the number of times RemoveLastSolverTrajectory is called per frame,
   /// it should only be called twice per solver
   std::map<std::string, Integer> mRemovedCount;
};

#endif
