//$Id$
//------------------------------------------------------------------------------
//                                  OFGLOptionsDialog
//------------------------------------------------------------------------------
// OpenFramesInterface Plugin for GMAT (General Mission Analysis Tool)
//
// Copyright (c) 2022 Emergent Space Technologies, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Developed by Emergent Space Technologies, Inc. under contract number
// NNX16CG16C
//
// Author: Matthew Ruschmann, Emergent Space Technologies, Inc.
// Created: August 21, 2017
/**
 *  @class OFGLOptionsDialog
 *  Panel for configuring OpenGL options for the OpenFrames plugin
 */
//------------------------------------------------------------------------------

#ifndef OFGLOPTIONSDIALOG_hpp
#define OFGLOPTIONSDIALOG_hpp

#include "wx/wx.h"
#include <wx/spinctrl.h>

#include "OpenFramesInterface_defs.hpp"
#include "OpenGLOptions.hpp"
#include "gmatwxdefs.hpp"

// Forward declaration of referenced types
class OpenFramesInterface;


class OFGLOptionsDialog: public wxDialog
{
public:
   /// Title for the dialog titlebar
   static const wxString DIALOG_TITLE;

   OFGLOptionsDialog(OpenFramesInterface *forObject, wxWindow *parent);
   virtual ~OFGLOptionsDialog();

protected:
   /// The object for saving star settings to
   OpenFramesInterface *mOFInterface;

   /// True if there are mCustom data changes to be applied
   bool mHasCustomDataChanged;

   /// True when the dialog is in apply mode because there are data changes to
   /// be applied
   bool mDataChanged;
   /// The when the dialog is able to close (currently always true)
   bool mCanClose;

   /// To enable virtual reality display
   wxCheckBox *mShowVRCheckBox;
   /// Sets the MSAA samples
   wxComboBox *mMSAASamplesComboBox;

   /// A sizer to separate dialog buttons from the content page
   wxBoxSizer *mPanelSizer;
   /// A sizer for the content page
   wxSizer *mMiddleSizer;
   /// The top-level sizer for the dialog
   wxStaticBoxSizer *mBottomSizer;

   /// OK button, applies and closes
   wxButton *mOkButton;
   /// Cancel button, prompts to apply then closes
   wxButton *mCancelButton;
   /// Help button displays help
   wxButton *mHelpButton;

   /// Custom star options
   OpenGLOptions mCustom;

   // for initializing data
   void InitializeData();

   // methods based on GmatPanel
   virtual void Create();
   virtual void Show();
   void LoadData();
   void SaveData();

   // event handler
   void OnComboBoxChange(wxCommandEvent& event);
   void OnCheckBoxChange(wxCommandEvent& event);

   void OnOK(wxCommandEvent &event);
   void OnCancel(wxCommandEvent &event);
   void OnHelp(wxCommandEvent &event);
   void OnSystemClose(wxCloseEvent& event);

   void EnableUpdate(bool enable);

   DECLARE_EVENT_TABLE();

   // IDs for the controls and the menu commands
   enum
   {
      ID_BUTTON_OK = 8000,
      ID_BUTTON_CANCEL,
      ID_BUTTON_HELP,
   };
   enum
   {
      ID_CHECKBOX = 15600,
      ID_COMBOBOX,
      ID_TEXT,
   };
};

#endif
