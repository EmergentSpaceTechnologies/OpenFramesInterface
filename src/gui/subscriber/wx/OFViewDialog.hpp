 //$Id$
//------------------------------------------------------------------------------
//                                  OFViewDialog
//------------------------------------------------------------------------------
// OpenFramesInterface Plugin for GMAT (General Mission Analysis Tool)
//
// Copyright (c) 2022 Emergent Space Technologies, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Developed by Emergent Space Technologies, Inc. under contract number
// NNX16CG16C
//
// Author: Matthew Ruschmann, Emergent Space Technologies, Inc.
// Created: August 21, 2017
/**
 *  @class OFViewDialog
 *  Dialog to configure OpenFramesView objects
 */
//------------------------------------------------------------------------------

#ifndef OFVIEWDIALOG_hpp
#define OFVIEWDIALOG_hpp

#include "wx/wx.h"
#include "wx/spinctrl.h"

// Forward declaration of referenced types
class OpenFramesView;


class OFViewDialog: public wxDialog
{
public:
   /// Title for the dialog titlebar
   static const wxString DIALOG_TITLE;

   OFViewDialog(OpenFramesView *forObject, const wxArrayString &objects, wxWindow *parent, bool enableUpdate);
   virtual ~OFViewDialog();

protected:
   /// The object for view settings to
   OpenFramesView *mView;
   /// Possible objects that the view could focus on
   wxArrayString mObjects;

   /// True when the camera settings have changed
   bool mRealDataChanged;
   /// True when the dialog is in apply mode because there are data changes to
   /// be applied
   bool mDataChanged;
   /// The when the dialog is able to close (currently always true)
   bool mCanClose;

   /// Text box for renaming the view
   wxTextCtrl *mViewName;
   /// Checkbox to have the view always looking at another object
   wxCheckBox *mViewLookAtObjCheckBox;
   /// Radio button for relative instead of absolute view
   wxRadioButton *mViewRelativeRadio;
   /// Radio button for absolute instead of relative view
   wxRadioButton *mViewAbsoluteRadio;
   /// Radio button for fitting view to object
   wxRadioButton *mViewFitObjectRadio;
   /// Radio button for fitting view to trajectory
   wxRadioButton *mViewFitTrajRadio;
   /// Radio button for ZUp angle instead of shortest angle
   wxRadioButton *mViewZUpRadio;
   /// Radio button for shortest angle instead of ZUp angle
   wxRadioButton *mViewDirectRadio;
   /// Combo box to select the center of the view
   wxComboBox *mViewObjComboBox;
   /// Combo box to select the look at target
   wxComboBox *mViewTgtComboBox;

   /// Check box to enable setting of the camera location
   wxCheckBox *mCameraOnCheckBox;
   /// Static text to label the eye vector widgets
   wxStaticText *mEyeStaticText;
   /// First entry for the eye vector
   wxSpinCtrlDouble *mEye1SpinCtrl;
   /// Second entry for the eye vector
   wxSpinCtrlDouble *mEye2SpinCtrl;
   /// Third entry for the eye vector
   wxSpinCtrlDouble *mEye3SpinCtrl;
   /// Static text to label the center vector widgets
   wxStaticText *mCenterStaticText;
   /// First entry for the center vector
   wxSpinCtrlDouble *mCenter1SpinCtrl;
   /// Second entry for the center vector
   wxSpinCtrlDouble *mCenter2SpinCtrl;
   /// Third entry for the center vector
   wxSpinCtrlDouble *mCenter3SpinCtrl;
   /// Static text to label the up vector widgets
   wxStaticText *mUpStaticText;
   /// First entry for the up vector
   wxSpinCtrlDouble *mUp1SpinCtrl;
   /// Second entry for the up vector
   wxSpinCtrlDouble *mUp2SpinCtrl;
   /// Third entry for the up vector
   wxSpinCtrlDouble *mUp3SpinCtrl;

   /// Static text to label the eye vector widgets
   wxStaticText *mCurrentEyeStaticText;
   /// First entry for the eye vector
   wxSpinCtrlDouble *mCurrentEye1SpinCtrl;
   /// Second entry for the eye vector
   wxSpinCtrlDouble *mCurrentEye2SpinCtrl;
   /// Third entry for the eye vector
   wxSpinCtrlDouble *mCurrentEye3SpinCtrl;
   /// Static text to label the center vector widgets
   wxStaticText *mCurrentCenterStaticText;
   /// First entry for the center vector
   wxSpinCtrlDouble *mCurrentCenter1SpinCtrl;
   /// Second entry for the center vector
   wxSpinCtrlDouble *mCurrentCenter2SpinCtrl;
   /// Third entry for the center vector
   wxSpinCtrlDouble *mCurrentCenter3SpinCtrl;
   /// Static text to label the up vector widgets
   wxStaticText *mCurrentUpStaticText;
   /// First entry for the up vector
   wxSpinCtrlDouble *mCurrentUp1SpinCtrl;
   /// Second entry for the up vector
   wxSpinCtrlDouble *mCurrentUp2SpinCtrl;
   /// Third entry for the up vector
   wxSpinCtrlDouble *mCurrentUp3SpinCtrl;
   /// Button to copy current settings to default
   wxButton *mCopyUpButton;

   /// A sizer to separate dialog buttons from the content page
   wxBoxSizer *mPanelSizer;
   /// A sizer for the content page
   wxSizer *mMiddleSizer;
   /// The top-level sizer for the dialog
   wxStaticBoxSizer *mBottomSizer;

   /// OK button, applies and closes
   wxButton *mOkButton;
   /// Cancel button, prompts to apply then closes
   wxButton *mCancelButton;
   /// Help button displays help
   wxButton *mHelpButton;

   // for initializing data
   void InitializeData();

   // methods based on GmatPanel
   virtual void Create();
   void Refresh();
   virtual void Show();
   void LoadData();
   bool SaveData();

   void EnableCameraSettings(bool enable);

   // event handler
   void OnCheckBoxChange(wxCommandEvent& event);
   void OnRadioButtonChange(wxCommandEvent& event);
   void OnComboBoxChange(wxCommandEvent& event);
   void OnTextChange(wxCommandEvent& event);
   void OnSpinChange(wxCommandEvent& event);
   void OnCopyUp(wxCommandEvent& event);

   void OnOK(wxCommandEvent &event);
   void OnCancel(wxCommandEvent &event);
   void OnHelp(wxCommandEvent &event);
   void OnSystemClose(wxCloseEvent& event);

   void EnableUpdate(bool enable);

   DECLARE_EVENT_TABLE();

   // IDs for the controls and the menu commands
   enum
   {
      ID_BUTTON_OK = 8000,
      ID_BUTTON_CANCEL,
      ID_BUTTON_HELP,
   };
   enum
   {
      ID_COMBOBOX = 15600,
      CHECKBOX,
      RADIOBUTTON,
      STATIC_TEXT,
      ID_TEXTCTRL,
      ID_SPINCTRL,
      COPY_UP_BUTTON,
   };

private:
   /// Up arrow image
   static const wxBitmap BITMAP_UP_ARROW;
};

#endif
