//$Id$
//------------------------------------------------------------------------------
//                                  OFDateTimeDialog
//------------------------------------------------------------------------------
// OpenFramesInterface Plugin for GMAT (General Mission Analysis Tool)
//
// Copyright (c) 2022 Emergent Space Technologies, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Developed by Emergent Space Technologies, Inc. under contract number
// NNX16CG16C
//
// Author: Matthew Ruschmann, Emergent Space Technologies, Inc.
// Created: August 21, 2017
//------------------------------------------------------------------------------
/**
 * Dialog to configure a time epoch
 */
//------------------------------------------------------------------------------

#include "gmatwxdefs.hpp"
#include "OFDateTimeDialog.hpp"
#include "OpenFramesInterface.hpp"

#include "TimeSystemConverter.hpp"
#include "MessageInterface.hpp"


//------------------------------
// event tables for wxWindows
//------------------------------
BEGIN_EVENT_TABLE(OFDateTimeDialog, wxDialog)
   EVT_BUTTON(ID_BUTTON_OK, OFDateTimeDialog::OnOK)
   EVT_BUTTON(ID_BUTTON_CANCEL, OFDateTimeDialog::OnCancel)
   EVT_BUTTON(ID_BUTTON_DEFAULT, OFDateTimeDialog::OnDefault)
   EVT_BUTTON(ID_BUTTON_CURRENT, OFDateTimeDialog::OnCurrent)
   EVT_CLOSE(OFDateTimeDialog::OnSystemClose)

   EVT_TEXT(ID_TEXTCTRL, OFDateTimeDialog::OnTextChange)
   EVT_COMBOBOX(ID_COMBOBOX, OFDateTimeDialog::OnComboboxChange)
END_EVENT_TABLE()


const wxString OFDateTimeDialog::DIALOG_TITLE = "Time Option";


//------------------------------------------------------------------------------
// OFDateTimeDialog(wxWindow *parent, const wxString &subscriberName)
//------------------------------------------------------------------------------
/**
 * Constructs OFDateTimeDialog object.
 *
 * @param <epoch> The time that is being configured in A1ModJulian.
 *                Any changes are applied to this time.
 * @param <defaultValue> The default value that epoch can optionally be reset to
 *                       in A1ModJulian (days)
 * @param <currentValue> The current value that epoch can optionally be set to
 *                       in A1ModJulian (days)
 * @param <timeFormat> The GMAT time format in which to initially display the time
 *                     e.g. "A1ModJulian", "UTCGregorian", etc.
 * @param <parent> input parent.
 * @param <title> The desired title of the dialog
 *
 * @note Creates the OFDateTimeDialog GUI
 */
//------------------------------------------------------------------------------
OFDateTimeDialog::OFDateTimeDialog(Real &epoch, Real defaultValue, Real currentValue, std::string &timeFormat, wxWindow *parent, const char *title = nullptr) :
    wxDialog(parent, -1, DIALOG_TITLE, wxDefaultPosition, wxDefaultSize, wxDEFAULT_DIALOG_STYLE|wxMAXIMIZE_BOX),
    mDataChanged(false),
    mCanClose(true),
    mEpoch(epoch),
    mDefault(defaultValue),
    mCurrent(currentValue),
    mTimeFormat(timeFormat)
{
   if (title != nullptr)
      SetTitle(title);
   InitializeData();
   Create();
   Show();

   // shortcut keys
   wxAcceleratorEntry entries[1];
   entries[0].Set(wxACCEL_CTRL,  static_cast<int>('W'), ID_BUTTON_CANCEL);
   wxAcceleratorTable accel(1, entries);
   this->SetAcceleratorTable(accel);
}


//------------------------------------------------------------------------------
// ~OFDateTimeDialog()
//------------------------------------------------------------------------------
/**
 * Destructor
 */
OFDateTimeDialog::~OFDateTimeDialog()
{
}


//------------------------------------------------------------------------------
// void InitializeData()
//------------------------------------------------------------------------------
/**
 * Initializes non-pointer data
 */
void OFDateTimeDialog::InitializeData()
{
   mDataChanged = false;
   mCanClose = true;
}


//------------------------------------------------------------------------------
// void Create()
//------------------------------------------------------------------------------
/**
 * Creates and configures the contents of the dialog
 */
void OFDateTimeDialog::Create()
{
   int bsize = 2; // border size

   // create axis array
   wxArrayString emptyList;

   //-----------------------------------------------------------------
   // Main sizers and buttons
   //-----------------------------------------------------------------
   mPanelSizer = new wxBoxSizer(wxVERTICAL);
   mMiddleSizer = (wxSizer*)(new wxStaticBoxSizer(wxVERTICAL, this));
   mBottomSizer = new wxStaticBoxSizer(wxVERTICAL, this );
   wxBoxSizer *buttonSizer = new wxBoxSizer(wxHORIZONTAL);
   wxBoxSizer *presetSizer = new wxBoxSizer(wxHORIZONTAL);

   // create bottom buttons
   mOkButton = new wxButton(this, ID_BUTTON_OK, "OK", wxDefaultPosition, wxDefaultSize, 0);
   mOkButton->SetToolTip("Apply Changes and Close");
   mOkButton->SetDefault();
   mCancelButton = new wxButton(this, ID_BUTTON_CANCEL, "Cancel", wxDefaultPosition, wxDefaultSize, 0);
   mCancelButton->SetToolTip("Close Without Applying Changes");
   mDefaultButton = new wxButton(this, ID_BUTTON_DEFAULT, "Default", wxDefaultPosition, wxDefaultSize, 0);
   mDefaultButton->SetToolTip("Reset to default value");
   mCurrentButton = new wxButton(this, ID_BUTTON_CURRENT, "Current", wxDefaultPosition, wxDefaultSize, 0);
   mCurrentButton->SetToolTip("Use the current time of the plot");

   buttonSizer->Add(mOkButton, 0, wxALIGN_CENTER | wxALL, bsize);
   buttonSizer->Add(mCancelButton, 0, wxALIGN_CENTER | wxALL, bsize);
   presetSizer->Add(mDefaultButton, 0, wxALIGN_RIGHT | wxALL, bsize);
   presetSizer->Add(mCurrentButton, 0, wxALIGN_RIGHT | wxALL, bsize);

   mBottomSizer->Add(buttonSizer, 0, wxGROW | wxALL, bsize);

   //-----------------------------------------------------------------
   // Data collect and update frequency
   //-----------------------------------------------------------------
   wxStaticText *epochFormatStaticText = new wxStaticText(this, ID_TEXT, wxT("Epoch Format"), wxDefaultPosition,
                                                          wxDefaultSize, 0);
   
   int epochWidth = 170;
   #ifdef __APPLE__
      epochWidth = 178;
   #endif
   // combo box for the epoch format
   mEpochFormatComboBox = new wxComboBox(this, ID_COMBOBOX, wxT(""), wxDefaultPosition, wxSize(epochWidth,-1),
                                         emptyList, wxCB_DROPDOWN | wxCB_READONLY);
   
   // label for epoch
   wxStaticText *epochStaticText = new wxStaticText(this, ID_TEXT, wxT("Epoch"), wxDefaultPosition, wxDefaultSize, 0);
   // textfield for the epoch value
   mEpochValue = new wxTextCtrl( this, ID_TEXTCTRL, wxT(""), wxDefaultPosition, wxSize(epochWidth,-1), 0);

   wxFlexGridSizer *plotOptionSizer = new wxFlexGridSizer(2, 0, 0);
   plotOptionSizer->Add(epochFormatStaticText, 0, wxALIGN_RIGHT|wxALIGN_CENTER_VERTICAL|wxALL, bsize);
   plotOptionSizer->Add(mEpochFormatComboBox, 0, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL|wxALL, bsize);
   plotOptionSizer->Add(epochStaticText, 0, wxALIGN_RIGHT|wxALIGN_CENTER_VERTICAL|wxALL, bsize);
   plotOptionSizer->Add(mEpochValue, 0, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL|wxALL, bsize);

   wxStaticBoxSizer *plotOptionStaticSizer = new wxStaticBoxSizer(wxVERTICAL, this, wxT("Time Option"));
   plotOptionStaticSizer->Add(plotOptionSizer, 0, wxALIGN_CENTER_HORIZONTAL|wxALL, bsize);
   plotOptionStaticSizer->AddSpacer(10);
   plotOptionStaticSizer->Add(presetSizer, 0, wxALIGN_CENTER_HORIZONTAL|wxALL, bsize);

   //-----------------------------------------------------------------
   // Add to page sizer
   //-----------------------------------------------------------------

   wxFlexGridSizer *pageSizer1 = new wxFlexGridSizer(3, 2, 0, 0);
   pageSizer1->Add(plotOptionStaticSizer, 0, wxALIGN_CENTRE|wxGROW|wxALL, bsize);

   //-----------------------------------------------------------------
   // Add to middle sizer
   //-----------------------------------------------------------------
   wxBoxSizer *pageSizer = new wxBoxSizer(wxVERTICAL);
   pageSizer->Add(pageSizer1, 0, wxALIGN_CENTRE|wxALL, bsize);

   mMiddleSizer->Add(pageSizer, 0, wxALIGN_CENTRE|wxALL, bsize);
}


//------------------------------------------------------------------------------
// void Show()
//------------------------------------------------------------------------------
/**
 * Shows the dialog
 */
//------------------------------------------------------------------------------
void OFDateTimeDialog::Show()
{
   mPanelSizer->Add(mMiddleSizer, 1, wxGROW | wxALL, 1);
   mPanelSizer->Add(mBottomSizer, 0, wxGROW | wxALL, 1);

   SetAutoLayout(true); // tell the enclosing window to adjust to the size of the sizer
   SetSizer(mPanelSizer); //use the sizer for layout
   mPanelSizer->SetSizeHints(this); //set size hints to honour minimum size

   LoadData();
   EnableUpdate(false);

   // call Layout() to force layout of the children anew
   mPanelSizer->Layout();
   Centre();
}


//------------------------------------------------------------------------------
// void LoadData()
//------------------------------------------------------------------------------
/**
 * Loads the current time value from mEpoch
 */
void OFDateTimeDialog::LoadData()
{
   try
   {
      // Load all possible time formats
      StringArray reps = TimeSystemConverter::Instance()->GetValidTimeRepresentations();
      for (unsigned int i = 0; i < reps.size(); i++)
      {
         mEpochFormatComboBox->Append(reps[i].c_str());
      }
      
      // Convert the epoch from A1ModJulian to the requested time format
      Real epochVal;
      std::string epochStr;
      TimeSystemConverter::Instance()->Convert("A1ModJulian", mEpoch, "", mTimeFormat, epochVal, epochStr);

      mEpochFormatComboBox->SetValue(mTimeFormat);
      mEpochValue->SetValue(epochStr);
   }
   catch (BaseException &e)
   {
      MessageInterface::PopupMessage(Gmat::ERROR_, e.GetFullMessage().c_str());
   }

   EnableUpdate(false);
}


//------------------------------------------------------------------------------
// void SaveData()
//------------------------------------------------------------------------------
/**
 * Applies any changes to mEpoch
 */
void OFDateTimeDialog::SaveData()
{
   //-----------------------------------------------------------------
   // save values to base, base code should do the range checking
   //-----------------------------------------------------------------
   try
   {
      if (mDataChanged)
      {
         // Get the epoch string
         std::string newEpoch = mEpochValue->GetValue().WX_TO_STD_STRING;
         Real a1mj;
         std::string outStr;

         // Convert to A1ModJulian
         TimeSystemConverter::Instance()->Convert(mTimeFormat, -999.999, newEpoch, "A1ModJulian", a1mj, outStr);
         mEpoch = a1mj;
      }

      EnableUpdate(false);
   }
   catch (BaseException &e)
   {
      MessageInterface::PopupMessage(Gmat::ERROR_, e.GetFullMessage().c_str());
   }
}


//------------------------------------------------------------------------------
// void OnTextChange(wxCommandEvent& event)
//------------------------------------------------------------------------------
/**
 * Callback that enables application of a new time epoch
 *
 * @param <event> Event details
 */
void OFDateTimeDialog::OnTextChange(wxCommandEvent& event)
{
   EnableUpdate(true);
}


//------------------------------------------------------------------------------
// void OnComboboxChange(wxCommandEvent& event)
//------------------------------------------------------------------------------
/**
 * Callback that enables application of a new time epoch
 *
 * @param <event> Event details
 */
void OFDateTimeDialog::OnComboboxChange(wxCommandEvent& event)
{
   try
   {
      // Get the current time string and newly-selected time format
      std::string timeStr = mEpochValue->GetValue().WX_TO_STD_STRING;
      std::string newTimeFormat = mEpochFormatComboBox->GetValue().WX_TO_STD_STRING;

      // Convert time string to new time format
      Real newTimeVal;
      std::string newTimeStr;
      TimeSystemConverter::Instance()->Convert(mTimeFormat, -999.999, timeStr, newTimeFormat, newTimeVal, newTimeStr);
      mTimeFormat = newTimeFormat;
      mEpochValue->SetValue(newTimeStr);
   }
   catch (BaseException &e)
   {
      MessageInterface::PopupMessage(Gmat::ERROR_, e.GetFullMessage().c_str());
   }
}


//------------------------------------------------------------------------------
// void OnOk()
//------------------------------------------------------------------------------
/**
 * Callback that saves the data changes and closes the page
 *
 * @param <event> Event details
 */
//------------------------------------------------------------------------------
void OFDateTimeDialog::OnOK(wxCommandEvent &event)
{
   if (mDataChanged)
   {
      SaveData();
      if (mCanClose)
         EndModal(wxID_OK);
   }
   else
   {
      if (mCanClose)
         EndModal(wxID_CANCEL);
   }

}


//------------------------------------------------------------------------------
// void OnCancel()
//------------------------------------------------------------------------------
/**
 * Close page without prompting about changes
 *
 * @param <event> Event details
 */
//------------------------------------------------------------------------------
void OFDateTimeDialog::OnCancel(wxCommandEvent &event)
{
   EndModal(wxID_CANCEL);
}


//------------------------------------------------------------------------------
// void OnSystemClose(wxCloseEvent& even)
//------------------------------------------------------------------------------
/**
 * Close page after prompting for changes
 *
 * @param <event> Details of the event
 */
//------------------------------------------------------------------------------
void OFDateTimeDialog::OnSystemClose(wxCloseEvent& event)
{
   // Prompt user if changes were made
   if (mDataChanged)
   {
      if (wxMessageBox(_T("There were changes made to \"" + GetTitle() + "\" panel"
         " which will be lost on Close. \nDo you want to close anyway?"),
         _T("Please Confirm Close"),
         wxICON_QUESTION | wxYES_NO) != wxYES)
      {
         event.Veto();
         return;
      }
   }

   EndModal(wxID_CANCEL);
}


//------------------------------------------------------------------------------
// void OnDefault()
//------------------------------------------------------------------------------
/**
 * Callback that resets the time value to the default
 *
 * @param <event> Event details
 */
//------------------------------------------------------------------------------
void OFDateTimeDialog::OnDefault(wxCommandEvent &event)
{
   try
   {
      Real defaultTimeVal;
      std::string defaultTimeStr;
      TimeSystemConverter::Instance()->Convert("A1ModJulian", mDefault, "", mTimeFormat, defaultTimeVal, defaultTimeStr);
      mEpochValue->SetValue(defaultTimeStr);
      EnableUpdate(true);
   }
   catch (BaseException &e)
   {
      MessageInterface::PopupMessage(Gmat::ERROR_, e.GetFullMessage().c_str());
   }
}


//------------------------------------------------------------------------------
// void OnCurrent()
//------------------------------------------------------------------------------
/**
 * Callback that resets the time value to the current time
 *
 * @param <event> Event details
 */
//------------------------------------------------------------------------------
void OFDateTimeDialog::OnCurrent(wxCommandEvent &event)
{
   try
   {
      Real currentTimeVal;
      std::string currentTimeStr;
      TimeSystemConverter::Instance()->Convert("A1ModJulian", mCurrent, "", mTimeFormat, currentTimeVal, currentTimeStr);
      mEpochValue->SetValue(currentTimeStr);
      EnableUpdate(true);
   }
   catch (BaseException &e)
   {
      MessageInterface::PopupMessage(Gmat::ERROR_, e.GetFullMessage().c_str());
   }
}


//------------------------------------------------------------------------------
// virtual void EnableUpdate(bool enable = true)
//------------------------------------------------------------------------------
/**
 * Changes the mode of the dialog based on whether there are data changes to
 * apply
 *
 * @param <enable> If true, then application of data changes is enabled
 */
void OFDateTimeDialog::EnableUpdate(bool enable)
{
   mDataChanged = enable;
   if (mOkButton != nullptr)
      mOkButton->Enable(enable);
}
