//$Id$
//------------------------------------------------------------------------------
//                                  TimeDilator
//------------------------------------------------------------------------------
// OpenFramesInterface Plugin for GMAT (General Mission Analysis Tool)
//
// Copyright (c) 2022 Emergent Space Technologies, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Developed by Emergent Space Technologies, Inc. under contract number
// NNX16CG16C
//
// Author: Ravi Mathur, Emergent Space Technologies, Inc.
// Created: December 1, 2015
/**
*  TimeDilator class
 *  A widget for constraining limits of the time slider.
*/
//------------------------------------------------------------------------------

#include "TimeDilator.hpp"
#include "OFDateTimeDialog.hpp"

#include "MessageInterface.hpp"

#include <wx/settings.h>
#include <wx/tooltip.h>
#include <cmath>
#include <sstream>
#include <iomanip>


wxBEGIN_EVENT_TABLE(TimeDilator, wxControl)
EVT_PAINT(TimeDilator::OnPaint)
EVT_SIZE(TimeDilator::OnSize)
EVT_MOTION(TimeDilator::OnMouseMove)
EVT_LEFT_DOWN(TimeDilator::OnMouseLeftDown)
EVT_LEFT_UP(TimeDilator::OnMouseLeftUp)
EVT_RIGHT_UP(TimeDilator::OnMouseRightUp)
EVT_MOUSEWHEEL(TimeDilator::OnMouseWheel)
EVT_MENU_RANGE(ID_ZOOM_IN, ID_END_TIME, TimeDilator::OnMenuSelection)
wxEND_EVENT_TABLE()

wxIMPLEMENT_DYNAMIC_CLASS(TimeDilator, wxControl);

wxDEFINE_EVENT(DIALATE_CURRENT_TIME, wxCommandEvent);


const wxString TimeDilator::TOOLTIP_TIME_SLIDER("Translate simulation time within the window");
const wxString TimeDilator::TOOLTIP_DILATOR_MOVE_TIME("Translate simulation time to here");
const wxString TimeDilator::TOOLTIP_DILATOR_MOVE_WINDOW("Move the time window");
const wxString TimeDilator::TOOLTIP_DILATOR_MOVE_TIME_WINDOW("Translate simulation time and move the time window");
const wxString TimeDilator::TOOLTIP_DILATOR_SIZE_WINDOW("Resize the time window");


/**
 * Constructor
 */
TimeDilator::TimeDilator() :
   mBuffer(nullptr)
{
   Init();
}


/**
 * Constructor
 */
TimeDilator::TimeDilator(wxWindow *parent, wxWindowID winid, const wxPoint& pos, const wxSize& size, long style,
                           const wxValidator& val, const wxString& name) :
   mBuffer(nullptr)
{
   Init();
   Create(parent, winid, pos, size, style, val, name);
}


/**
 * Destructor
 */
TimeDilator::~TimeDilator()
{
   if (mBuffer != nullptr)
   {
      delete mBuffer;
      mBuffer = nullptr;
   }
}


/**
 * Create the control
 */
bool TimeDilator::Create(wxWindow *parent, wxWindowID winid, const wxPoint& pos, const wxSize& size, long style,
                          const wxValidator& val, const wxString& name)
{
   bool status = wxControl::Create(parent, winid, pos, size, style, val, name);
   SetToolTip(new wxToolTip(TOOLTIP_TIME_SLIDER));
   mBuffer = new wxBitmap(GetClientSize());
   if (parent != nullptr)
      SetBackgroundColour(parent->GetBackgroundColour());
   return status;
}


/**
 * Init the widget's internals
 */
void TimeDilator::Init()
{
   mMouseState = IDLE;
   mStartLimit = 1.0;
   mEndLimit = 2.0;
   mStartTime = 1.0;
   mEndTime = 2.0;
   mCurrentTime = 1.5;
   mTimeFormat = "UTCGregorian"; // Tells OFDateTimeDialog to show time in UTCGregorian when it first opens up
   mCurrentTimeSet = false;
   SetCursorState(TIME_SLIDER);
}


void TimeDilator::SetLimits(Real start, Real end)
{
   if (mEndLimit != end || mStartLimit != start)
   {
      mStartLimit = start;
      mEndLimit = end;
      mStartTime = start;
      mEndTime = end;
      if (!mCurrentTimeSet)
      {
         mCurrentTime = start;
         mCurrentTimeSet = true;
      }
      else
      {
         if (mCurrentTime < start)
            mCurrentTime = start;
         else if (mCurrentTime > end)
            mCurrentTime = end;
      }
      TriggerRedraw();
   }
}


void TimeDilator::SetDilation(Real start, Real end, bool scaleCurrentTime)
{
   TimeFraction timeFrac;
   if (scaleCurrentTime)
      timeFrac = GetCurrentTimeFraction();
   if (start > mStartLimit)
      mStartTime = start;
   else
      mStartTime = mStartLimit;
   if (end < mEndLimit)
      mEndTime = end;
   else
      mEndTime = mEndLimit;
   if (scaleCurrentTime)
      SetCurrentTimeFraction(timeFrac);
   TriggerRedraw();
}


/**
 * Set the current time in the sliders
 */
void TimeDilator::SetCurrent(Real current)
{
   if (current < mStartLimit)
      current = mStartLimit;
   if (current > mEndLimit)
      current = mEndLimit;
   if (mCurrentTime != current)
   {
      mCurrentTime = current;
      TriggerRedraw();
   }
}


/**
 * We need to calculate and return the best size of the widget...
 */
wxSize TimeDilator::DoGetBestSize() const
{
   return wxSize(160, 24);
}


/**
 * Blit the buffer to a wxDC
 */
void TimeDilator::OnPaint(wxPaintEvent&)
{
   wxMemoryDC tmpDC;
   tmpDC.SelectObject(*mBuffer);
   Draw(tmpDC);
   tmpDC.SelectObject(wxNullBitmap);

   wxPaintDC dc(this);
   wxMemoryDC bitmapDc;
   bitmapDc.SelectObject(*mBuffer);
   wxRegionIterator upd(GetUpdateRegion()); // get the update rect list
   while (upd)
   {
      // Get client area to repaint
      wxRect rect(upd.GetRect());
      dc.Blit(rect.GetPosition(), rect.GetSize(), &bitmapDc, rect.GetPosition());
      upd++;
   }
}


/**
 * Resize the buffer
 */
void TimeDilator::OnSize(wxSizeEvent&)
{
   wxSize size = GetClientSize();
   delete mBuffer;
   mBuffer = new wxBitmap(size);
   TriggerRedraw();
}


/**
 * Redraw the widget
 */
void TimeDilator::TriggerRedraw()
{
   Refresh(false);
}


/**
 * Draw the widget in the buffer
 */
void TimeDilator::Draw(wxDC &dc)
{
   wxSystemSettings settings;
   int w = mBuffer->GetWidth();
   int h = mBuffer->GetHeight();

   dc.SetBackground(GetBackgroundColour());
   dc.Clear();

   // Draw the bottom
   int s = 2 * h / 3 - 1;
   dc.SetPen(wxPen(settings.GetColour(wxSYS_COLOUR_BTNSHADOW)));
   dc.DrawLine(0, s+1, 0, h-2);
   dc.DrawLine(0, s+1, w-1, s+1);
   dc.SetPen(wxPen(settings.GetColour(wxSYS_COLOUR_BTNHIGHLIGHT)));
   dc.DrawLine(w-1, s+1, w-1, h-1);
   dc.DrawLine(0, h-2, w-1, h-2);
   dc.SetPen(wxPen(settings.GetColour(wxSYS_COLOUR_BTNFACE)));
   dc.SetBrush(wxBrush(settings.GetColour(wxSYS_COLOUR_BTNFACE)));
   dc.DrawRectangle(1, s+2, w-2, h-s-4);
   wxRect rect = GetRangeInLimitsPixelRect(mBuffer->GetSize());
   dc.SetPen(wxPen(settings.GetColour(wxSYS_COLOUR_HIGHLIGHT)));
   dc.SetBrush(wxBrush(settings.GetColour(wxSYS_COLOUR_HIGHLIGHT)));
   dc.DrawRectangle(rect.GetX(), rect.GetY(), rect.GetWidth(), rect.GetHeight());
   int currentInLimits = GetCurrentInLimitsPixelPos(mBuffer->GetSize());
   if (currentInLimits >= rect.GetX() && currentInLimits <= rect.GetX() + rect.GetWidth())
   {
      dc.SetPen(wxPen(settings.GetColour(wxSYS_COLOUR_HIGHLIGHTTEXT)));
      dc.DrawLine(currentInLimits, s+2, currentInLimits, h-2);
   }
   else
   {
      dc.SetPen(wxPen(settings.GetColour(wxSYS_COLOUR_BTNTEXT)));
      dc.DrawLine(currentInLimits, s+2, currentInLimits, h-2);
   }

   // Draw the top
   int c = (s-2) / 2;
   dc.SetPen(wxPen(settings.GetColour(wxSYS_COLOUR_BTNSHADOW)));
   dc.DrawLine(0, c+2, 0, c-1);
   dc.DrawLine(0, c-1, w-1, c-1);
   dc.SetPen(wxPen(settings.GetColour(wxSYS_COLOUR_BTNHIGHLIGHT)));
   dc.DrawLine(w-1, c+2, w-1, c-2);
   dc.DrawLine(0, c+2, w-1, c+2);
   dc.SetPen(wxPen(settings.GetColour(wxSYS_COLOUR_BTNFACE)));
   dc.SetBrush(wxBrush(settings.GetColour(wxSYS_COLOUR_BTNFACE)));
   dc.DrawRectangle(1, c, w-2, 2);
   int currentInRange = GetCurrentInRangePixelPos(mBuffer->GetSize());
   if (currentInRange > -1)
   {
      // Draw the handle
      int m = (s-2 - c);
      dc.SetPen(wxPen(settings.GetColour(wxSYS_COLOUR_GRAYTEXT)));
      dc.SetBrush(wxBrush(settings.GetColour(wxSYS_COLOUR_GRAYTEXT)));
      dc.DrawRectangle(1, c, currentInRange-1, 2);
      dc.SetPen(wxPen(settings.GetColour(wxSYS_COLOUR_BTNHIGHLIGHT)));
      dc.DrawLine(currentInRange-3, c+m, currentInRange-3, c-m+1);
      dc.DrawLine(currentInRange-3, c-m+1, currentInRange+3, c-m+1);
      dc.SetPen(wxPen(settings.GetColour(wxSYS_COLOUR_BTNSHADOW)));
      dc.DrawLine(currentInRange+3, c+m, currentInRange+3, c-m);
      dc.DrawLine(currentInRange-3, c+m, currentInRange+3, c+m);
      dc.SetPen(wxPen(settings.GetColour(wxSYS_COLOUR_BTNFACE)));
      dc.SetBrush(wxBrush(settings.GetColour(wxSYS_COLOUR_BTNFACE)));
      dc.DrawRectangle(currentInRange-2, c-m+2, 5, 2*m-2);
   }
   else if (currentInRange == -2)
   {
      // Handle is off to the far right
      dc.SetPen(wxPen(settings.GetColour(wxSYS_COLOUR_GRAYTEXT)));
      dc.SetBrush(wxBrush(settings.GetColour(wxSYS_COLOUR_GRAYTEXT)));
      dc.DrawRectangle(1, c, w-2, 2);
   }
   // else Handle off to the far left
}


/**
 * Get the rectangle representing time dilation in pixels
 */
wxRect TimeDilator::GetRangeInLimitsPixelRect(const wxSize &clientSize)
{
   int t = 2 * clientSize.GetHeight() / 3 - 1;
   Real range = mEndLimit - mStartLimit;
   int x = 1 + int(round((clientSize.GetWidth() - 3)*(mStartTime - mStartLimit)/range));
   if (x > clientSize.GetWidth() - 2)
      x = clientSize.GetWidth() - 2;
   int y = t + 2;
   int w = 1 + int(round((clientSize.GetWidth() - 3)*(mEndTime - mStartLimit)/range)) - x + 1;
   if (w < 1)
      w = 1;
   int h = (clientSize.GetHeight() - t) - 4;

   return wxRect(x, y, w, h);
}


/**
 * Get the rectangle representing time dilation in pixels
 */
int TimeDilator::GetCurrentInLimitsPixelPos(const wxSize &clientSize)
{
   Real range = mEndLimit - mStartLimit;
   int x = 1 + int(round((clientSize.GetWidth() - 3)*(mCurrentTime - mStartLimit)/range));
   if (x > clientSize.GetWidth() - 2)
      x = clientSize.GetWidth() - 2;
   else if (x < 1)
      x = 1;

   return x;
}


/**
 * Get the rectangle representing time dilation in pixels
 */
int TimeDilator::GetCurrentInRangePixelPos(const wxSize &clientSize)
{
   Real range = mEndTime - mStartTime;
   int x = 3;
   if (mCurrentTime < mStartTime)
      x = -1;
   else if (mCurrentTime > mEndTime)
      x = -2;
   else
   {
      x = 3 + int(round((clientSize.GetWidth() - 7)*(mCurrentTime - mStartTime)/range));
      if (x > clientSize.GetWidth() - 4)
         x = clientSize.GetWidth() - 4;
      else if (x < 3)
         x = 3;
   }

   return x;
}


/**
 * Mouse movement
 */
void TimeDilator::OnMouseMove(wxMouseEvent &event)
{
   wxSize size = GetClientSize();
   int split = 2 * size.GetHeight() / 3 - 1;
   if ((event.GetY() > split || mMouseState != IDLE) && mMouseState != SETTING_CURRENT)
   {
      // Mouse in the dilator or grabbed by the dilator
      if (mMouseState != IDLE)
      {
         Real range = mEndLimit - mStartLimit;
         Real pos = static_cast<Real>(event.GetX() - 1) / static_cast<Real>(size.GetWidth() - 2);
         Real time = mStartLimit + pos * range;
         if (mMouseState == MOVING_CURRENT)
         {
            if (time < mStartLimit)
               time = mStartLimit;
            else if (time > mEndLimit)
               time = mEndLimit;
            if (mTimeFracAtMouseDown.inRange)
            {
               Real range = mEndTime - mStartTime;
               Real left = mTimeFracAtMouseDown.fraction * range;
               Real right = (1.0 - mTimeFracAtMouseDown.fraction) * range;
               mStartTime = time - left;
               mEndTime = time + right;
               if (mStartTime < mStartLimit)
               {
                  Real range0 = mEndTime - mStartTime;
                  mStartTime = mStartLimit;
                  mEndTime = mStartTime + range0;
                  if (mEndTime > mEndLimit)
                     mEndTime = mEndLimit;
               }
               if (mEndTime > mEndLimit)
               {
                  Real range0 = mEndTime - mStartTime;
                  mEndTime = mEndLimit;
                  mStartTime = mEndTime - range0;
                  if (mStartTime < mStartLimit)
                     mStartTime = mStartLimit;
               }
            }
            TriggerCurrentTimeEvent(time);
         }
         else if (mMouseState == MOVING_START)
         {
            if (time > mEndTime)
               time = mEndTime;
            SetDilation(time, mEndTime, mTimeFracAtMouseDown.inRange);
         }
         else if (mMouseState == MOVING_END)
         {
            if (time < mStartTime)
               time = mStartTime;
            SetDilation(mStartTime, time, mTimeFracAtMouseDown.inRange);
         }
         else if (mMouseState == TRANSLATING)
         {
            if (time < mStartLimit)
               time = mStartLimit;
            else if (time > mEndLimit)
               time = mEndLimit;
            Real range = mEndTime - mStartTime;
            Real delta = mMouseInitial - time;
            if (mStartTimeAtMouseDown - delta < mStartLimit)
               delta = mStartTimeAtMouseDown - mStartLimit;
            else if (mStartTimeAtMouseDown - delta + range > mEndLimit)
               delta = mStartTimeAtMouseDown + range - mEndLimit;
            mStartTime = mStartTimeAtMouseDown - delta;
            mEndTime = mStartTime + range;
            SetCurrentTimeFraction(mTimeFracAtMouseDown);
            TriggerRedraw();
         }
      }
      else
      {
         // Set the mouse cursor
         wxRect time = GetRangeInLimitsPixelRect(size);
         int current = GetCurrentInLimitsPixelPos(size);
         if (abs(event.GetX() - time.GetX()) < 3) // resize start
            SetCursorState(DILATOR_SIZE_WINDOW);
         else if (abs(event.GetX() - (time.GetX() + time.GetWidth())) < 3) // resize end
            SetCursorState(DILATOR_SIZE_WINDOW);
         else if (abs(event.GetX() - current) < 3) // current time
            SetCursorState(DILATOR_MOVE_TIME_WINDOW);
         else if (event.GetX() > time.GetX() && event.GetX() < time.GetX() + time.GetWidth()) // translate
            SetCursorState(DILATOR_MOVE_WINDOW);
         else
            SetCursorState(DILATOR_MOVE_TIME);
      }
   }
   else if (event.GetY() < split - 1 || mMouseState == SETTING_CURRENT)
   {
      // Mouse in the slider or grabbed by the slider
      if (event.LeftIsDown())
      {
         Real range = mEndTime - mStartTime;
         Real pos = static_cast<Real>(event.GetX() - 3) / static_cast<Real>(size.GetWidth() - 6);
         Real time = mStartTime + pos * range;
         if (time < mStartTime)
            TriggerCurrentTimeEvent(mStartTime);
         else if (time > mEndTime)
            TriggerCurrentTimeEvent(mEndTime);
         else
            TriggerCurrentTimeEvent(time);
         TriggerRedraw();
      }
      SetCursorState(TIME_SLIDER);
   }
   else
      SetCursorState(TIME_SLIDER);
}


/**
 * Set the current time and trigger an event
 */
void TimeDilator::SetCursorState(CursorState state)
{
   if (mMouseState == IDLE && mCursorState != state)
   {
      switch (state)
      {
      case TIME_SLIDER:
         SetCursor(wxCURSOR_ARROW);
         SetToolTip(nullptr);
         SetToolTip(new wxToolTip(TOOLTIP_TIME_SLIDER));
         break;
      case DILATOR_MOVE_TIME:
         SetCursor(wxCURSOR_ARROW);
         SetToolTip(nullptr);
         SetToolTip(new wxToolTip(TOOLTIP_DILATOR_MOVE_TIME));
         break;
      case DILATOR_MOVE_WINDOW:
         SetCursor(wxCURSOR_RIGHT_ARROW);
         SetToolTip(nullptr);
         SetToolTip(new wxToolTip(TOOLTIP_DILATOR_MOVE_WINDOW));
         break;
      case DILATOR_MOVE_TIME_WINDOW:
         SetCursor(wxCURSOR_ARROW);
         SetToolTip(nullptr);
         SetToolTip(new wxToolTip(TOOLTIP_DILATOR_MOVE_TIME_WINDOW));
         break;
      case DILATOR_SIZE_WINDOW:
         SetCursor(wxCURSOR_SIZEWE);
         SetToolTip(nullptr);
         SetToolTip(new wxToolTip(TOOLTIP_DILATOR_SIZE_WINDOW));
         break;
      }
      mCursorState = state;
   }
}


/**
 * Mouse down
 */
void TimeDilator::OnMouseLeftDown(wxMouseEvent &event)
{
   wxSize size = GetClientSize();
   int split = 2 * size.GetHeight() / 3 - 1;
   if (event.GetY() > split)
   {
      wxRect time = GetRangeInLimitsPixelRect(size);
      int current = GetCurrentInLimitsPixelPos(size);
      mTimeFracAtMouseDown = GetCurrentTimeFraction();
      if (abs(event.GetX() - time.GetX()) < 3)
         mMouseState = MOVING_START;
      else if (abs(event.GetX() - (time.GetX()+time.GetWidth())) < 3)
         mMouseState = MOVING_END;
      else if (abs(event.GetX() - current) < 3)
         mMouseState = MOVING_CURRENT;
      else if (event.GetX() > time.GetX() && event.GetX() < time.GetX() + time.GetWidth())
      {
         mMouseState = TRANSLATING;
         Real range = mEndLimit - mStartLimit;
         Real pos = static_cast<Real>(event.GetX() - 1) / static_cast<Real>(size.GetWidth() - 2);
         mMouseInitial = mStartLimit + pos * range;
         mStartTimeAtMouseDown = mStartTime;
      }
      else
         mMouseState = MOVING_CURRENT;
   }
   else if (event.GetY() < split - 1)
   {
      mMouseState = SETTING_CURRENT;
   }

   if (mMouseState != IDLE)
   {
      CaptureMouse();
      OnMouseMove(event);
   }
   SetToolTip(nullptr);
}


/**
 * Mouse left up releases the mouse
 */
void TimeDilator::OnMouseLeftUp(wxMouseEvent &event)
{
   if (mMouseState != IDLE)
   {
      mMouseState = IDLE;
      ReleaseMouse();
   }
}


/**
 * Mouse right up shows a popup menu
 */
void TimeDilator::OnMouseRightUp(wxMouseEvent &event)
{
   std::stringstream zoomString;
   bool enableZoomOut = ( 2 * (mEndTime - mStartTime) <= (mEndLimit - mStartLimit) );
   double zoomLevelDouble = (mEndLimit - mStartLimit) / (mEndTime - mStartTime);
   wxMenu menu(0L);

   zoomString << "Current zoom is " << std::setprecision(2) << std::fixed << zoomLevelDouble;
   menu.Append(ID_ZOOM_LEVEL, zoomString.str());
   menu.Enable(ID_ZOOM_LEVEL, false);

   zoomString.str(std::string());
   zoomString.clear();
   zoomString << "Zoom in to " << std::setprecision(2) << std::fixed << 2.0 * zoomLevelDouble;
   menu.Append(ID_ZOOM_IN, zoomString.str());

   zoomString.str(std::string());
   zoomString.clear();
   if (enableZoomOut)
      zoomString << "Zoom out to " << std::setprecision(2) << std::fixed << zoomLevelDouble / 2.0;
   else
      zoomString << "Zoom out to 1.00";

   menu.Append(ID_ZOOM_OUT, zoomString.str());
   menu.Append(ID_ZOOM_NORMAL, "Reset Zoom to 1.00");
   menu.AppendSeparator();
   menu.Append(ID_CURRENT_TIME, "Set Current Time");
   menu.Append(ID_START_TIME, "Set Initial Time Dilation");
   menu.Append(ID_END_TIME, "Set Final Time Dilation");

   SetToolTip(nullptr);
   PopupMenu(&menu);
}


/**
 * Mouse wheel + ctrl zooms time
 */
void TimeDilator::OnMouseWheel(wxMouseEvent &event)
{
   if (event.ControlDown() && event.GetWheelAxis() == wxMOUSE_WHEEL_VERTICAL)
   {
      double amount = static_cast<double>(event.GetWheelRotation()) / static_cast<double>(event.GetWheelDelta());
      if (amount >= 0)
         amount = 1.0 + 0.25 * amount;
      else
         amount = 1.0 / (1.0 - 0.25 * amount);
      ZoomTime(amount);
   }
   else
      event.Skip();
   SetToolTip(nullptr);
}


/**
 * Item selected from the right click menu
 */
void TimeDilator::OnMenuSelection(wxCommandEvent &event)
{
   switch (event.GetId())
   {
   case ID_ZOOM_IN:
   {
      ZoomTime(2.0);
      break;
   }
   case ID_ZOOM_OUT:
   {
      ZoomTime(0.5);
      break;
   }
   case ID_ZOOM_NORMAL:
   {
      SetDilation(mStartLimit, mEndLimit);
      break;
   }
   case ID_CURRENT_TIME:
   {
      Real epoch = mCurrentTime;
      OFDateTimeDialog *time = new OFDateTimeDialog(epoch, mEndLimit, mCurrentTime, mTimeFormat, this, "Current Time");
      time->Centre();
      if (time->ShowModal() == wxID_OK)
      {
         if (epoch < mStartLimit - 1e-6)
            MessageInterface::PopupMessage(Gmat::ERROR_, "The new current time would be less than the earliest simulation time");
         else if (epoch > mEndLimit + 1e-6)
            MessageInterface::PopupMessage(Gmat::ERROR_, "The new current time would be greater than the latest simulation time");
         else
         {
            if (epoch < mStartLimit)
               epoch = mStartLimit;
            if (epoch > mEndLimit)
               epoch = mEndLimit;
            TriggerCurrentTimeEvent(epoch);
         }
      }
      time->Destroy();
      break;
   }
   case ID_START_TIME:
   {
      Real epoch = mStartTime;
      OFDateTimeDialog *time = new OFDateTimeDialog(epoch, mStartLimit, mCurrentTime, mTimeFormat, this, "Initial Time Dilation");
      time->Centre();
      if (time->ShowModal() == wxID_OK)
      {
         bool inLowerBound = ( epoch >= mStartLimit - 1e-6 );
         if (!inLowerBound)
            MessageInterface::PopupMessage(Gmat::ERROR_, "The new initial time dilation would be less than the earliest simulation time");
         else if (epoch >= mEndTime)
            MessageInterface::PopupMessage(Gmat::ERROR_, "The new initial time dilation would be greater than the final time dilation");
         else
         {
            if (epoch < mStartLimit)
               epoch = mStartLimit;
            SetDilation(epoch, mEndTime);
         }
      }
      time->Destroy();
      break;
   }
   case ID_END_TIME:
   {
      Real epoch = mEndTime;
      OFDateTimeDialog *time = new OFDateTimeDialog(epoch, mEndLimit, mCurrentTime, mTimeFormat, this, "Final Time Dilation");
      time->Centre();
      if (time->ShowModal() == wxID_OK)
      {
         bool inUpperBound = ( epoch <= mEndLimit + 1e-6 );
         if (!inUpperBound)
            MessageInterface::PopupMessage(Gmat::ERROR_, "The new final time dilation would be greater than the latest simulation time");
         else if (epoch <= mStartTime)
            MessageInterface::PopupMessage(Gmat::ERROR_, "The new final time dilation would be less than the initial time dilation");
         else
         {
            if (epoch > mEndLimit)
               epoch = mEndLimit;
            SetDilation(mStartTime, epoch);
         }
      }
      time->Destroy();
      break;
   }
   }
}


/**
 * Set the current time and trigger an event
 */
void TimeDilator::TriggerCurrentTimeEvent(Real time)
{
   SetCurrent(time);
   wxCommandEvent event(DIALATE_CURRENT_TIME, GetId());
   event.SetEventObject(this);
   // Do send it
   ProcessWindowEvent(event);
}


TimeDilator::TimeFraction TimeDilator::GetCurrentTimeFraction() const
{
   TimeFraction timeFrac;
   timeFrac.inRange = ( mCurrentTime >= mStartTime && mCurrentTime <= mEndTime );
   Real denominator = mEndTime - mStartTime;
   if (std::abs(denominator) < 1e-12)
      timeFrac.fraction = 1e-12;
   else
      timeFrac.fraction = (mCurrentTime - mStartTime) / denominator;
   return timeFrac;
}


void TimeDilator::SetCurrentTimeFraction(TimeFraction timeFrac)
{
   if (timeFrac.inRange)
      TriggerCurrentTimeEvent(mStartTime + timeFrac.fraction * (mEndTime - mStartTime));
}


void TimeDilator::ZoomTime(double zoomAmount)
{
   if (zoomAmount >= 1.0)
   {
      Real center, left, right;
      if (mCurrentTime >= mStartTime && mCurrentTime <= mEndTime)
      {
         center = mCurrentTime;
         left = (mCurrentTime - mStartTime) / zoomAmount;
         right = (mEndTime - mCurrentTime) / zoomAmount;
      }
      else
      {
         center = mStartTime + (mEndTime - mStartTime) / 2.0;
         left = (mEndTime - mStartTime) / 2.0 / zoomAmount;
         right = left;
      }
      SetDilation(center - left, center + right);
   }
   else
   {
      Real center, left, right;
      if (mCurrentTime >= mStartTime && mCurrentTime <= mEndTime)
      {
         center = mCurrentTime;
         left = (mCurrentTime - mStartTime) / zoomAmount;
         right = (mEndTime - mCurrentTime) / zoomAmount;
      }
      else
      {
         center = mStartTime + (mEndTime - mStartTime) / 2.0;
         left = (mEndTime - mStartTime) / 2.0 / zoomAmount;
         right = left;
      }
      Real start = center - left;
      Real end = center + right;
      if (start < mStartLimit)
      {
         // translate start up to mStartLimit
         start = mStartLimit;
         end = mStartLimit + left + right;
         // check the tail
         if (end > mEndLimit)
            end = mEndLimit;
      }
      else if (end > mEndLimit)
      {
         // translate end back to mEndLimit
         end = mEndLimit;
         start = mEndLimit - left - right;
         // check the head
         if (start > mStartLimit)
            start = mStartLimit;
      }
      bool isZoomFull = ( mStartTime == start && mEndTime == end );
      SetDilation(start, end, !isZoomFull);
   }
}
