//$Id: OFWindowProxyPanel.hpp rmathur $
//------------------------------------------------------------------------------
//                                  OFWindowProxyPanel
//------------------------------------------------------------------------------
// OpenFramesInterface Plugin for GMAT (General Mission Analysis Tool)
//
// Copyright (c) 2022 Emergent Space Technologies, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Developed by Emergent Space Technologies, Inc. under contract number
// NNX16CG16C
//
// Author: Ravi Mathur, Emergent Space Technologies, Inc.
// Created: December 1, 2015
/**
 *  @class OFWindowProxyPanel
 *  A GMAT plugin panel that manages a canvas for OpenFrames and toolbar buttons
 *  for interacting with OpenFrames.
 */
//------------------------------------------------------------------------------

#ifndef OFWINDOWPROXYPANEL_hpp
#define OFWINDOWPROXYPANEL_hpp

#include "OpenFramesInterface_defs.hpp"
#include "PluginWidget.hpp"

class GmatBase;
class OFScene;
class OFGLCanvas;
class OpenGLOptions;
class OpenFramesInterface;
class TimeDilator;

#include <wx/wxprec.h>
#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif


class OpenFramesInterface_API OFWindowProxyPanel : public PluginWidget, public wxPanel
{
public:
   /// Spacing between widgets on the toolbar
   static const int TOOL_BORDER_WIDTH = 2;

   // Text for tooltips
   static const std::string TOOLTIP_PLAY_ANIMATION;
   static const std::string TOOLTIP_PAUSE_ANIMATION;
   static const std::string TOOLTIP_PLAY_LINKED;
   static const std::string TOOLTIP_PAUSE_LINKED;
   static const std::string TOOLTIP_POSITIVE_SCALE;
   static const std::string TOOLTIP_NEGATIVE_SCALE;


   OFWindowProxyPanel(GmatBase *forObject, wxWindow *parent);
   virtual ~OFWindowProxyPanel();

   std::string GetForObjectName() const;

   // Override existing wxWindow interfaces
   bool Destroy() override;

   // Additional interfaces
   void SetObject(OpenFramesInterface *object);
   void RemoveObject(const OpenFramesInterface *object);
   void SetCanvas(OFGLCanvas *canvas, bool shaped);
   OFScene &GetScene();

   void ShowToolbar(bool show);
   void SetListOfViews(const wxArrayString &views);
   void AppendToListOfViews(const wxString &view);
   void SetCurrentViewByIndex(int current);
   wxString GetCurrentViewString();
   void SelectViewByString(const wxString &view);
   void SetTimeSliderLimits(Real initialTime, Real finalTime);
   void SetTimeSliderValue(double timeOffset);
   void SetTimeScaleValue(Real scale, bool updateText=true);
   void IncreaseScale();
   void DecreaseScale();
   void SetPlayPauseMode(bool setToPlay);
   void ToggleScaleSign();

   void OnToolbar(wxCommandEvent &event);
   void OnViewSelect(wxCommandEvent &event);
   void OnTimeSlider(wxCommandEvent &event);
   void MouseWheel(wxMouseEvent& event);

private:
   static const wxBitmap FORWARD_BITMAP;
   static const wxBitmap BACK_BITMAP;

   /// Stored Reference to the object that this panel was created for
   OpenFramesInterface *mForObject;
   /// The drawing canvas for this instance
   OFScene *mScene;
   /// The toolbar for this instance
   wxBoxSizer *mToolSizer;
   /// The list of possible views
   wxComboBox *mViewList;
   /// Static text for displaying the current time scale
   wxTextCtrl *mScaleText;
   /// A button for toggling +/- time scale
   wxButton *mScaleSignButton;
   /// The time scale currently display in the text control
   Real mScale;
   /// The sign of the time scale
   int mScaleSign;
   /// True if the play/pause button state is play (UI in play mode)
   bool mIsPlaying;
   /// True if GUI is showing time sync, false otherwise
   bool mShowSyncedButtons;
   /// True if the current time scale is valid, false if the time scale is invalid (red)
   bool mScaleValid;
   /// The normal foreground color for mScaleText
   wxColour mScaleForegroundColor;
   /// The normal background color for mScaleText
   wxColour mScaleBackgroundColor;
   /// The play/pause button
   wxButton *mPlayPauseButton;
   /// The slower button
   wxButton *mSlowerButton;
   /// The faster button
   wxButton *mFasterButton;
   /// The time manipulation control
   TimeDilator *mTimeDilator;

   void SetScaleText(Real scaleFactor);
   Real UpByTwo(Real init);
   Real DownByTwo(Real init);

   void ResizeViewComboBox();

   wxDECLARE_EVENT_TABLE();

   enum
   {
      ID_PLAY_PAUSE = 15650,
      ID_SLOWER,
      ID_FASTER,
      ID_SCALE_SIGN,
      ID_SCALE_TEXT,
      ID_RESET_VIEW,
      ID_VIEW_SELECT,
      ID_TIME_SLIDER,
      ID_TIME_DILATOR,
   };
};

#endif