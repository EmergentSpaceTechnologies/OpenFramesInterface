//$Id$
//------------------------------------------------------------------------------
//                                  OFConfigurationPanel
//------------------------------------------------------------------------------
// OpenFramesInterface Plugin for GMAT (General Mission Analysis Tool)
//
// Copyright (c) 2022 Emergent Space Technologies, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Developed by Emergent Space Technologies, Inc. under contract number
// NNX16CG16C
//
// Author: Matthew Ruschmann, Emergent Space Technologies, Inc.
// Created: August 21, 2017
/**
 *  @class OFConfigurationPanel
 *  Panel for configuring an OpenFramesInterace object instance
 */
//------------------------------------------------------------------------------

#ifndef OFCONFIGURATIONPANEL_hpp
#define OFCONFIGURATIONPANEL_hpp

#include "wx/wx.h"
#include <wx/spinctrl.h>

#include "OpenFramesInterface_defs.hpp"
#include "PluginWidget.hpp"

// Forward declaration of referenced types
class OpenFramesInterface;
class OpenFramesView;
class OpenFramesVector;
class OpenFramesSensorMask;
class GmatBase;


class OFConfigurationPanel: public PluginWidget, public wxPanel
{
public:
   const Integer BORDER_SIZE = 2; // border size

   OFConfigurationPanel(GmatBase *forObject, wxWindow *parent);
   virtual ~OFConfigurationPanel();

   // From PluginWidget
   virtual void RenameObject(const std::string &oldName,
                             const std::string newName,
                             const UnsignedInt ofType);
   virtual void UpdateObjectList(const UnsignedInt ofType);

protected:
   /// The maximum number of existing views
   static const int MAX_EXISTING_VIEWS = 100U;

   /// The maximum number of existing vectors
   static const int MAX_EXISTING_VECTORS = 100U;

   /// The maximum number of existing sensor masks
   static const int MAX_EXISTING_MASKS = 100U;
   
   /// The object that this panel is configuring
   OpenFramesInterface *mOFInterface;
   /// A buffer object to store changes until the changes are applied
   OpenFramesInterface *mBufferInterface;

   /// True when Real data field(s) have changes that can be applied
   bool mHasRealDataChanged;
   /// True when drawing option data field(s) have changes that can be applied
   bool mHasDrawingOptionChanged;
   /// True when list of selected objects has changes that can be applied
   bool mHasSpChanged;
   /// True when Integer data field(s) have changes that can be applied
   bool mHasIntegerDataChanged;

   /// True when object(s) configuration has changes that can be applied
   bool mHasShowObjectChanged;
   /// True when coordinate system has changes that can be applied
   bool mHasCoordSysChanged;
   /// True when stars enable/disable has changes that can be applied
   bool mHasStarOptionChanged;
   /// True when the list of selected views has changes that can be applied
   bool mHasViewsChanged;
   /// True when the list of selected vectors has changes that can be applied
   bool mHasVectorsChanged;
   /// True when the list of selected sensor masks has changes that can be applied
   bool mHasMasksChanged;
   /// True when mBufferInterface has changes that can be applied
   bool mHasBufferChanged;
   /// True when time sync setting has changed
   bool mHasTimeSyncChanged;
   /// True when any data has changed such that the it could be applied
   bool mDataChanged;
   /// False if the window is not allowed to close (currently always true)
   bool mCanClose;

   /// The number of selected spacecraft
   int  mScCount;
   /// The number of selected celestial bodies (non-spacecraft)
   int  mNonScCount;

   /// The name of the selected spacecraft or celestial body that is
   /// currently highlighted for configuration
   std::string mSelSpName;

   /// Maps spacecraft or bodies to buffered value for draw object option
   std::map<std::string, bool> mDrawObjectMap;
   /// Maps spacecraft or bodies to buffered value for draw trajectory option
   std::map<std::string, bool> mDrawTrajectoryMap;
   /// Maps spacecraft or bodies to buffered value for draw local axes option
   std::map<std::string, bool> mDrawAxesMap;
   /// Maps spacecraft or bodies to buffered value for draw local axes option
   std::map<std::string, bool> mDrawXYPlaneMap;
   /// Maps spacecraft or bodies to buffered value for draw object label option
   std::map<std::string, bool> mDrawLabelMap;
   /// Maps spacecraft or bodies to buffered value for use propagate label
   /// instead of object label option
   std::map<std::string, bool> mUsePropagateLabelMap;
   /// Maps spacecraft or bodies to buffered value for draw center option
   std::map<std::string, bool> mDrawCenterPointMap;
   /// Maps spacecraft or bodies to buffered value for draw end points option
   std::map<std::string, bool> mDrawEndPointsMap;
   /// Maps spacecraft or bodies to buffered value for draw velocity option
   std::map<std::string, bool> mDrawVelocityMap;
   /// Maps spacecraft or bodies to buffered value for draw lat/lon grid option
   std::map<std::string, bool> mDrawGridMap;
   /// Maps spacecraft or bodies to buffered value for draw line width option
   std::map<std::string, Real> mDrawLineWidthMap;
   /// Maps spacecraft or bodies to buffered value for marker size option
   std::map<std::string, UnsignedInt> mDrawMarkerSizeMap;
   /// Maps spacecraft or bodies to buffered value for marker font option
   std::map<std::string, UnsignedInt> mDrawFontSizeMap;
   /// Maps spacecraft or bodies to buffered value for marker font option
   std::map<std::string, std::string> mDrawFontPositionMap;

   /// List of spacecraft excluded (not selected) from the plot
   wxArrayString mExcludedScList;
   /// List of celestial bodies excluded (not selected) from the plot
   wxArrayString mExcludedCelesPointList;
   
   /// List of existing views used for the "add existing" popup menu
   StringArray mExistingViews;

   /// List of existing vectors used for the "add existing" popup menu
   StringArray mExistingVectors;
   
   /// List of existing sensor masks used for the "add existing" popup menu
   StringArray mExistingMasks;

   /// Static text next to marker size for enabling/disabling the widget
   wxStaticText *mMarkerSizeStaticText;

   /// To enable showing the plot
   wxCheckBox *mShowPlotCheckBox;
   /// To enable showing the toolbar
   wxCheckBox *mShowToolbarCheckBox;
   /// To enable drawing the X-Y plane
   wxCheckBox *mXYPlaneCheckBox;
   /// To enable drawing the ecliptic plane
   wxCheckBox *mEclipticCheckBox;
   /// Tto enable global coordinate frame axes
   wxCheckBox *mAxesCheckBox;
   /// To enable labels on global coordinate frame axes
   wxCheckBox *mShowAxesLabelsCheckBox;
   /// To enable name label of global coordinate frame
   wxCheckBox *mFrameLabelCheckBox;
   /// To enable drawing of a spacecraft or body object
   wxCheckBox *mDrawObjectCheckBox;
   /// To enable trajectory for a spacecraft or body object
   wxCheckBox *mDrawTrajectoryCheckBox;
   /// To enable local axes for a spacecraft or body object
   wxCheckBox *mDrawAxesCheckBox;
   /// To enable XYPlane for a spacecraft or body object
   wxCheckBox *mDrawXYPlaneCheckBox;
   /// To enable center point for a spacecraft or body object
   wxCheckBox *mDrawCenterPointCheckBox;
   /// To enable end points for a spacecraft or body object
   wxCheckBox *mDrawEndPointsCheckBox;
   /// To enable velocity markers for a spacecraft or body object
   wxCheckBox *mDrawVelocityCheckBox;
   /// To enable lat/lon grid for a spacecraft or body object
   wxCheckBox *mDrawGridCheckBox;
   /// To enable drawing of stars
   wxCheckBox *mEnableStarsCheckBox;
   /// To draw body name label for a spacecraft or body object
   wxCheckBox *mDrawBodyLabelBox;
   /// To draw propagate command label for a spacecraft
   wxCheckBox *mDrawPropLabelBox;

   /// To set last N solver trajectories to draw
   wxSpinCtrl *mSILastNCtrl;
   /// To set line width for a spacecraft or body object's trajectory
   wxSpinCtrlDouble *mLineWidthTextCtrl;
   /// To set center marker size for a spacecraft or body object
   wxSpinCtrl *mMarkerSizeTextCtrl;
   /// To set font size for a spacecraft or body object
   wxSpinCtrl *mFontSizeSpinCtrl;
   /// To set font size for a spacecraft or body object
   wxSpinCtrlDouble *mAxesCheckBoxSpinCtrl;

   wxListBox *mSpacecraftListBox;
   wxListBox *mCelesPointListBox;
   wxListBox *mSelectedScListBox;
   wxListBox *mSelectedObjListBox;
   wxListBox *mSelectedViewListBox;
   wxListBox *mSelectedVectorListBox;
   wxListBox *mSelectedMaskListBox;

   wxButton *mStarsButton;
   wxButton *addScButton;
   wxButton *removeScButton;
   wxButton *clearScButton;
   wxButton *mViewEditButton;
   wxButton *mViewNewButton;
   wxButton *mViewAddButton;
   wxButton *mViewDeleteButton;
   wxButton *mViewUpButton;
   wxButton *mViewDownButton;
   wxButton *mVectorEditButton;
   wxButton *mVectorNewButton;
   wxButton *mVectorAddButton;
   wxButton *mVectorDeleteButton;
   wxButton *mVectorUpButton;
   wxButton *mVectorDownButton;
   wxButton *mMaskEditButton;
   wxButton *mMaskNewButton;
   wxButton *mMaskAddButton;
   wxButton *mMaskDeleteButton;
   wxButton *mMaskUpButton;
   wxButton *mMaskDownButton;
   wxButton *mGLOptionsButton;
   wxComboBox *mSyncParentCombobox;

   wxComboBox *mSolverIterComboBox;
   wxComboBox *mCoordSysComboBox;
   wxComboBox *mFontPositionComboBox;

   wxFlexGridSizer *mObjectSizer;
   wxBoxSizer *mScOptionSizer;

   /// A sizer to separate dialog buttons from the content page
   wxBoxSizer *mPanelSizer;
   /// A sizer for the content page
   wxSizer *mMiddleSizer;
   /// The top-level sizer for the dialog
   wxStaticBoxSizer *mBottomSizer;

   /// Script button, shows script dialog
   wxButton *mScriptButton;
   /// OK button, applies and closes
   wxButton *mOkButton;
   /// Apply button, does not close
   wxButton *mApplyButton;
   /// Cancel button, prompts to apply then closes
   wxButton *mCancelButton;
   /// About button displays about info
   wxButton *mAboutButton;
   /// Help button displays help
   wxButton *mHelpButton;

   // for initializing data
   void InitializeData();

   // methods based on GmatPanel
   virtual void Create();
   void Refresh();
   virtual void Show();
   void LoadData();
   void SaveData();

   // event handler
   void OnAddSpacePoint(wxCommandEvent& event);
   void OnRemoveSpacePoint(wxCommandEvent& event);
   void OnClearSpacePoint(wxCommandEvent& event);
   void OnSelectAvailObject(wxCommandEvent& event);
   void OnSelectSpacecraft(wxCommandEvent& event);
   void OnSelectOtherObject(wxCommandEvent& event);
   void OnCheckBoxChange(wxCommandEvent& event);
   void OnComboBoxChange(wxCommandEvent& event);
   void OnComboBoxDropdown(wxCommandEvent& event);
   void OnSpinChange(wxSpinEvent& event);
   void OnSpinChangeDouble(wxSpinDoubleEvent& event);
   void OnNewView(wxCommandEvent &event);
   void OnNewVector(wxCommandEvent &event);
   void OnNewMask(wxCommandEvent &event);
   void OnAddView(wxCommandEvent &event);
   void OnAddVector(wxCommandEvent &event);
   void OnAddMask(wxCommandEvent &event);
   void OnExistingView(wxCommandEvent &event);
   void OnExistingVector(wxCommandEvent &event);
   void OnExistingMask(wxCommandEvent &event);
   void OnDeleteView(wxCommandEvent &event);
   void OnDeleteVector(wxCommandEvent &event);
   void OnDeleteMask(wxCommandEvent &event);
   void OnEditView(wxCommandEvent &event);
   void OnEditVector(wxCommandEvent &event);
   void OnEditMask(wxCommandEvent &event);
   void OnSelectView(wxCommandEvent& event);
   void OnSelectVector(wxCommandEvent& event);
   void OnSelectMask(wxCommandEvent& event);
   void OnViewUp(wxCommandEvent& event);
   void OnVectorUp(wxCommandEvent &event);
   void OnMaskUp(wxCommandEvent &event);
   void OnViewDown(wxCommandEvent& event);
   void OnVectorDown(wxCommandEvent &event);
   void OnMaskDown(wxCommandEvent &event);
   void OnStarOptions(wxCommandEvent& event);
   void OnGLOptions(wxCommandEvent& event);

   void OnApply(wxCommandEvent &event);
   void OnOK(wxCommandEvent &event);
   void OnCancel(wxCommandEvent &event);
   void OnAbout(wxCommandEvent &event);
   void OnHelp(wxCommandEvent &event);
   void OnScript(wxCommandEvent &event);

   void EnableUpdate(bool enable);

   DECLARE_EVENT_TABLE();

   // IDs for the controls and the menu commands
   enum
   {
      ID_BUTTON_OK = 8000, ///< Unique ID for OK button
      ID_BUTTON_APPLY,     ///< Unique ID for Apply button
      ID_BUTTON_CANCEL,    ///< Unique ID for Cancel button
      ID_BUTTON_ABOUT,     ///<Unique ID for About button
      ID_BUTTON_HELP,      ///< Unique ID for Help button
      ID_BUTTON_SCRIPT,    ///< Unique ID for Show Script button
   };
   enum
   {
      ID_SPINCTRL = 15600, ///< ID for all spin controls
      ID_COMBOBOX,         ///< ID for all comboboxes
      ID_LISTBOX,          ///< ID for available celestial body and spacecraft listboxes
      SC_SEL_LISTBOX,      ///< Unique ID for selected spacecraft listbox
      OBJ_SEL_LISTBOX,     ///< Unique ID for selected celestial body listbox
      NEW_VIEW_BUTTON,     ///< Unique ID for new view button
      ADD_VIEW_BUTTON,     ///< Unique ID for add view button
      DELETE_VIEW_BUTTON,  ///< Unique ID for delete view button
      EDIT_VIEW_BUTTON,    ///< Unique ID for edit view button
      VIEW_SEL_LISTBOX,    ///< Unique ID for views listbox
      VIEW_UP_BUTTON,      ///< Unique ID for move view up button
      VIEW_DOWN_BUTTON,    ///< Unique ID for move view down button
      VECTOR_SEL_LISTBOX,  ///< Unique ID for vector listbox
	   NEW_VECTOR_BUTTON,   ///< Unique ID for new vector button
	   ADD_VECTOR_BUTTON,   ///< Unique ID for add vector button
	   DELETE_VECTOR_BUTTON,///< Unique ID for delete vector button
	   EDIT_VECTOR_BUTTON,  ///< Unique ID for edit vector button
	   VECTOR_UP_BUTTON,    ///< Unique ID for move vector up button
	   VECTOR_DOWN_BUTTON,  ///< Unique ID for move vector down button
      MASK_SEL_LISTBOX,    ///< Unique ID for mask listbox
      NEW_MASK_BUTTON,     ///< Unique ID for new mask button
      ADD_MASK_BUTTON,     ///< Unique ID for add mask button
      DELETE_MASK_BUTTON,  ///< Unique ID for delete mask button
      EDIT_MASK_BUTTON,    ///< Unique ID for edit mask button
      MASK_UP_BUTTON,      ///< Unique ID for move mask up button
      MASK_DOWN_BUTTON,    ///< Unique ID for move mask down button
      CHECKBOX,            ///< ID for all checkboxes
      ADD_SP_BUTTON,       ///< Unique ID for add object button
      REMOVE_SP_BUTTON,    ///< Unique ID for remove object button
      CLEAR_SP_BUTTON,     ///< Unique ID for clear objects button
      STAR_OPTIONS,        ///< Unique ID for star options button
      GL_OPTIONS,          ///< ID for OpenGL options button
      START_VIEW_MENU,     ///< Unique ID for selecting views in the add view popup
      END_VIEW_MENU = START_VIEW_MENU + MAX_EXISTING_VIEWS,
      START_VECTOR_MENU,     ///< Unique ID for selecting vectors in the add vector popup
      END_VECTOR_MENU = START_VECTOR_MENU + MAX_EXISTING_VECTORS,
      START_MASK_MENU,     ///< Unique ID for selecting sensor masks in the add mask popup
      END_MASK_MENU = START_MASK_MENU + MAX_EXISTING_MASKS,
   };

private:
   /// Up arrow image
   static const wxBitmap BITMAP_UP_ARROW;
   /// Down arrow image
   static const wxBitmap BITMAP_DOWN_ARROW;
   /// Left arrow image
   static const wxBitmap BITMAP_LEFT_ARROW;
   /// Right arrow image
   static const wxBitmap BITMAP_RIGHT_ARROW;
   /// Remove icon image
   static const wxBitmap BITMAP_REMOVE;

   /// Client data specifying a spacecraft
   static Gmat::ObjectType SPACECRAFT_CLIENT_DATA;
   /// Client data specifying a ground station
   static Gmat::ObjectType GROUND_STATION_CLIENT_DATA;
   /// Client data specifying a celestial body
   static Gmat::ObjectType CELESTIAL_BODY_CLIENT_DATA;

   OpenFramesInterface *GetCloneFromSandbox();
   void RefreshPossibleParents();
   void UpdateViewButtons();
   void UpdateVectorButtons();
   void UpdateMaskButtons();
   void ShowSpacePointOption(const wxString &name, Gmat::ObjectType objType);
   int EditViewDialog(OpenFramesView &view, bool enableUpdate);
   int EditVectorDialog(OpenFramesVector &vector, bool enableUpdate);
   int EditMaskDialog(OpenFramesSensorMask &mask, bool enableUpdate);
   void CreateNewView(std::string name);
   void CreateNewVector(std::string name);
   void CreateNewMask(std::string name);
   void ShowScriptDialog(const std::string &title, const std::string &script);
};

#endif
