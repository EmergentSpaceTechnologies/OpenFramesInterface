//$Id$
//------------------------------------------------------------------------------
//                                  OFDateTimeDialog
//------------------------------------------------------------------------------
// OpenFramesInterface Plugin for GMAT (General Mission Analysis Tool)
//
// Copyright (c) 2022 Emergent Space Technologies, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Developed by Emergent Space Technologies, Inc. under contract number
// NNX16CG16C
//
// Author: Matthew Ruschmann, Emergent Space Technologies, Inc.
// Created: August 21, 2017
/**
 *  @class OFDateTimeDialog
 *  Dialog for setting a time
 */
//------------------------------------------------------------------------------

#ifndef OFDATETIMEDIALOG_hpp
#define OFDATETIMEDIALOG_hpp

#include <wx/wx.h>
#include <wx/spinctrl.h>

#include "OpenFramesInterface_defs.hpp"

// Forward declaration of referenced types
class OpenFramesInterface;


class OpenFramesInterface_API OFDateTimeDialog: public wxDialog
{
public:
   static const wxString DIALOG_TITLE;

   OFDateTimeDialog(Real &epoch, Real defaultValue, Real currentValue, std::string &timeFormat, wxWindow *parent, const char *title);
   virtual ~OFDateTimeDialog();

protected:
   /// True when the data has changed and can be applied
   bool mDataChanged;
   /// False if the dialog is not allowed to close (always true for now)
   /// NOTE: Why is this necessary? Can it be removed?
   bool mCanClose;

   /// The time value that shall be modified in A1ModJulian
   Real &mEpoch;
   /// The default time value in A1ModJulian (set when the defaults button is pushed)
   Real mDefault;
   /// The current time value in A1ModJulian (set when the current button is pushed)
   Real mCurrent;
   /// Holds the current time format
   std::string &mTimeFormat;

   /// A combo box to select the format of the epoch in mEpochValue text control
   wxComboBox *mEpochFormatComboBox;
   /// The value of the epoch that shall be applied to mDefault
   wxTextCtrl *mEpochValue;

   /// A sizer to separate dialog buttons from the content page
   wxBoxSizer *mPanelSizer;
   /// A sizer for the content page
   wxSizer *mMiddleSizer;
   /// The top-level sizer for the dialog
   wxStaticBoxSizer *mBottomSizer;

   /// The OK button, applies and closes
   wxButton *mOkButton;
   /// The Cancel button, prompts then closes
   wxButton *mCancelButton;
   /// The default button, sets the value in mEpochValue to mDefault
   wxButton *mDefaultButton;
   /// The current button, sets the value in mEpochValue to the current time offset
   wxButton *mCurrentButton;

   // for initializing data
   void InitializeData();

   // methods based on GmatPanel
   virtual void Create();
   virtual void Show();
   void LoadData();
   void SaveData();

   // event handler
   void OnTextChange(wxCommandEvent& event);
   void OnComboboxChange(wxCommandEvent& event);

   void OnOK(wxCommandEvent &event);
   void OnCancel(wxCommandEvent &event);
   void OnSystemClose(wxCloseEvent& event);
   void OnDefault(wxCommandEvent &event);
   void OnCurrent(wxCommandEvent &event);

   void EnableUpdate(bool enable);

   DECLARE_EVENT_TABLE();

   // IDs for the controls and the menu commands
   enum
   {
      ID_BUTTON_OK = 8000, ///< Unique ID for OK button
      ID_BUTTON_CANCEL,    ///< Unique ID for Cancel button
   };
   enum
   {
      ID_COMBOBOX = 15600, ///< Unique ID for the Epoch format combo box
      ID_TEXT,             ///< ID for all static text
      ID_TEXTCTRL,         ///< Unique ID for the Epoch text box
      ID_BUTTON_DEFAULT,   ///< Unique ID for Default button
      ID_BUTTON_CURRENT,   ///< Unique ID for Default button
   };
};

#endif
