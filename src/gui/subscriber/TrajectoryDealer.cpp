//$Id: TrajectoryDealer.hpp rmathur $
//------------------------------------------------------------------------------
//                                  TrajectoryDealer
//------------------------------------------------------------------------------
// OpenFramesInterface Plugin for GMAT (General Mission Analysis Tool)
//
// Copyright (c) 2022 Emergent Space Technologies, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Developed by Emergent Space Technologies, Inc. under contract number
// NNX16CG16C
//
// Author: Matthew Ruschmann, Emergent Space Technologies, Inc.
// Created: February 6, 2018
/**
 *  TrajectoryDealer class
 *  Singleton that manages trajectories so that multiple windows can use the
 *  same trajectory object, which reduces memory usage for large trajectories.
 */
//------------------------------------------------------------------------------

#include "TrajectoryDealer.hpp"
#include "TrajectoryListener.hpp"
#include "OFRenderPool.hpp"

#include <OpenFrames/Trajectory.hpp>

#include <algorithm>


// Initialize static members
TrajectoryDealer trajectoryPoolInstance;


/**
 * Gets singleton
 *
 * @return The singleteon render pool
 */
TrajectoryDealer &TrajectoryDealer::Instance()
{
   return trajectoryPoolInstance;
}


/**
 * Default constructor
 */
TrajectoryDealer::TrajectoryDealer()
{
   // Nothing to do here
}


/**
 * Destructor
 */
TrajectoryDealer::~TrajectoryDealer()
{
   // Nothing to do here
}


/**
 * Adds given state information to the trajectory
 *
 * @param t The time since the Epoch (seconds)
 * @param x x-axis position (meters)
 * @param y y-axis position (meters)
 * @param z z-axis position (meters)
 * @param vx x-axis velocity (meters/second)
 * @param vy y-axis velocity (meters/second)
 * @param vz z-axis velocity (meters/second)
 * @param qx x-vector element of attitude quaternion
 * @param qy y-vector element of attitude quaternion
 * @param qz z-vector element of attitude quaternion
 * @param qw scalar element of attitude quaternion
 */
void TrajectoryDealer::AddState(const std::string &bodyName, const std::string &frameName, const std::string &propName,
                                GmatBase *propID, Real t, const Real* posvel, const Real* att, bool isSolving,
                                bool mergeAllSegments)
{
   if (isSolving)
   {
      // Check if a new solution segment needs to be created
      std::vector<TrajectoryDealer::SegmentData> &sols = FindSolutionsFor(bodyName, frameName);
      if (sols.size() == 0 || sols.back().finalized)
      {
         sols.emplace_back();
         sols.back().propName = propName;
         sols.back().ids.push_back(propID);
         sols.back().traj = new OpenFrames::Trajectory(3U, 1U);
         sols.back().traj->autoInformSubscribers(false);
         sols.back().finalized = false;
         sols.back().merge = false;
         // notify listeners of the new solution
         for (auto l = mListeners.begin(); l < mListeners.end(); l++)
         {
            if ((*l)->IsNameSameAs(bodyName)  && (*l)->IsFrameSameAs(frameName))
                (*l)->AddSolution(sols.back().traj.get());
         }
         mRemovedCount[frameName] = 0;
      }
      // Add the state to the last solution segment
      OpenFrames::Trajectory::DataType begin, end;
      bool isNew = !sols.back().traj->getTimeRange(begin, end); // end is the last time added, new if invalid range
      if (!isNew)
      {
         // Verify that the segment will continue to be monotonic in time
         if (begin < end)
            isNew = t > end;
         else if (end > begin)
            isNew = t < begin;
         else
            isNew = t != end;
      }
      if (isNew)
      {
         double vel[3] = { posvel[0] + 1000.0*posvel[3],
            posvel[1] + 1000.0*posvel[4],
            posvel[2] + 1000.0*posvel[5] };
         sols.back().traj->addTime(t);
         sols.back().traj->addPosition(posvel);
         sols.back().traj->addAttitude(att);
         sols.back().traj->setOptional(0U, vel);
         sols.back().traj->informSubscribers();
      }
   }
   else
   {
      std::vector<TrajectoryDealer::SegmentData> &segs = FindSegmentsFor(bodyName, frameName);

      // If segments will be merged (e.g. celestial body) and the incoming time is within
      // the previous segment's time range, then there's no point creating a new segment.
      // In this case, just make sure the previous segment will accept new points.
      if(mergeAllSegments && (segs.size() > 0))
      {
         double timeDist = segs.back().traj->getTimeDistance(t);
         if(timeDist <= 0.0) segs.back().finalized = false;
      }

      // Check if the previous segment should be continued
      // First check that the most recent trajectory can be continued
      if (segs.size() > 0 && segs.back().finalized && IsTrajectoryEndpointAt(segs.back(), t, posvel))
      {
         if (std::find(segs.back().ids.begin(), segs.back().ids.end(), propID) < segs.back().ids.end())
         {
            // check that the segment is visible in all listeners
            if (IsSegmentShownInAllListeners(bodyName, frameName, segs.back()))
               // The most recent trajectory is from the same propagate command, open it for continuation
               segs.back().finalized = false;
         }
         else
         {
            // Check if any earlier segments can be continued (When looping over multiple propagates)
            for (int ii = 0; ii + 1 < segs.size(); ii++)
            {
               // Is this the same propagator being called
               if (std::find(segs[ii].ids.begin(), segs[ii].ids.end(), propID) < segs[ii].ids.end())
               {
                  // Check that segments are all forward or all backward
                  bool ableToConcatenate = true;
                  int directionFlag = 0;
                  for (int jj = ii; ableToConcatenate && jj < segs.size(); jj++)
                  {
                     // check that segment is visible in all listeners
                     ableToConcatenate = ableToConcatenate && IsSegmentShownInAllListeners(bodyName, frameName, segs[jj]);
                     // direction check
                     ableToConcatenate = ableToConcatenate && SameDirectionAs(directionFlag, segs[jj]);
                     if (ii < jj)
                        ableToConcatenate = ableToConcatenate && IsTrajectoryEndpointsConnected(segs[jj-1], segs[jj]);
                  }
                  // check that the last segment is visible in all listeners
                  ableToConcatenate = ableToConcatenate && IsSegmentShownInAllListeners(bodyName, frameName, segs.back());
                  if (ableToConcatenate)
                  {
                     // Concatenate trajectories, open last segment
                     OFRenderPool::Instance().LockScenesWithRoot(frameName);
                     for (int jj = segs.size() - 1; jj > ii; jj--)
                     {
                        ConcatenateSegments(segs[ii], segs[jj]);
                        for (auto l = mListeners.begin(); l < mListeners.end(); l++)
                        {
                           if ((*l)->IsNameSameAs(bodyName)  && (*l)->IsFrameSameAs(frameName))
                              (*l)->RemoveSegment(segs[jj].traj.get());
                        }
                        segs.erase(segs.begin() + jj);
                     }
                     segs[ii].finalized = false;
                     OFRenderPool::Instance().UnlockScenesWithRoot(frameName);
                     break;
                  }
               }
            }
         }
      }
      // Check if this is a new segment
      if (segs.size() == 0 || segs.back().finalized)
      {
         OFRenderPool::Instance().LockScenesWithRoot(frameName);
         segs.emplace_back();
         segs.back().propName = propName;
         segs.back().ids.push_back(propID);
         segs.back().traj = new OpenFrames::Trajectory(3U, 1U);
         segs.back().traj->autoInformSubscribers(false);
         segs.back().finalized = false;
         segs.back().merge = mergeAllSegments;
         // notify listeners of the new segment
         for (auto l = mListeners.begin(); l < mListeners.end(); l++)
         {
            if ((*l)->IsNameSameAs(bodyName)  && (*l)->IsFrameSameAs(frameName))
               (*l)->AddSegment(propName, segs.back().traj.get());
         }
         OFRenderPool::Instance().UnlockScenesWithRoot(frameName);
      }
      // Add the state
      OpenFrames::Trajectory::DataType begin, end;
      bool isNew = !segs.back().traj->getTimeRange(begin, end);
      if (!isNew)
      {
         // verify that the segment will continue to be monotonic in time
         if (begin < end)
            isNew = t > end;
         else if (end > begin)
            isNew = t < begin;
         else
            isNew = t != end;
      }
      if (isNew)
      {
         double vel[3] = { posvel[0] + 1000.0*posvel[3],
            posvel[1] + 1000.0*posvel[4],
            posvel[2] + 1000.0*posvel[5] };
         segs.back().traj->addTime(t);
         segs.back().traj->addPosition(posvel);
         segs.back().traj->addAttitude(att);
         segs.back().traj->setOptional(0U, vel);
         segs.back().traj->informSubscribers();
         // check for a color change whenever points are added
         for (auto l = mListeners.begin(); l < mListeners.end(); l++)
         {
            if ((*l)->IsNameSameAs(bodyName)  && (*l)->IsFrameSameAs(frameName) && (*l)->IsSpacecraft())
                (*l)->UpdateColor();
         }
      }
   }
}


/**
 * Combines all of the given segments into the first given segment
 *
 * @param[in] bodyName The name of the body that segments are for
 * @param[in] frameName The name of the frame that segments are in
 * @param[inout] segments
 *
 * This algorithm could be more efficient if it existed within
 * OpenFrames::Trajectory because it could work directly with the std::vector
 * objects. There are efficient algorithms for combining and sorting
 * std::vectors.
 */
void TrajectoryDealer::CombineAllSegments(const std::string &bodyName, const std::string &frameName,
                                           std::vector<TrajectoryDealer::SegmentData> &segments)
{
   if (segments.size() > 1)
   {
      std::vector<TrajectoryDealer::SegmentData> segsToMerge = segments;

      // Duplicate the first trajectory so that the actual one can be reused
      segsToMerge[0].traj = new OpenFrames::Trajectory(3U, 1U);
      const OpenFrames::Trajectory::DataArray &times = segments[0].traj->getTimeList();
      for (int ii = 0; ii < segments[0].traj->getNumTimes(); ii++)
         CopyPointToEndOfTrajectory(segments[0].traj, ii, segsToMerge[0].traj);

      // Initialize
      OpenFrames::Trajectory::DataType t0, tf, earliest;
      int indexOfSeg; // The segment containing the earliest time
      int indexInSeg; // The index of the eariest time within that segment
      std::vector<bool> isForward(segsToMerge.size()); // true if the corresponding entry in segments is forward
      std::vector<int> index(segsToMerge.size()); // index of the earliest time in the corresponding entry of segments
      for (int jj = 0; jj < segsToMerge.size(); jj++)
      {
         bool valid = segsToMerge[jj].traj->getTimeRange(t0, tf);
         if (valid)
         {
            isForward[jj] = ( t0 <= tf );
            if (isForward[jj])
            {
               index[jj] = 0;
               if (jj == 0 || t0 < earliest)
               {
                  earliest = t0;
                  indexOfSeg = jj;
                  indexInSeg = 0;
               }
            }
            else
            {
               index[jj] = segsToMerge[jj].traj->getNumTimes() - 1;
               if (jj == 0 || tf < earliest)
               {
                  earliest = tf;
                  indexOfSeg = jj;
                  indexInSeg = index[jj];
               }
            }
         }
         else
         {
            isForward[jj] = true;
            index[jj] = 0;
         }
      }

      // Start the new trajectory with the earliest point
      int count = 0;
      for (auto seg = segsToMerge.begin(); seg < segsToMerge.end(); seg++)
         count = count + static_cast<int>(seg->traj->getNumTimes());
      segments[0].traj->clear();
      segments[0].traj->reserveMemory(count);
      CopyPointToEndOfTrajectory(segsToMerge[indexOfSeg].traj, indexInSeg, segments[0].traj);
      if (isForward[indexOfSeg])
         index[indexOfSeg]++;
      else
         index[indexOfSeg]--;

      // Merge trajectories by repeatedly finding the earliest point of all remaining points
      while (segments[0].traj->getNumTimes() < count)
      {
         bool valid = false;
         for (int jj = 0; jj < segsToMerge.size(); jj++)
         {
            if (isForward[jj] && index[jj] < segsToMerge[jj].traj->getNumTimes())
            {
               if (!valid)
               {
                  valid = true;
                  indexOfSeg = jj;
                  indexInSeg = index[jj];
                  earliest = segsToMerge[jj].traj->getTimeList()[indexInSeg];
               }
               else
               {
                  OpenFrames::Trajectory::DataType t = segsToMerge[jj].traj->getTimeList()[index[jj]];
                  if (t < earliest)
                  {
                     indexOfSeg = jj;
                     indexInSeg = index[jj];
                     earliest = t;
                  }
               }
            }
            else if (!isForward[jj] && index[jj] >= 0)
            {
               if (!valid)
               {
                  valid = true;
                  indexOfSeg = jj;
                  indexInSeg = index[jj];
                  earliest = segsToMerge[jj].traj->getTimeList()[indexInSeg];
               }
               else
               {
                  OpenFrames::Trajectory::DataType t = segsToMerge[jj].traj->getTimeList()[index[jj]];
                  if (t < earliest)
                  {
                     indexOfSeg = jj;
                     indexInSeg = index[jj];
                     earliest = t;
                  }
               }
            }
         }
         if (valid)
         {
            CopyPointToEndOfTrajectory(segsToMerge[indexOfSeg].traj, indexInSeg, segments[0].traj);
            if (isForward[indexOfSeg])
               index[indexOfSeg]++;
            else
               index[indexOfSeg]--;
         }
         else
            break; // This should never happen due to the while loop case
      }

      // Remove old trajectories from listeners and the segments array
      OFRenderPool::Instance().LockScenesWithRoot(frameName);
      for (int jj = segments.size() - 1; jj >= 1; jj--)
      {
         for (auto l = mListeners.begin(); l < mListeners.end(); l++)
         {
            if ((*l)->IsNameSameAs(bodyName)  && (*l)->IsFrameSameAs(frameName))
               (*l)->RemoveSegment(segments[jj].traj.get());
         }
         segments.erase(segments.begin() + jj);
      }
      OFRenderPool::Instance().UnlockScenesWithRoot(frameName);

      // Inform subscribers of the update
      segments.front().traj->informSubscribers();
   }
}


void TrajectoryDealer::CopyPointToEndOfTrajectory(OpenFrames::Trajectory *source, int index,
                                                  OpenFrames::Trajectory *destination)
{
   OpenFrames::Trajectory::DataType pv[3];
   OpenFrames::Trajectory::DataType att[4];

   destination->addTime(source->getTimeList()[index]);
   bool valid = source->getPosition(index, pv[0], pv[1], pv[2]);
   if (valid)
      destination->addPosition(pv);
   valid = source->getAttitude(index, att[0], att[1], att[2], att[3]);
   if (valid)
      destination->addAttitude(att);
   valid = source->getOptional(index, 0U, pv[0], pv[1], pv[2]);
   if (valid)
      destination->setOptional(0U, pv);
}


bool TrajectoryDealer::IsSegmentShownInAllListeners(const std::string &bodyName, const std::string &frameName,
                                                    const SegmentData &segment)
{
   bool all = true;
   for (auto l = mListeners.begin(); all && l < mListeners.end(); l++)
   {
      if ((*l)->IsNameSameAs(bodyName)  && (*l)->IsFrameSameAs(frameName))
         all = all && (*l)->HasSegment(segment.traj);
   }
   return all;
}


bool TrajectoryDealer::IsTrajectoryEndpointsConnected(const SegmentData &first, const SegmentData &second) const
{
   unsigned int numPos = second.traj->getNumPos();
   if (numPos > 0)
   {
      double pos[3];
      double startTime, endTime;
      bool valid = second.traj->getPosition(0, pos[0], pos[1], pos[2]);
      second.traj->getTimeRange(startTime, endTime);
      return valid && IsTrajectoryEndpointAt(first, startTime, pos);
   }
   else
      // if there are not any points, then they might as well be connected
      return true;
}


bool TrajectoryDealer::IsTrajectoryEndpointAt(const SegmentData &segment, double time, const double *pos) const
{
   unsigned int numPos = segment.traj->getNumPos();
   if (numPos > 0)
   {
      double pos0[3];
      double startTime, endTime;
      segment.traj->getTimeRange(startTime, endTime);
      segment.traj->getPosition(numPos-1, pos0[0], pos0[1], pos0[2]);
      bool timeInRange = endTime == time;
      bool xInRange = pos0[0] == pos[0];
      bool yInRange = pos0[1] == pos[1];
      bool zInRange = pos0[2] == pos[2];
      return ( timeInRange && xInRange && yInRange && zInRange );
   }
   else
      // if there are not any points, then they might as well be connected
      return true;
}


/*
 * Check that a segment is going in the direction indicated
 *
 * For trajectories with a single point or no points the previousFlag is 0.
 *
 * @param[inout] previousFlag A flag indicating the direction of segments previously
 *               checked. Set to 0 for the first segment, which means no direction.
 * @param[in] segment The segment to check
 *
 * @return True if segment is going in the direction indicated by previousFlag
 * @return False if the direction is different
 */
bool TrajectoryDealer::SameDirectionAs(int &previousFlag, const SegmentData &segment)
{
   OpenFrames::Trajectory::DataType start, end;
   bool valid = segment.traj->getTimeRange(start, end);
   OpenFrames::Trajectory::DataType range = end - start;
   int currentFlag = (0.0 < range) - (range < 0.0); // signum
   if (previousFlag == 0)
   {
      previousFlag = currentFlag;
      return true;
   }
   else
   {
      if (currentFlag == previousFlag)
         return true;
      else if (currentFlag == 0)
         return true;
      else
         return false;
   }
}


/*
 * Concatenates two segments
 *
 * The thief and the prey should have the same time direction, which is not
 * verified by this function. If the prey overlaps the theif in time, then
 * all points in the prey before the thief's end time are ignored.
 *
 * @param thief The segment being added two
 * @param prey The segment containing the points to add
 */
void TrajectoryDealer::ConcatenateSegments(SegmentData &thief, SegmentData &prey)
{
   for (auto id = prey.ids.begin(); id < prey.ids.end(); id++)
   {
      if (std::find(thief.ids.begin(), thief.ids.end(), *id) == thief.ids.end())
         thief.ids.push_back(*id);
   }
   // assume there are the same number of time, position, attitude, and options as added above
   const OpenFrames::Trajectory::DataArray &t = prey.traj->getTimeList();
   OpenFrames::Trajectory::DataType posvel[6];
   OpenFrames::Trajectory::DataType att[4];
   OpenFrames::Trajectory::DataType t0, tf;
   thief.traj->getTimeRange(t0, tf);
   bool increasing;
   if (t0 < tf)
      increasing = true;
   else
      increasing = false;
   for (int ii = 0; ii < t.size(); ii++)
   {
      if ((increasing && (t[ii] > tf)) || (!increasing && (t[ii] < tf)))
      {
         thief.traj->addTime(t[ii]);
         bool valid = prey.traj->getPosition(ii, posvel[0], posvel[1], posvel[2]);
         if (valid)
            thief.traj->addPosition(posvel);
         valid = prey.traj->getAttitude(ii, att[0], att[1], att[2], att[3]);
         if (valid)
            thief.traj->addAttitude(att);
         valid = prey.traj->getOptional(0U, ii, posvel[3], posvel[4], posvel[5]);
         if (valid)
            thief.traj->setOptional(0U, &posvel[3]);
      }
   }
   thief.traj->informSubscribers();
}


/**
 * Finalizes an orbit trajectory
 *
 * All of the trajectories for the given body in a specific coordinate frame
 * are finalized here. We make sure that celestial body and ground stations
 * are finalized here by checking merge.
 */
void TrajectoryDealer::FinalizeOrbitTrajectory(const std::string &bodyName, const std::string &frameName)
{
   for (auto s = mSegments.begin(); s != mSegments.end(); s++)
   {
      // if the frame matches
      if (std::get<0>(s->first) == bodyName && std::get<1>(s->first) == frameName)
         s->second.back().finalized = true;
      // Combine segments into a single segment
      if (s->second[0].merge)
      {
         s->second.back().finalized = true;
         CombineAllSegments(std::get<0>(s->first), std::get<1>(s->first), s->second);
      }
   }
}


/**
 * Finalizes a solver trajectory
 *
 * All of the trajectories for the given coordinate frame are finalized
 * here. We need to make sure that celestial body and ground stations
 * are finalized here, but we assume that finalizing all spacecraft (even
 * spacecraft that don't have new segments) is okay.
 */
void TrajectoryDealer::FinalizeSolverTrajectory(const std::string &bodyName, const std::string &frameName)
{
   OFRenderPool::Instance().LockScenesWithRoot(frameName);
   for (auto s = mSolutions.begin(); s != mSolutions.end(); s++)
   {
      // if the frame matches
      if (std::get<1>(s->first) == frameName)
      {
         if (!s->second.back().finalized)
         {
            s->second.back().finalized = true;

            // let listeners clear out old trajectories, and determine how many to keep in the pool
            Integer maxOldTrajectoriesToKeep = 0;
            for (auto l = mListeners.begin(); l < mListeners.end(); l++)
            {
               if ((*l)->IsFrameSameAs(frameName) && (*l)->IsNameSameAs(std::get<0>(s->first)))
               {
                  Integer kept = (*l)->FinalizeSolverTrajectory();
                  if (kept > maxOldTrajectoriesToKeep)
                     maxOldTrajectoriesToKeep = kept;
               }
            }

            // reduce the pool down to the max number of old trajectories required by listeners
            for (Integer ii = s->second.size() - 1 - maxOldTrajectoriesToKeep; ii >= 0; ii--)
               s->second.erase(s->second.begin() + ii);
         }
      }

   }
   OFRenderPool::Instance().UnlockScenesWithRoot(frameName);
}


/**
 * Removes a number of the most recent solver trajectories
 *
 * This clears the way for the user to see the actual, chosen trajectory.
 */
void TrajectoryDealer::RemoveLastSolverTrajectory(const std::string &bodyName, const std::string &frameName,
                                                  Integer numTrajToRemove)
{
   OFRenderPool::Instance().LockScenesWithRoot(frameName);
   for (auto s = mSolutions.begin(); s != mSolutions.end(); s++)
   {
      // if the frame matches
      if (std::get<1>(s->first) == frameName)
      {
         if (mRemovedCount[frameName] < 2)
         {
            // tell listeners to remove last trajectories
            mRemovedCount[frameName] = mRemovedCount[frameName] + 1;
            for (auto l = mListeners.begin(); l < mListeners.end(); l++)
            {
               if ((*l)->IsFrameSameAs(frameName))
               {
                  if ((*l)->IsSpacecraft())
                     (*l)->RemoveLastSolverTrajectory(numTrajToRemove);
                  else
                     (*l)->RemoveAllSolverTrajectories();
               }
            }

            // remove the last couple of trajectories
            for (Integer ii = s->second.size() - 1; ii > s->second.size() - 1 - numTrajToRemove; ii--)
               s->second.erase(s->second.begin() + ii);
         }
      }
   }
   OFRenderPool::Instance().UnlockScenesWithRoot(frameName);
}


void TrajectoryDealer::ClearAllTrajectories()
{
   OFRenderPool::Instance().LockAllScenes();
   for (auto l = mListeners.begin(); l < mListeners.end(); l++)
      (*l)->ResetTrajectories();
   mSegments.clear();
   mSolutions.clear();
   OFRenderPool::Instance().UnlockAllScenes();
}


void TrajectoryDealer::RegisterListener(TrajectoryListener &listener)
{
   bool found = false;
   for (auto l = mListeners.begin(); l < mListeners.end(); l++)
   {
      if ((*l) == &listener)
      {
         found = true;
         break;
      }
   }
   if (!found)
      mListeners.emplace_back(&listener);
}


void TrajectoryDealer::UnregisterListener(TrajectoryListener &listener)
{
   bool remainingObjectsInFrame = false;
   for (int n = mListeners.size() - 1; n >= 0; n--)
   {
      if (mListeners[n] == &listener)
         mListeners.erase(mListeners.begin() + n);
      else
      {
         if (mListeners[n]->IsFrameSameAs(listener) && mListeners[n]->IsNameSameAs(listener))
            remainingObjectsInFrame = true;
      }
   }
   if (!remainingObjectsInFrame)
   {
      SegmentIdentifier key(listener.GetObjectName(), listener.GetFrameName());
      mSegments.erase(key);
      mSolutions.erase(key);
   }
}


std::vector<TrajectoryDealer::SegmentData> &TrajectoryDealer::FindSegmentsFor(const std::string &bodyName,
                                                                              const std::string &frameName)
{
   SegmentIdentifier key(bodyName, frameName);

   // emplace new or find old
   auto ret = mSegments.emplace(key, std::vector<SegmentData>(0));
   auto s = std::get<0>(ret);
   return std::get<1>(*s);
}


std::vector<TrajectoryDealer::SegmentData> &TrajectoryDealer::FindSolutionsFor(const std::string &bodyName,
                                                                               const std::string &frameName)
{
   SegmentIdentifier key(bodyName, frameName);

   // emplace new or find old
   auto ret = mSolutions.emplace(key, std::vector<SegmentData>(0));
   auto s = std::get<0>(ret);
   return std::get<1>(*s);
}
