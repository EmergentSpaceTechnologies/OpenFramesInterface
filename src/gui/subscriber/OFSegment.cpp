//$Id$
//------------------------------------------------------------------------------
//                                  OFSegment
//------------------------------------------------------------------------------
// OpenFramesInterface Plugin for GMAT (General Mission Analysis Tool)
//
// Copyright (c) 2022 Emergent Space Technologies, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Developed by Emergent Space Technologies, Inc. under contract number
// NNX16CG16C
//
// Author: Matthew Ruschmann, Emergent Space Technologies, Inc.
// Created: August 21, 2017
/**
 *  OFSegment class
 *  Conveniently wraps all OpenFrames objects that implement a single Gmat space
 *  object.
 */
//------------------------------------------------------------------------------

#include "OFSegment.hpp"
#include "SpacePointOptions.hpp"

#include "FileManager.hpp"
#include "MessageInterface.hpp"

#include <OpenFrames/CurveArtist.hpp>
#include <OpenFrames/DrawableTrajectory.hpp>
#include <OpenFrames/MarkerArtist.hpp>
#include <OpenFrames/Model.hpp>
#include <OpenFrames/SegmentArtist.hpp>
#include <OpenFrames/TrajectoryFollower.hpp>

#include <algorithm>
#include <math.h>


/**
 * Constructor
 *
 * @param <propName> The name of the propagate command associated with this
 *                   trajectory
 * @param <options> Configuration that defines the appearance of this instance
 * @param <timeScale> The initial time scale for the trajectory follower
 * @param <drawableTrajectory> The drawable to associate artists with
 */
OFSegment::OFSegment(const std::string &propName, const SpacePointOptions &options,
                     OpenFrames::DrawableTrajectory *drawableTrajectory, double radius,
                     OpenFrames::Trajectory *trajectory) :
   mPropagatorName(propName),
   mDrawableTraj(drawableTrajectory),
   mTrajectory(trajectory),
   mOverrideColor(false)
{
   RgbColorFloat orbitColor(options.GetCurrentOrbitColor());

   // Create spacecraft as a 3D model with a body frame
   // Model(name, color[r,g,b,a])
   mFrame = new OpenFrames::ReferenceFrame(propName);
   mFrame->showNameLabel(false);
   mFrame->showAxes(OpenFrames::ReferenceFrame::NO_AXES);
   mFrame->showAxesLabels(OpenFrames::ReferenceFrame::NO_AXES);

   // Children and artists
   mEndMarker = new OpenFrames::MarkerArtist(mTrajectory);
   std::string roseShaderLoc = FileManager::Instance()->FindPath("Marker_Rose.frag", "TEXTURE_PATH", true, true, false);
   if (roseShaderLoc.size() > 0)
      mEndMarker->setMarkerShader(roseShaderLoc);
   else
      MessageInterface::ShowMessage("Cannot find file Marker_Rose.frag in TEXTURE_PATH.\n");
   mEndMarker->setMarkers(OpenFrames::MarkerArtist::START + OpenFrames::MarkerArtist::END);
   mEndMarker->setNodeMask(0x0);
   mDrawableTraj->addArtist(mEndMarker);

   mCurveArtist = new OpenFrames::CurveArtist(mTrajectory);
   mCurveArtist->setNodeMask(0x0);
   mDrawableTraj->addArtist(mCurveArtist); // Add to spacecraft window

   OpenFrames::Trajectory::DataSource data;
   mSegmentArtist = new OpenFrames::SegmentArtist(mTrajectory);
   data._src = OpenFrames::Trajectory::POSOPT;
   data._opt = 0U;         // Use position
   data._element = 0U;     // X
   mSegmentArtist->setStartXData(data);
   data._element = 1U;     // Y
   mSegmentArtist->setStartYData(data);
   data._element = 2U;     // Z
   mSegmentArtist->setStartZData(data);
   data._opt = 1U;         // Use velocity
   data._element = 0U;     // VX
   mSegmentArtist->setEndXData(data);
   data._element = 1U;     // VY
   mSegmentArtist->setEndYData(data);
   data._element = 2U;     // VZ
   mSegmentArtist->setEndZData(data);
   mSegmentArtist->setNodeMask(0x0);
   mDrawableTraj->addArtist(mSegmentArtist); // Add to spacecraft window


   // Set up options
   Configure(options);

   // Tell model to follow trajectory (by default in LOOP mode)
   mTrajectoryFollower = new OpenFrames::TrajectoryFollower(mTrajectory);
   mTrajectoryFollower->setFollowType(OpenFrames::TrajectoryFollower::POSITION | OpenFrames::TrajectoryFollower::ATTITUDE,
                                      OpenFrames::TrajectoryFollower::LIMIT);
   mFrame->getTransform()->setUpdateCallback(mTrajectoryFollower);
}


void OFSegment::Configure(const SpacePointOptions &options)
{
   RgbColorFloat orbitColor(options.GetCurrentOrbitColor());

   mFrame->showNameLabel(options.GetUsePropagateLabel());
   if (!mOverrideColor)
      mFrame->setColor(orbitColor.RedFloat(), orbitColor.GreenFloat(), orbitColor.BlueFloat(), 1.0f);

   // Draw start and end markers
   if (options.GetDrawEnds())
   {
      // MarkerArtist draws the segment start and end markers
      if (!mOverrideColor)
      {
         mEndMarker->setMarkerColor(OpenFrames::MarkerArtist::START, orbitColor.RedFloat(),
                                    orbitColor.GreenFloat(), orbitColor.BlueFloat());
         mEndMarker->setMarkerColor(OpenFrames::MarkerArtist::END, orbitColor.RedFloat(),
                                    orbitColor.GreenFloat(), orbitColor.BlueFloat());
      }
      mEndMarker->setMarkerSize(options.GetDrawMarkerSize());

      // Add to spacecraft window
      mEndMarker->setNodeMask(0xffffffff);
   }
   else
   {
      mEndMarker->setNodeMask(0x0);
   }

   // Curve artist draws the trajectory
   if (options.GetDrawTrajectory())
   {
      mCurveArtist->setWidth(options.GetDrawLineWidth()); // Line width for the trajectory
      if (!mOverrideColor)
         mCurveArtist->setColor(orbitColor.RedFloat(), orbitColor.GreenFloat(), orbitColor.BlueFloat());
      mCurveArtist->setNodeMask(0xffffffff);
   }
   else
   {
      mCurveArtist->setNodeMask(0x0);
   }

   // Segment artist draws velocity vectors
   if (options.GetDrawVelocity())
   {
      if (!mOverrideColor)
         mSegmentArtist->setColor(orbitColor.RedFloat(), orbitColor.GreenFloat(), orbitColor.BlueFloat());
      mSegmentArtist->setNodeMask(0xffffffff);
   }
   else
   {
      mSegmentArtist->setNodeMask(0x0);
   }
}


/**
 * Destructor
 */
OFSegment::~OFSegment()
{
   if (mEndMarker.valid())
      mDrawableTraj->removeArtist(mEndMarker);
   if (mCurveArtist.valid())
      mDrawableTraj->removeArtist(mCurveArtist);
   if (mSegmentArtist.valid())
      mDrawableTraj->removeArtist(mSegmentArtist);
   // Assumes that OpenFrames objects are deleted by osg::ref_ptr
}


/**
 * Changes the color of this orbit segment
 *
 * @param color The new color
 */
void OFSegment::SetOrbitColor(RgbColorFloat &color, bool overrideColor)
{
   mOverrideColor = overrideColor;
   if (mCurveArtist != nullptr)
      mCurveArtist->setColor(color.RedFloat(), color.GreenFloat(), color.BlueFloat());
   if (mEndMarker != nullptr)
      mEndMarker->setMarkerColor(OpenFrames::MarkerArtist::START, color.RedFloat(),
                                 color.GreenFloat(), color.BlueFloat());
}
