//$Id$
//------------------------------------------------------------------------------
//                                  OFSpaceObject
//------------------------------------------------------------------------------
// OpenFramesInterface Plugin for GMAT (General Mission Analysis Tool)
//
// Copyright (c) 2022 Emergent Space Technologies, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Developed by Emergent Space Technologies, Inc. under contract number
// NNX16CG16C
//
// Author: Matthew Ruschmann, Emergent Space Technologies, Inc.
// Created: August 21, 2017
/**
 *  OFSpaceObject class
 *  Conveniently wraps all OpenFrames objects that implement a single Gmat space
 *  object.
 */
//------------------------------------------------------------------------------

#include "OFSpaceObject.hpp"
#include "OFSegment.hpp"
#include "RgbColorFloat.hpp"
#include "TrajectoryDealer.hpp"

#include "Spacecraft.hpp"
#include "CelestialBody.hpp"
#include "FileManager.hpp"
#include "AttitudeConversionUtility.hpp"
#include "MessageInterface.hpp"

#include <OpenFrames/CurveArtist.hpp>
#include <OpenFrames/DrawableTrajectory.hpp>
#include <OpenFrames/MarkerArtist.hpp>
#include <OpenFrames/Model.hpp>
#include <OpenFrames/Sphere.hpp>
#include <OpenFrames/LatLonGrid.hpp>
#include <OpenFrames/RadialPlane.hpp>
#include <OpenFrames/TrajectoryFollower.hpp>
#include <osgDB/FileNameUtils>
#include <osgDB/FileUtils>

#include <algorithm>
#include <math.h>


/**
 * Constructor
 *
 * @param options Configuration that defines the appearance of this instance
 * @param timeScale The initial time scale for the trajectory follower
 */
OFSpaceObject::OFSpaceObject(const SpacePointOptions &options, const std::string &frameName, bool isSpacecraft,
                             Integer numberOfSolverTrajectories) :
   mOptions(options),
   mSetNewColor(false),
   mNewColorOverride(false),
   mRadius(1.0),
   mIsSpacecraft(isSpacecraft),
   mNumberOfSolverTrajectories(numberOfSolverTrajectories)
{
   SetFrameName(frameName);
   SetObjectName(options.GetName());

   // Drawable Trajectory
   mDrawTraj = new OpenFrames::DrawableTrajectory(mOptions.GetName() + " trajectory");
   mFollowView = new OpenFrames::TrajectoryFollower(nullptr);
   mFollowView->setFollowType(OpenFrames::TrajectoryFollower::POSITION | OpenFrames::TrajectoryFollower::ATTITUDE,
                              OpenFrames::TrajectoryFollower::LIMIT);

   // Children
   mXYPlane = new OpenFrames::RadialPlane("xy_plane", 0.0, 0.0, 0.5, 1.0);
   mXYPlane->showContents(false);
   mXYPlane->showNameLabel(false);
   mXYPlane->showAxes(OpenFrames::ReferenceFrame::NO_AXES);
   mXYPlane->showAxesLabels(OpenFrames::ReferenceFrame::NO_AXES);
   mLatLonGrid = new OpenFrames::LatLonGrid(options.GetName() + " grid", 0.0, 0.0, 0.0, mRadius);
   mLatLonGrid->showContents(false);
   mLatLonGrid->showNameLabel(false);
   mLatLonGrid->showAxes(OpenFrames::ReferenceFrame::NO_AXES);
   mLatLonGrid->showAxesLabels(OpenFrames::ReferenceFrame::NO_AXES);

   mDrawCenter = new OpenFrames::DrawableTrajectory(options.GetName() + " center marker");
   mDrawCenter->showContents(false);
   mDrawCenter->showAxes(OpenFrames::ReferenceFrame::NO_AXES);
   mDrawCenter->showAxesLabels(OpenFrames::ReferenceFrame::NO_AXES);
   mDrawCenter->showNameLabel(false);
   mCenterMarker = new OpenFrames::MarkerArtist();
   mCenterMarker->setNodeMask(0x0);
   std::string circleShaderLoc = FileManager::Instance()->FindPath("Marker_Circle.frag", "TEXTURE_PATH", true, true, false);
   if (circleShaderLoc.size() > 0)
      mCenterMarker->setMarkerShader(circleShaderLoc);
   else
      MessageInterface::ShowMessage("Cannot find file Marker_Circle.frag in TEXTURE_PATH.\n");
   mDrawCenter->addArtist(mCenterMarker);

   Configure();
}


/**
 * Destructor
 */
OFSpaceObject::~OFSpaceObject()
{
   for (int jj = mPrimaryParents.size() - 1; jj >= 0; jj--)
   {
      for (int ii = 0; ii < mSegments.size(); ii++)
      {
         mPrimaryParents[jj]->removeChild(&(mSegments[ii]->Frame()));
      }
      mPrimaryParents[jj]->removeChild(mDrawTraj);
      mPrimaryParents[jj]->removeChild(mViewRefFrame);
      mPrimaryParents.pop_back();
   }
   // Assumes that OpenFrames objects are deleted by osg::ref_ptr
}


void OFSpaceObject::ResetState()
{
   mViewRefFrame->setPosition(0.0, 0.0, 0.0);
   mViewRefFrame->setAttitude(0.0, 0.0, 0.0, 1.0);
}


void OFSpaceObject::ResetTrajectories()
{
   // clear drawables
   mDrawTraj->removeAllArtists();
   mSolverTrajs.clear();
   mSolverArtists.clear();

   // clear segments
   for (auto parent = mPrimaryParents.begin(); parent < mPrimaryParents.end(); parent++)
   {
      for (int ii = 0; ii < mSegments.size(); ii++)
      {
         (*parent)->removeChild(&(mSegments[ii]->Frame()));
         mFollowView->removeTrajectory(&(mSegments[ii]->Trajectory()));
      }
   }
   mSegments.clear();
   if (mViewRefFrame.valid() && IsSpacecraft())
      ResetState();
}


bool OFSpaceObject::AttemptReconfigure(const SpacePointOptions &options, const std::string &frameName, bool isSpacecraft,
                                       Integer numberOfSolverTrajectories)
{
   bool reconfigured = false;
   mIsSpacecraft = isSpacecraft;
   mNumberOfSolverTrajectories = numberOfSolverTrajectories;

   if (IsNameSameAs(options.GetName()))
   {
      SetFrameName(frameName);
      mOptions = options;
      reconfigured = true;

      // remove from parents
      for (auto parent = mPrimaryParents.begin(); parent < mPrimaryParents.end(); parent++)
      {
         for (int ii = 0; ii < mSegments.size(); ii++)
         {
            (*parent)->removeChild(&(mSegments[ii]->Frame()));
         }
         (*parent)->removeChild(mDrawTraj);
         (*parent)->removeChild(mViewRefFrame);
      }
      mPrimaryParents.clear();

      Configure();
   }

   return reconfigured;
}


void OFSpaceObject::Configure()
{
   // Load model parameters
   RgbColorFloat orbitColor(mOptions.GetCurrentOrbitColor());
   RgbColorFloat targetColor(mOptions.GetCurrentTargetColor());
   const std::string &name = mOptions.GetName();
   std::string textureFullPath, textureNightFullPath;
   std::string modelFullPath = mOptions.GetModelFile();
   Real modelScale = 1.0;
   Real modelOffset[3] = {0.0, 0.0, 0.0};
   Rvector3 modelRotation;
   Real radius_equatorial = 100.0, radius_polar = 100.0;

   // Grab parameters specific to a spacecraft
   const Spacecraft *sc = dynamic_cast<const Spacecraft *>(mOptions.GetSpacePointConst());
   if (sc != nullptr)
   {
      if (modelFullPath.size() == 0)
      {
         modelFullPath = sc->GetStringParameter(sc->GetParameterID("ModelFileFullPath"));
         modelScale = sc->GetRealParameter(sc->GetParameterID("ModelScale"));
         modelOffset[0] = sc->GetRealParameter(sc->GetParameterID("ModelOffsetX"));
         modelOffset[1] = sc->GetRealParameter(sc->GetParameterID("ModelOffsetY"));
         modelOffset[2] = sc->GetRealParameter(sc->GetParameterID("ModelOffsetZ"));
         modelRotation[0] = sc->GetRealParameter(sc->GetParameterID("ModelRotationX")) * M_PI/180.0;
         modelRotation[1] = sc->GetRealParameter(sc->GetParameterID("ModelRotationY")) * M_PI/180.0;
         modelRotation[2] = sc->GetRealParameter(sc->GetParameterID("ModelRotationZ")) * M_PI/180.0;
      }
   }

   // Grab parameters specific to a celestial body
   CelestialBody *cb = dynamic_cast<CelestialBody *>(mOptions.GetSpacePoint());
   if (cb != nullptr)
   {
      textureFullPath = cb->GetStringParameter(cb->GetParameterID("TextureMapFullPath"));
      radius_equatorial = cb->GetEquatorialRadius();
      radius_polar = cb->GetPolarRadius();
      if (modelFullPath.size() == 0)
      {
         modelFullPath = cb->GetStringParameter(cb->GetParameterID("3DModelFileFullPath"));
         modelScale = cb->GetRealParameter(cb->GetParameterID("3DModelScale"));
         modelOffset[0] = cb->GetRealParameter(cb->GetParameterID("3DModelOffsetX"));
         modelOffset[1] = cb->GetRealParameter(cb->GetParameterID("3DModelOffsetY"));
         modelOffset[2] = cb->GetRealParameter(cb->GetParameterID("3DModelOffsetZ"));
         modelRotation[0] = cb->GetRealParameter(cb->GetParameterID("3DModelRotationX")) * M_PI/180.0;
         modelRotation[1] = cb->GetRealParameter(cb->GetParameterID("3DModelRotationY")) * M_PI/180.0;
         modelRotation[2] = cb->GetRealParameter(cb->GetParameterID("3DModelRotationZ")) * M_PI/180.0;
      }

      // Night texture has same name as day texture, but with "_night" suffix
      if(osgDB::fileType(textureFullPath) == osgDB::REGULAR_FILE)
      {
         // Get path and filename components
         std::string fname = osgDB::getNameLessAllExtensions(textureFullPath);

         // Find all files in same directory with suffix "_night" and any file type
         std::string nightfname_wild = fname + "_night.*";
         osgDB::DirectoryContents results = osgDB::expandWildcardsInFilename(nightfname_wild);
         if(!results.empty()) textureNightFullPath = results[0]; // Pick first file
      }

      // Solver trajs should be the normal color for celestial bodies
      mOptions.SetCurrentTargetColor(mOptions.GetCurrentOrbitColor());
   }

   // Grab parameters specific to a ground station
   if (mOptions.GetSpacePoint() != nullptr && mOptions.GetSpacePoint()->GetTypeName() == "GroundStation")
   {
      // Solver trajs should be the normal color for ground stations
      mOptions.SetCurrentTargetColor(mOptions.GetCurrentOrbitColor());
   }

   // Determine what type of object to use as the base ReferenceFrame
   BodyReferenceFrameType refFrameType = OF_REFERENCE_FRAME;
   if (mOptions.GetDrawObject())
   {
      if (modelFullPath.size() > 0)
         refFrameType = OF_MODEL;
      else if (textureFullPath.size() > 0)
         refFrameType = OF_SPHERE;
   }

   // Set the body frame as either Sphere, Model, or ReferenceFrame
   OpenFrames::Sphere *sphereCheck = dynamic_cast<OpenFrames::Sphere *>(mViewRefFrame.get());
   OpenFrames::Model *modelCheck = dynamic_cast<OpenFrames::Model *>(mViewRefFrame.get());
   switch (refFrameType)
   {
   case OF_SPHERE:
   {
      OpenFrames::Sphere *sphere;
      if (sphereCheck == nullptr)
      {
         sphere = new OpenFrames::Sphere(name);
         // Remove old from parents/children and put new on parents/children
         ChangeChildFrameOnParents(sphere);
         mViewRefFrame = sphere;
         mViewRefFrame->addChild(mXYPlane);
         mViewRefFrame->addChild(mLatLonGrid);
         mViewRefFrame->addChild(mDrawCenter);
         mModelFilename.clear();
      }
      else
      {
         sphere = sphereCheck;
      }

      sphere->setRadius(radius_equatorial);
      if (radius_polar != radius_equatorial)
         sphere->setSphereScale(1.0, 1.0, radius_polar / radius_equatorial);
      // GMAT's texture maps are 180 degrees out of aligment with OpenFrames' interpretration
      sphere->setSphereAttitude(osg::Quat(0.0, 0.0, 1.0, 0.0));

      // Load day texture
      bool success = sphere->setTextureMap(textureFullPath);
      if (!success)
      {
         MessageInterface::ShowMessage("OpenFrames cannot find or cannot load the texture %s.\n", textureFullPath.c_str());
      }

      // Load night texture
      if(!textureNightFullPath.empty())
      {
         success = sphere->setNightTextureMap(textureNightFullPath);
         if (!success)
         {
            MessageInterface::ShowMessage("OpenFrames cannot find or cannot load the night texture %s.\n", textureNightFullPath.c_str());
            textureNightFullPath.clear();
         }
      }
      mRadius = radius_equatorial;
      break;
   }
   case OF_MODEL:
   {
      OpenFrames::Model *model;
      if (modelCheck == nullptr)
      {
         model = new OpenFrames::Model(name);
         // Remove old from parents/children and put new on parents/children
         ChangeChildFrameOnParents(model);
         mViewRefFrame = model;
         mViewRefFrame->addChild(mXYPlane);
         mViewRefFrame->addChild(mLatLonGrid);
         mViewRefFrame->addChild(mDrawCenter);
      }
      else
      {
         model = modelCheck;
      }

      // Lazy-load the model (i.e. only reload if the full filepath has changed)
      if (model->setModel(modelFullPath, false))
      {
         mModelFilename = modelFullPath;
         model->setModelScale(modelScale, modelScale, modelScale);
         Rvector quat0 = AttitudeConversionUtility::ToQuaternion(modelRotation, 1U, 2U, 3U);
         osg::Quat quat(quat0[0], quat0[1], quat0[2], quat0[3]);
         model->setModelPosition(modelOffset[0], modelOffset[1], modelOffset[2]);
         model->setModelAttitude(quat);
         mRadius = model->getBound().radius();
      }
      else
      {
         MessageInterface::ShowMessage("OpenFrames cannot find or cannot load the model %s.\n", modelFullPath.c_str());
         mModelFilename.clear();
         mRadius = radius_equatorial;
      }
      
      break;
   }
   case OF_REFERENCE_FRAME:
   default:
   {
      bool create = ( !mViewRefFrame.valid() || sphereCheck != nullptr || modelCheck != nullptr );
      if (create)
      {
         OpenFrames::ReferenceFrame *standardRefFrame = new OpenFrames::ReferenceFrame(name);
         // Remove old from parents/children and put new on parents/children
         ChangeChildFrameOnParents(standardRefFrame);
         mViewRefFrame = standardRefFrame;
         mViewRefFrame->addChild(mXYPlane);
         mViewRefFrame->addChild(mLatLonGrid);
         mViewRefFrame->addChild(mDrawCenter);
         mModelFilename.clear();
      }
      mRadius = radius_equatorial;
      break;
   }
   }

   // Configure base axes options
   mViewRefFrame->getTransform()->setUpdateCallback(mFollowView);
   mViewRefFrame->showNameLabel(mOptions.GetDrawLabel());
   mViewRefFrame->setColor(orbitColor.RedFloat(), orbitColor.GreenFloat(), orbitColor.BlueFloat(), 1.0f);
   if (mOptions.GetDrawAxes())
   {
      double offset = mRadius;
      mViewRefFrame->showAxes(OpenFrames::ReferenceFrame::X_AXIS | OpenFrames::ReferenceFrame::Y_AXIS
                                                                 | OpenFrames::ReferenceFrame::Z_AXIS);
      mViewRefFrame->showAxesLabels(OpenFrames::ReferenceFrame::X_AXIS | OpenFrames::ReferenceFrame::Y_AXIS
                                                                       | OpenFrames::ReferenceFrame::Z_AXIS);
      osg::Vec3d axis;
      axis[0] = offset;
      mViewRefFrame->moveXAxis(axis, offset);
      axis[0] = 0.0;
      axis[1] = offset;
      mViewRefFrame->moveYAxis(axis, offset);
      axis[1] = 0.0;
      axis[2] = offset;
      mViewRefFrame->moveZAxis(axis, offset);
   }
   else
   {
      mViewRefFrame->showAxes(OpenFrames::ReferenceFrame::NO_AXES);
      mViewRefFrame->showAxesLabels(OpenFrames::ReferenceFrame::NO_AXES);
   }

   // Configure drawable traj
   mDrawTraj->setColor(orbitColor.RedFloat(), orbitColor.GreenFloat(), orbitColor.BlueFloat(), 0.9f);
   mDrawTraj->showAxes(OpenFrames::ReferenceFrame::NO_AXES);
   mDrawTraj->showAxesLabels(OpenFrames::ReferenceFrame::NO_AXES);
   mDrawTraj->showNameLabel(false);

   // XY plane in the relative frame
   if (mOptions.GetDrawXYPlane())
   {
      mXYPlane->setLineColor(osg::Vec4(orbitColor.RedFloat(), orbitColor.GreenFloat(), orbitColor.BlueFloat(), 1.0));
      mXYPlane->setPlaneColor(osg::Vec4(orbitColor.RedFloat(), orbitColor.GreenFloat(), orbitColor.BlueFloat(), 0.2));
      mXYPlane->setParameters(15*mRadius, mRadius, M_PI / 6.0);
      mXYPlane->showContents(true);
   }
   else
   {
      mXYPlane->showContents(false);
   }

   // Configure latitude/longitude grid
   if (mOptions.GetDrawGrid())
   {
      mLatLonGrid->setColor(osg::Vec4(0.0, 0.0, 0.0, 0.9));
      mLatLonGrid->setParameters(mRadius, mRadius, mRadius, M_PI/8.0, M_PI/8.0);
      mLatLonGrid->showContents(true);
   }
   else
   {
      mLatLonGrid->showContents(false);
   }

   // Draw the center point
   if (mOptions.GetDrawCenter())
   {
      // Drawable trajectory holds the spacecraft center marker
      mDrawCenter->setColor(orbitColor.RedFloat(), orbitColor.GreenFloat(), orbitColor.BlueFloat(), 0.9f);

      // MarkerArtist draws the spacecraft center marker
      mCenterMarker->setMarkerSize(mOptions.GetDrawMarkerSize());
      mCenterMarker->setMarkerColor(OpenFrames::MarkerArtist::START, orbitColor.RedFloat(),
                                    orbitColor.GreenFloat(), orbitColor.BlueFloat());

      // Add to spacecraft window
      mDrawCenter->showContents(true);
      mCenterMarker->setNodeMask(0xffffffff);
   }
   else {
      mCenterMarker->setNodeMask(0x0);
      mDrawCenter->showContents(false);
   }

   // Pass the configuration on to each trajectory segment
   for (auto model = mSegments.begin(); model < mSegments.end(); model++)
   {
      (*model)->Configure(mOptions);
   }

   // Set options for any solver trajectories
   for (auto artist = mSolverArtists.begin(); artist < mSolverArtists.end(); artist++)
   {
      (*artist)->setColor(targetColor.RedFloat(), targetColor.GreenFloat(), targetColor.BlueFloat());
      (*artist)->setWidth(mOptions.GetDrawLineWidth());
   }

   // Set up Sun as a light source
   if(IsNameSameAs("Sun"))
   {
      // Create light at center of Sun
      mViewRefFrame->setLightSourceEnabled(true);
      osg::Light* sunLight = mViewRefFrame->getLightSource()->getLight();
      sunLight->setPosition(osg::Vec4(0.0, 0.0, 0.0, 1.0));
      sunLight->setAmbient(osg::Vec4(0.4, 0.4, 0.4, 1.0));
      sunLight->setDiffuse(osg::Vec4(2.0, 2.0, 2.0, 1.0));
      sunLight->setSpecular(osg::Vec4(0.8, 0.8, 0.8, 1.0));
   }

   // Set material properties for celestial bodies
   // TODO Find appropriate material properties for celestial bodies
   OpenFrames::Sphere* sphere = dynamic_cast<OpenFrames::Sphere*>(mViewRefFrame.get());
   if(sphere)
   {
      osg::Material* mat = new osg::Material;

      // Sun: full emission, no reflection
      if(IsNameSameAs("Sun"))
      {
         mat->setAmbient(osg::Material::FRONT_AND_BACK, osg::Vec4(0, 0, 0, 1));
         mat->setDiffuse(osg::Material::FRONT_AND_BACK, osg::Vec4(0, 0, 0, 1));
         mat->setSpecular(osg::Material::FRONT_AND_BACK, osg::Vec4(0, 0, 0, 1));
         mat->setEmission(osg::Material::FRONT_AND_BACK, osg::Vec4(1, 1, 1, 1));
      }

      // Earth: depends on whether night texture is defined
      else if(IsNameSameAs("Earth"))
      {
         // No night texture: low-level ambient reflection
         if(textureNightFullPath.empty())
         {
            mat->setAmbient(osg::Material::FRONT_AND_BACK, osg::Vec4(0.5, 0.5, 0.5, 1));
            mat->setDiffuse(osg::Material::FRONT_AND_BACK, osg::Vec4(1, 1, 1, 1));
         }

         // Night texture: no ambient reflection so night texture shows through
         else
         {
            mat->setAmbient(osg::Material::FRONT_AND_BACK, osg::Vec4(0, 0, 0, 1));
            mat->setDiffuse(osg::Material::FRONT_AND_BACK, osg::Vec4(1, 1, 1, 1));
         }

         // Earth has specular reflection but no emission
         mat->setSpecular(osg::Material::FRONT_AND_BACK, osg::Vec4(0.4, 0.4, 0.4, 1));
         mat->setShininess(osg::Material::FRONT_AND_BACK, 100.0); // Range [0, 128]
         mat->setEmission(osg::Material::FRONT_AND_BACK, osg::Vec4(0, 0, 0, 1));
      }

      // Otherwise only ambient and diffuse reflections
      else
      {
         mat->setAmbient(osg::Material::FRONT_AND_BACK, osg::Vec4(0.5, 0.5, 0.5, 1));
         mat->setDiffuse(osg::Material::FRONT_AND_BACK, osg::Vec4(1, 1, 1, 1));
         mat->setSpecular(osg::Material::FRONT_AND_BACK, osg::Vec4(0, 0, 0, 1));
         mat->setEmission(osg::Material::FRONT_AND_BACK, osg::Vec4(0, 0, 0, 1));
      }

      sphere->setMaterial(mat);
   }
}


void OFSpaceObject::ChangeChildFrameOnParents(OpenFrames::ReferenceFrame *frame)
{
   if (mViewRefFrame.valid())
   {
      for (int kk = mViewRefFrame->getNumChildren() - 1; kk >= 0; kk--)
      {
         OpenFrames::ReferenceFrame *child = mViewRefFrame->getChild(kk);
         mViewRefFrame->removeChild(child);
         frame->addChild(child);
      }
      for (auto p = mPrimaryParents.begin(); p < mPrimaryParents.end(); p++)
      {
         (*p)->removeChild(mViewRefFrame);
         (*p)->addChild(frame);
      }
   }
}


/**
 * Adds the primary object for this spacecraft to a reference frame
 *
 * @param parent The reference frame to assign this instance to as a child
 */
void OFSpaceObject::AddPrimaryToParentFrame(OpenFrames::ReferenceFrame *parent)
{
   parent->addChild(mDrawTraj);
   parent->addChild(mViewRefFrame);
   for (auto sc = mSegments.begin(); sc < mSegments.end(); sc++)
   {
      parent->addChild(&(*sc)->Frame());
   }
   mPrimaryParents.push_back(parent);
}


/**
 * Gets a primary reference frame that represents the center of this object
 * across all trajectories
 *
 * @return A pointer to the frame
 */
OpenFrames::ReferenceFrame *OFSpaceObject::FrameOnWholeTrajectory() const
{
   return mViewRefFrame;
}


/**
 * Gets a primary drawable trajectory that represents the center of this object
 * across all trajectories
 *
 * @return A pointer to the frame
 */
OpenFrames::ReferenceFrame *OFSpaceObject::WholeTrajectory() const
{
   return mDrawTraj;
}


/**
 * Gets a secondary reference frame that represents the center of this object
 * for a single trajectory
 *
 * For now, it selects the first trajectory with the given propName. Eventually,
 * this function may return multiple frames so that the user can switch around
 * the different views.
 *
 * @param propName The name of the propagator that generated the trajectory
 *
 * @return A pointer to the frame
 */
OpenFrames::ReferenceFrame *OFSpaceObject::TrajectorySegment(const std::string &propName) const
{
   OpenFrames::ReferenceFrame *frame = nullptr;
   for (auto model = mSegments.begin(); model < mSegments.end(); model++)
   {
      if ((*model)->GetPropagatorName() == propName)
      {
         frame = &(*model)->Frame();
         break;
      }
   }
   return frame;
}


/**
 * Subscriber always gets the color before the point that belongs to that color.
 */
void OFSpaceObject::UpdateColor()
{
   if (mSetNewColor)
   {
      mSetNewColor = false;
      if (mSegments.size() > 0)
         mSegments.back()->SetOrbitColor(mNewColor, mNewColorOverride);
   }
}


/**
 * Adds a new segment to this object
 *
 * @param propName The name of the Propagate command creating this trajectory
 * @param trajectory The trajectory containing segment data
 */
void OFSpaceObject::AddSegment(const std::string &propName, OpenFrames::Trajectory *trajectory)
{
   mSegments.push_back(new OFSegment(propName, mOptions, mDrawTraj, mRadius, trajectory));
   for (auto p = mPrimaryParents.begin(); p < mPrimaryParents.end(); p++)
      (*p)->addChild(&mSegments.back()->Frame());
   if (mSegments.size() == 1)
      mFollowView->setTrajectory(&mSegments.back()->Trajectory());
   else
      mFollowView->addTrajectory(&mSegments.back()->Trajectory());
}


/**
 * Remove a new segment from this object
 *
 * @param trajectory The trajectory representing the segment to remove
 */
void OFSpaceObject::RemoveSegment(OpenFrames::Trajectory *trajectory)
{
   for (int ii = mSegments.size() - 1; ii >= 0; ii--)
   {
      if (&mSegments[ii]->Trajectory() == trajectory)
      {
         for (auto p = mPrimaryParents.begin(); p < mPrimaryParents.end(); p++)
            (*p)->removeChild(&mSegments[ii]->Frame());
         mFollowView->removeTrajectory(&mSegments.back()->Trajectory());
         mSegments.erase(mSegments.begin() + ii);
      }
   }
}


bool OFSpaceObject::HasSegment(OpenFrames::Trajectory *trajectory)
{
   bool found = false;
   for (auto seg = mSegments.begin(); seg < mSegments.end(); seg++)
   {
      if (&(*seg)->Trajectory() == trajectory)
      {
         found = true;
         break;
      }
   }
   return found;
}


/**
 * Adds a new solution segment to this object
 *
 * @param trajectory The trajectory containing segment data
 */
void OFSpaceObject::AddSolution(OpenFrames::Trajectory *trajectory)
{
   RgbColorFloat targetColor(mOptions.GetCurrentTargetColor());
   mSolverTrajs.push_back(trajectory);
   mSolverArtists.push_back(new OpenFrames::CurveArtist(mSolverTrajs.back()));
   mSolverArtists.back()->setWidth(mOptions.GetDrawLineWidth()); // Line width for the trajectory
   mSolverArtists.back()->setColor(targetColor.RedFloat(), targetColor.GreenFloat(), targetColor.BlueFloat());
}


/**
 * Sets the color of the current orbit trajectory
 *
 * @param color The color
 */
void OFSpaceObject::SetSegmentColor(RgbColorFloat &color)
{
   mSetNewColor = true;
   mNewColor = color;
   mNewColorOverride = true;
}


/**
 * Sets the color of the current orbit trajectory to the default value
 */
void OFSpaceObject::SetDefaultSegmentColor()
{
   RgbColorFloat defaultColor(mOptions.GetCurrentOrbitColor());
   mSetNewColor = true;
   mNewColor = defaultColor;
   mNewColorOverride = false;
}


/**
 * Sets the default color of the orbit trajectories
 *
 * @param color The color
 */
void OFSpaceObject::SetOrbitColor(RgbColorFloat &color)
{
   mOptions.SetCurrentOrbitColor(color.GetIntColor());
}


/**
 * Sets the color of the solver trajectory
 *
 * @param color The color
 */
void OFSpaceObject::SetTargetColor(RgbColorFloat &color)
{
   mOptions.SetCurrentTargetColor(color.GetIntColor());
}


/**
 * Finalizes a solver trajectories and displays it
 *
 * @param removeOldTrajectories If true, then all old solver trajectories are
 *                              removed
 *
 * @return The number of solver trajectories kept
 *
 * First, the latest solver trajectory is displayed. Second, any old solver
 * trajectories that need to be removed are removed.
 *
 * This function displays the new trajectory before removing older trajectories
 * because this order makes the visualization a little smoother.
 */
Integer OFSpaceObject::FinalizeSolverTrajectory()
{
   if (mSolverTrajs.size() > 0)
   {
      if (mSolverTrajs.back()->getNumPos() > 0)
      {
         // Add the trajectory artist to the drawable
         mDrawTraj->addArtist(mSolverArtists.back());
         // Follow the latest solver trajectory if no regular segments
         if (mSegments.size() == 0)
         {
            mFollowView->addTrajectory(mSolverTrajs.back());
            if (mSolverTrajs.size() > 1)
               mFollowView->removeTrajectory(mSolverTrajs.at(mSolverTrajs.size()-1));
         }
         // Remove old trajectories
         if (mNumberOfSolverTrajectories > 0)
         {
            for (Integer ii = mSolverTrajs.size() - 1 - mNumberOfSolverTrajectories; ii >= 0; ii--)
            {
               mDrawTraj->removeArtist(mSolverArtists.front());
               mSolverArtists.erase(mSolverArtists.begin());
               mSolverTrajs.erase(mSolverTrajs.begin());
            }
         }
      }
      // else Trajectory is empty, no need to display it
   }

   return mSolverTrajs.size();
}


/**
 * Removes just the last solver trajectory
 *
 * This is actually the next to the last trajectory because there is always an
 * empty (or in progress) trajectory as the "real" final trajectory. Of course,
 * that last trajectory should be empty when this function is called, but that
 * is not checked because its irrelevant to this function.
 */
void OFSpaceObject::RemoveLastSolverTrajectory(Integer numTrajToRemove)
{
   // Always unfollow last trajectory before discarding.
   if (mSolverTrajs.size() > 0)
      mFollowView->removeTrajectory(mSolverTrajs.back());

   // Discard the last N trajectories:
   for (Integer ii = 0; ii < numTrajToRemove; ii++)
   {
      if (mSolverTrajs.size() > 0)
      {
         int last_i = mSolverTrajs.size() - 1;
         mDrawTraj->removeArtist(mSolverArtists[last_i]);
         mSolverArtists.erase(mSolverArtists.begin() + last_i);
         mSolverTrajs.erase(mSolverTrajs.begin() + last_i);
      }
      // else Nothing to remove
   }
}


/**
 * Removes all last solver trajectories
 *
 * This actually removes all but one trajectory because the last trajectory
 * is empty, unassigned, and waiting to be used.
 */
void OFSpaceObject::RemoveAllSolverTrajectories()
{
   // Always unfollow last trajectory before discarding. Do not check
   // mSegments.size() because trajectories may have been added.
   if (mSolverTrajs.size() > 0)
      mFollowView->removeTrajectory(mSolverTrajs.back());

   // Discard all but the last trajectory
   for (Integer ii = mSolverTrajs.size() - 1; ii >= 0; ii--)
   {
      mDrawTraj->removeArtist(mSolverArtists[ii]);
      mSolverArtists.erase(mSolverArtists.begin() + ii);
      mSolverTrajs.erase(mSolverTrajs.begin() + ii);
   }
}
