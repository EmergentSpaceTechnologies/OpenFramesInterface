//$Id$
//------------------------------------------------------------------------------
//                                  OFScene
//------------------------------------------------------------------------------
// OpenFramesInterface Plugin for GMAT (General Mission Analysis Tool)
//
// Copyright (c) 2022 Emergent Space Technologies, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Developed by Emergent Space Technologies, Inc. under contract number
// NNX16CG16C
//
// Author: Matthew Ruschmann, Emergent Space Technologies, Inc.
// Created: December 8, 2017
/**
 *  @class OFScene
 *  Implements OpenFrames objects and interacts with a OFCanvas.
 */
//------------------------------------------------------------------------------

#ifndef OFSCENE_hpp
#define OFSCENE_hpp

#include <OpenFrames/WindowProxy.hpp> // before windows.h define min and max macros

#include "OpenFramesInterface_defs.hpp"
#include "StarOptions.hpp"
#include "OpenGLOptions.hpp"

#include <OpenFrames/CoordinateAxes.hpp>
#include <OpenFrames/PolyhedralCone.hpp>
#include <OpenFrames/RadialPlane.hpp>
#include <OpenFrames/View.hpp>
#include <OpenFrames/FrameManager.hpp>
#include <OpenFrames/OpenVRDevice.hpp>

#include <osg/ref_ptr>

#include <QKeyEvent>

class OFWindowProxyPanel;
class OpenFramesView;
class OpenFramesVector;
class OpenFramesSensorMask;
class OpenFramesInterface;
class OFGLCanvas;
class CoordinateOptions;
class SpacePointOptions;
class OFStarListener;
class OFEclipticPlane;
class OFSpaceObject;
class CoordinateSystem; // From GMAT base

class OpenFramesInterface_API OFScene
{
public:
   /// The radius of Earth, shorthand copied from GMAT
   static const Real EARTH_RADIUS;

   OFScene(OFWindowProxyPanel* parent);
   virtual ~OFScene();

   OFGLCanvas &GetCanvas() { return *mCanvas; }
   OpenFrames::WindowProxy *GetWinProxy() { return mWinProxy; }
   bool IsRootCoordinateFrameNamed(const std::string &name) { return ( mRootCoordinateFrameName == name ); }

   unsigned int GetWinProxyID();
   void SetWinProxyID(unsigned int id);
   bool MakeCurrent();
   void SwapBuffers();
   bool PreProcessKeyPress(QKeyEvent &event);
   void KeyPressCallback(unsigned int row, unsigned int col, int key);
   void VREventCallback(unsigned int row, unsigned int col, const OpenFrames::OpenVREvent *vrEvent);

   void LockFrameManager();
   void UnlockFrameManager();
   void AddTimeSyncChild(OpenFrames::WindowProxy *child);
   void RemoveTimeSynchronization();
   const std::string &GetSyncParentName() const { return mSyncParentName; }
   bool IsTimeSynced();
   bool IsTimeSyncedTo(const OpenFrames::WindowProxy *windowProxy) const;
   void SetFOVy(double newFOVy);
   double GetFOVy() const;
   void NextView();
   void PreviousView();
   void SetCurrentView(unsigned int viewIndex);
   void ResetTrackBall();
   void SaveCurrentTrackBall();

   void Initialize(const OpenGLOptions &glOptions, const CoordinateOptions &frameOptions, const StarOptions &starOptions,
                   OpenFramesInterface *timeSyncParent, std::vector<SpacePointOptions> &spacePoints,
                   const std::string &interfaceName, std::vector<OpenFramesView *> &views, std::vector<OpenFramesVector *> &vectors,
                   std::vector<OpenFramesSensorMask *> &masks,
                   const CoordinateSystem *frame, Integer numberOfSolverTrajectories);
   void Reconfigure(const OpenGLOptions &glOptions, const CoordinateOptions &frameOptions, const StarOptions &starOptions,
                    OpenFramesInterface *timeSyncParent, std::vector<SpacePointOptions> &spacePoints,
                    const std::string &interfaceName, std::vector<OpenFramesView *> &views, std::vector<OpenFramesVector *> &vectors,
                    std::vector<OpenFramesSensorMask *> &masks,
                    const std::string &frameName, Integer numberOfSolverTrajectories);
   void UpdateSegmentViews(const std::string &spacecraft, const std::string &propagate);
   void UpdateTimeLimits(Real t, bool isSolving);
   void SetSegmentColor(const std::string &name, bool useDefaultColor, UnsignedInt color);
   void SetOrbitColor(const std::string &name, UnsignedInt color);
   void SetTargetColor(const std::string &name, UnsignedInt color);
   void SetTimeOffset(Real timeOffset);
   void SetTimeInWindowProxy(Real timeOffset);
   Real GetTimeOffsetFromWindowProxy();
   void TogglePlayback();

   Real GetTimeScaleFromWindowProxy();
   void SetTimeScaleToWindowProxy(Real newScaleFactor);

private:
   /// A tuple of a spacecraft name and a propagate command name for views
   class SegmentViewDef
   {
   public:
      std::string spacecraft;
      std::string propagate;
      OpenFramesView *definition;
   };

   /// data comprising a view
   class ViewDefinition
   {
   public:
      std::string name;
      std::string frameName;
      bool hasDefaultState;
      bool hasCurrentState;
      bool isOfTrajectory;
      osg::ref_ptr<OpenFrames::View> view;
      osg::Vec3d defaultEye;
      osg::Vec3d defaultCenter;
      osg::Vec3d defaultUp;
   };

   /// data comprising a vector
   class VectorDefinition
   {
   public:
      osg::ref_ptr<OpenFrames::ReferenceFrame> source;
      osg::ref_ptr<OpenFrames::ReferenceFrame> vector;
      
      bool isSameNameAs(const std::string& name) const
      {
        return (vector.valid() && vector->getName() == name);
      }

      const bool operator==(const VectorDefinition &rhs) const
      {
         return (source == rhs.source && vector == rhs.vector);
      }
      
      void add()
      {
         if(source.valid()) source->addChild(vector);
      }
      
      void remove()
      {
         if(source.valid()) source->removeChild(vector);
      }
   };
   
   /// data comprising a sensor mask
   class SensorMaskDefinition
   {
   public:
      osg::ref_ptr<OpenFrames::ReferenceFrame> source;
      osg::ref_ptr<OpenFrames::PolyhedralCone> mask;
      
      bool isSameNameAs(const std::string& name) const
      {
        return (mask.valid() && mask->getName() == name);
      }

      const bool operator==(const SensorMaskDefinition &rhs) const
      {
         return (source == rhs.source && mask == rhs.mask);
      }
      
      void add()
      {
         if(source.valid()) source->addChild(mask);
      }
      
      void remove()
      {
         if(source.valid()) source->removeChild(mask);
      }
   };

   /// The parent
   OFWindowProxyPanel *mParent;
   /// The canvas that everything is drawn on
   OFGLCanvas *mCanvas;
   /// The window proxy managed by this instance
   osg::ref_ptr<OpenFrames::WindowProxy> mWinProxy;
   /// The name of the parent to syncronize time with
   std::string mSyncParentName;
   /// The OpenGLOptions currently used in the canvas
   OpenGLOptions mGLOptions;

   /// Frame manager for WindowProxy
   osg::ref_ptr<OpenFrames::FrameManager> mFrameManager;
   /// Collection of spacepoint objects being plotted
   std::vector<osg::ref_ptr<OFSpaceObject>> mSpacecraft;
   /// Collection of celestial bodies being plotted
   std::vector<osg::ref_ptr<OFSpaceObject>> mBodies;
   /// Collection of ground stations being plotted
   std::vector<osg::ref_ptr<OFSpaceObject>> mGroundStations;
   /// Radial plane around the primary axis
   osg::ref_ptr<OpenFrames::RadialPlane> mRadialPlane;
   /// Radial plane around Earth's ecliptic
   osg::ref_ptr<OFEclipticPlane> mEclipticPlane;
   /// Center of the reference frame for the first cell of the grid
   osg::ref_ptr<OpenFrames::ReferenceFrame> mPrimaryRefFrame;
   /// Frame manager for WindowProxy
   osg::ref_ptr<OFStarListener> mStarListener;
   /// Axes object for first cell of grid
   osg::ref_ptr<OpenFrames::CoordinateAxes> mPrimaryAxes;
   /// Propagator segment view definitions
   std::vector<SegmentViewDef> mSegViewDefs;
   /// All of the views of the scene
   std::vector<ViewDefinition> mViews;
   /// All vectors in the scene
   std::vector<VectorDefinition> mVectors;
   /// All sensor masks in the scene
   std::vector<SensorMaskDefinition> mSensorMasks;
   
   /// Text on WindowProxy HUD
   osg::ref_ptr<osgText::Text> mHUDText_BottomLeft;

   /// Tracks the root coordinate frame name
   std::string mRootCoordinateFrameName;
   /// Tracks whether OpenFrames::FrameManager has been locked
   Integer mFrameManagerLockCount;
   /// The earliest time seen on any trajectory
   Real mEarliestTime;
   /// The latest time seen on any trajectory
   Real mLatestTime;

   /// The star settings currently in use
   StarOptions mStarOptions;

   void StartOpenFrames(const OpenGLOptions &options);
   void StopOpenFrames();
   void CreateWindowProxy(bool useVR);
   void ReleaseWindowProxy();

   Integer GetIndexOfCurrentView();

   void ClearViews();
   void ResetScene(bool frameIsBodyFixed, const std::string &bodyName);
   void SetReferenceFrame(const CoordinateOptions &frameOptions, const StarOptions &starOptions,
                          OpenFramesInterface *timeSyncParent, Integer numberOfSolverTrajectories);
   void SetObjects(std::vector<SpacePointOptions> &spacePoints, Integer numberOfSolverTrajectories);
   void SetVectors(std::vector<OpenFramesVector *> &vectors);
   void SetSensorMasks(std::vector<OpenFramesSensorMask *> &masks);
   void SetViews(const std::string &interfaceName, std::vector<OpenFramesView *> &views);
   std::string ProcessOpenFramesView(OpenFramesView &view, bool initialSetup, bool &valid);
   OpenFramesView *CreateViewForObject(const std::string &name);
   OpenFrames::ReferenceFrame *GetFrameOnTrajectoryByName(const std::string &objectName,
                                                          const std::string &propName = "");
   OpenFrames::ReferenceFrame *GetTrajectoryFrameByName(const std::string &objectName,
                                                        const std::string &propName = "");

   void ResumePlayback();
   void PausePlayback();
   void ResetPlayback(bool toTheEnd);
   void UpdateParentViewSelection();

   // Consolidate OpenGL lights from celestial bodies so that only the Sun emits light
   // Needed in case a celestial body uses an osgEarth model, which can include its own light
   void ConsolidateCelestialBodyLights();

   /// Reserve copy constructor
   OFScene(const OFScene &) = delete;
   /// Reserve assignment operator
   OFScene& operator=(const OFScene&) = delete;
};

#endif
