//$Id: OFWindowProxyPanel.hpp rmathur $
//------------------------------------------------------------------------------
//                                  OFWindowProxyPanel
//------------------------------------------------------------------------------
// OpenFramesInterface Plugin for GMAT (General Mission Analysis Tool)
//
// Copyright (c) 2022 Emergent Space Technologies, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Developed by Emergent Space Technologies, Inc. under contract number
// NNX16CG16C
//
// Author: Ravi Mathur, Emergent Space Technologies, Inc.
// Created: December 1, 2015
/**
 *  @class OFWindowProxyPanel
 *  A GMAT plugin panel that manages a canvas for OpenFrames and toolbar buttons
 *  for interacting with OpenFrames.
 */
//------------------------------------------------------------------------------

#ifndef OFWINDOWPROXYPANEL_hpp
#define OFWINDOWPROXYPANEL_hpp

#include "OpenFramesInterface_defs.hpp"
#include "PluginWidget.hpp"

class GmatBase;
class OFScene;
class OFGLCanvas;
class OpenGLOptions;
class OpenFramesInterface;

#include <QWidget>
#include <QPixmap>

class OpenFramesInterface_API OFWindowProxyPanel : public PluginWidget, public QWidget
{
public:
   /// Spacing between widgets on the toolbar
   static const int TOOL_BORDER_WIDTH = 2;

   // Text for tooltips
   static const std::string TOOLTIP_PLAY_ANIMATION;
   static const std::string TOOLTIP_PAUSE_ANIMATION;
   static const std::string TOOLTIP_PLAY_LINKED;
   static const std::string TOOLTIP_PAUSE_LINKED;
   static const std::string TOOLTIP_POSITIVE_SCALE;
   static const std::string TOOLTIP_NEGATIVE_SCALE;


   OFWindowProxyPanel(GmatBase *forObject, QWidget *parent);
   virtual ~OFWindowProxyPanel();

   /** QWidget interface that provides a widget size recommendation */
   QSize sizeHint() const override;

   /** Sets the size hint that will be provided */
   void setSizeHint(int width, int height);

   std::string GetForObjectName() const;

   //bool Destroy() override;
   bool Destroy();

   // Additional interfaces
   void SetObject(OpenFramesInterface *object);
   void RemoveObject(const OpenFramesInterface *object);
   void SetCanvas(OFGLCanvas *canvas, bool shaped);
   OFScene &GetScene();

   void ShowToolbar(bool show);
   void SetListOfViews(const QStringList &views);
   void AppendToListOfViews(const QString &view);
   void SetCurrentViewByIndex(int current);
   QString GetCurrentViewString();
   void SelectViewByString(const QString &view);
   void IncreaseScale();
   void DecreaseScale();
   void ToggleScaleSign();

private:
   static const QPixmap FORWARD_BITMAP;
   static const QPixmap BACK_BITMAP;

   /// The recommended size returned by sizeHint()
   QSize m_sizeHint;

   /// Stored Reference to the object that this panel was created for
   OpenFramesInterface *mForObject;
   /// The drawing canvas for this instance
   OFScene *mScene;
   /// The QWidget that contains the OFGLCanvas
   QWidget *mGLCanvasContainer;
   /// The time scale currently display in the text control
   Real mScale;
   /// The sign of the time scale
   int mScaleSign;
   /// True if the play/pause button state is play (UI in play mode)
   bool mIsPlaying;
   /// True if GUI is showing time sync, false otherwise
   bool mShowSyncedButtons;
   /// True if the current time scale is valid, false if the time scale is invalid (red)
   bool mScaleValid;

   Real UpByTwo(Real init);
   Real DownByTwo(Real init);

   void ResizeViewComboBox();

   enum
   {
      ID_PLAY_PAUSE = 15650,
      ID_SLOWER,
      ID_FASTER,
      ID_SCALE_SIGN,
      ID_SCALE_TEXT,
      ID_RESET_VIEW,
      ID_VIEW_SELECT,
      ID_TIME_SLIDER,
      ID_TIME_DILATOR,
   };
};

#endif