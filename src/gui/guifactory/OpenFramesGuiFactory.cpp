//$Id$
//------------------------------------------------------------------------------
//                           OpenFramesGuiFactory
//------------------------------------------------------------------------------
// OpenFramesInterface Plugin for GMAT (General Mission Analysis Tool)
//
// Copyright (c) 2022 Emergent Space Technologies, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Developed by Emergent Space Technologies, Inc. under contract number
// NNX16CG16C
//
// Author: Matthew Ruschmann, Emergent Space Technologies, Inc.
// Created: August 16, 2017
/**
 * Factory used for instantiating OpenFramesPlugin GUI elements
 */
//------------------------------------------------------------------------------

#include "OpenFramesGuiFactory.hpp"
#include "OFWindowProxyPanel.hpp"

// OFI configuration panel only applies to wxWidgets
#ifdef OFI_USE_WXWIDGETS
#include "OFConfigurationPanel.hpp"
#endif

/**
 * Constructor
 */
OpenFramesGuiFactory::OpenFramesGuiFactory()
{
#ifdef OFI_USE_WXWIDGETS
   creatables.push_back("OFConfigurationPanel");
#endif
   creatables.push_back("OFWindowProxyPanel");
}


/**
 * Destructor
 */
OpenFramesGuiFactory::~OpenFramesGuiFactory()
{
}


/**
 * Instruct the factory to create a widget
 *
 * @param ofType The type of widget to create
 * @param parent The parent for the widget
 * @param forObj The object associated with the UI element, either for
 *               configuration or for display
 */
GmatWidget* OpenFramesGuiFactory::CreateWidget(const std::string& ofType, void *parent,
      GmatBase* forObj)
{
   GmatWidget *retval = nullptr;

#ifdef OFI_USE_WXWIDGETS
   if (ofType == "OFConfigurationPanel")
   {
      OFConfigurationPanel *thePanel = new OFConfigurationPanel(forObj, (wxWindow*)parent);
      if (thePanel != nullptr)
      {
         retval = new GmatWidget(ofType, forObj);
         retval->SetWidget(thePanel, "Panel");
      }
   }
#endif

   if (ofType == "OFWindowProxyPanel")
   {
#ifdef OFI_USE_WXWIDGETS
      OFWindowProxyPanel *thePanel = new OFWindowProxyPanel(forObj, (wxWindow*)parent);
#elif OFI_USE_QT
      OFWindowProxyPanel *thePanel = new OFWindowProxyPanel(forObj, (QWidget*)parent);
#endif
      if (thePanel != nullptr)
      {
         GmatWidget *widget = new GmatWidget(ofType, forObj);
         widget->SetWidget(thePanel, "Panel");
         retval = widget;
      }
   }

   return retval;
}
