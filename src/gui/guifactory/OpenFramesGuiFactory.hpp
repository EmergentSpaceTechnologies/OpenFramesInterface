//------------------------------------------------------------------------------
//                           DemoGuiFactory
//------------------------------------------------------------------------------
// OpenFramesInterface Plugin for GMAT (General Mission Analysis Tool)
//
// Copyright (c) 2022 Emergent Space Technologies, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Developed by Emergent Space Technologies, Inc. under contract number
// NNX16CG16C
//
// Author: Matthew Ruschmann, Emergent Space Technologies, Inc.
// Created: August 16, 2017
/**
 *  @class GuiFactory
 *  Demonstration code for components accessed using the GUI plugin code
 *
 *  This code demonstrates GMAT GUI plugin components for the standard wxWidgets
 *  based GMAT GUI.
 */
//------------------------------------------------------------------------------

#ifndef OPENFRAMESGUIFACTORY_hpp
#define OPENFRAMESGUIFACTORY_hpp

#include "OpenFramesInterface_defs.hpp"
#include "GuiFactory.hpp"

class OpenFramesInterface_API OpenFramesGuiFactory : public GuiFactory
{
public:
   OpenFramesGuiFactory();
   virtual ~OpenFramesGuiFactory();

   virtual GmatWidget* CreateWidget(const std::string &ofType, void *parent,
                                    GmatBase *forObj = nullptr);

};

#endif
