# $Id$
# 
# Dependency management for projects using OpenFrames
# 
# Copyright (c) 2021 Emergent Space Technologies, Inc.
#
# Downloads and manages OpenFrames
#
# This approach uses FetchContent instead of ExternalProject because the latter
# does all work at build-time, which is incompatible with FIND_PACKAGE when
# later searching for OSG, OpenFrames, etc. In contrast, FetchContent downloads
# at configure-time.
# Build & install steps are also done immediately so that libraries will be
# found by FIND_PACKAGE during search.
#
# Original Author: Ravi Mathur
#
# DO NOT MODIFY THIS FILE UNLESS YOU KNOW WHAT YOU ARE DOING!

###############################################################################
# Download and setup OpenFrames
###############################################################################

SET(OPENFRAMES_SRC "${DEPENDENCY_PATH}/OpenFrames-git")
SET(OPENFRAMES_DIR "${OPENFRAMES_SRC}/installed" CACHE PATH "Path to OpenFrames install directory")

# Set OpenFrames build defaults
SET(OPENFRAMES_GITTAG "03764309")
SET(OPENFRAMESINTERFACE_OVERRIDE_OPENFRAMES_GITTAG "" CACHE STRING "(Optional) Override the default OpenFrames Git tag. Leave empty to use the default.")
IF(OPENFRAMESINTERFACE_OVERRIDE_OPENFRAMES_GITTAG)
  SET(OPENFRAMES_GITTAG ${OPENFRAMESINTERFACE_OVERRIDE_OPENFRAMES_GITTAG})
ENDIF()

# Enable OpenVR if available
IF(EXISTS "${OpenVR_ROOT_DIR}")
  SET(OF_VR_TYPE "OpenVR")
ELSE()
  SET(OF_VR_TYPE "OpenVR Stub")
ENDIF()

MESSAGE(STATUS "Setting up OpenFrames [${OPENFRAMES_GITTAG}]...")

# Get repo commit before downloading
GetGitCommit(${OPENFRAMES_SRC} COMMIT_BEFORE)

# Download OpenFrames
FetchContent_Populate(openframes
  GIT_REPOSITORY https://github.com/ravidavi/OpenFrames.git
  GIT_TAG ${OPENFRAMES_GITTAG}
  SOURCE_DIR ${OPENFRAMES_SRC}
  BINARY_DIR ${DEPENDENCY_BUILDPATH}/of
  SUBBUILD_DIR ${DEPENDENCY_BUILDPATH}/of-subbuild
  QUIET
)

# Get repo commit after downloading
GetGitCommit(${OPENFRAMES_SRC} COMMIT_AFTER)

# Update OpenFrames if repo changed or it hasn't been built yet
IF(NOT (COMMIT_BEFORE STREQUAL COMMIT_AFTER)
   OR NOT EXISTS ${OPENFRAMES_DIR})

  # Configure OpenFrames
  EXECUTE_PROCESS(
    COMMAND ${CMAKE_COMMAND}
      ${openframes_SOURCE_DIR}
      ${COMMON_BUILD_OPTIONS}
      -DOF_BUILD_DEMOS:BOOL=OFF
      -DOF_PYTHON_MODULE:BOOL=OFF # TODO: Reenable when ready for Python support
      -DOSG_DIR:PATH=${OSG_DIR}
  #    -DSWIG_EXECUTABLE:PATH=${SWIG_EXECUTABLE} # TODO: Reenable with Python module
      -DOPENVR_SDK_ROOT_DIR:PATH=${OpenVR_ROOT_DIR} # OpenFrames still uses the old OPENVR_SDK_ROOT_DIR variable
      -DOF_VR_TYPE:STRING=${OF_VR_TYPE}
      -DCMAKE_INSTALL_PREFIX:PATH=${OPENFRAMES_DIR}
    WORKING_DIRECTORY ${openframes_BINARY_DIR}
    OUTPUT_QUIET ERROR_QUIET
    OUTPUT_FILE ${DEPENDENCY_LOGPATH}/OpenFrames_configure.log
    ERROR_FILE ${DEPENDENCY_LOGPATH}/OpenFrames_configure.log
    RESULT_VARIABLE OPENFRAMES_CONFIGURE_RESULT
  )

  IF(NOT (OPENFRAMES_CONFIGURE_RESULT EQUAL 0))
    MESSAGE(FATAL_ERROR "OpenFrames could not be configured. See details in log file ${DEPENDENCY_LOGPATH}/OpenFrames_configure.log")
  ENDIF()

  EXECUTE_PROCESS(
    COMMAND ${CMAKE_COMMAND} --build . --target install --config Release ${UNIX_PARALLEL_OPTION}
    WORKING_DIRECTORY ${openframes_BINARY_DIR}
    OUTPUT_QUIET ERROR_QUIET
    OUTPUT_FILE ${DEPENDENCY_LOGPATH}/OpenFrames_build.log
    ERROR_FILE ${DEPENDENCY_LOGPATH}/OpenFrames_build.log
    RESULT_VARIABLE OPENFRAMES_BUILD_RESULT
  )

  IF(NOT (OPENFRAMES_BUILD_RESULT EQUAL 0))
    FILE(REMOVE_RECURSE "${OPENFRAMES_DIR}")
    MESSAGE(FATAL_ERROR "OpenFrames did not build. See details in log file ${DEPENDENCY_LOGPATH}/OpenFrames_build.log")
  ENDIF()

ENDIF()

###############################################################################
# Initialize CMake variables
###############################################################################

FIND_PATH(OPENFRAMES_INCLUDE_DIR
  NAMES OpenFrames/ReferenceFrame.hpp
  PATHS ${OPENFRAMES_DIR}/include
  DOC "Path to OpenFrames headers"
  NO_DEFAULT_PATH
)

FIND_LIBRARY(OPENFRAMES_LIBRARY
  NAMES OpenFrames
  PATHS
    ${OPENFRAMES_DIR}/lib
  DOC "Path to OpenFrames library"
  NO_DEFAULT_PATH
)

FIND_LIBRARY(OPENFRAMES_LIBRARY_DEBUG
  NAMES OpenFramesd
  PATHS
    ${OPENFRAMES_DIR}/lib
  DOC "Path to OpenFrames debug library"
  NO_DEFAULT_PATH
)

###############################################################################
# Install libraries
###############################################################################

INSTALL(DIRECTORY "${OPENFRAMES_DIR}/${DEPEND_LIB_DIR}/"
  DESTINATION ${DEPEND_INSTALL_DIR}
)
IF(OPENFRAMESINTERFACE_INSTALL_PREFIX)
  INSTALL(DIRECTORY "${OPENFRAMES_DIR}/${DEPEND_LIB_DIR}/"
    DESTINATION "${OPENFRAMESINTERFACE_INSTALL_PREFIX}/${DEPEND_INSTALL_DIR}"
  )
ENDIF()

# TODO: Reenable with Python support
#INSTALL(DIRECTORY "${OPENFRAMES_DIR}/bin/OFInterfaces"
#  DESTINATION bin
#)
