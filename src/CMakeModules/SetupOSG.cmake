# $Id$
# 
# Dependency management for projects using OSG
# 
# Copyright (c) 2021 Emergent Space Technologies, Inc.
#
# Downloads and manages OpenSceneGraph
#
# This approach uses FetchContent instead of ExternalProject because the latter
# does all work at build-time, which is incompatible with FIND_PACKAGE when
# later searching for OSG, OpenFrames, etc. In contrast, FetchContent downloads
# at configure-time.
# Build & install steps are also done immediately so that libraries will be
# found by FIND_PACKAGE during search.
#
# Original Author: Ravi Mathur
#
# DO NOT MODIFY THIS FILE UNLESS YOU KNOW WHAT YOU ARE DOING!

###############################################################################
# OpenSceneGraph dependencies
###############################################################################

IF(WIN32)
  # Download OSG 3rd-party dependencies on Windows
  MESSAGE(STATUS "Downloading OpenSceneGraph 3rd-Party Dependencies...")

  FetchContent_Populate(osg-3rdparty
    URL https://download.osgvisual.org/3rdParty_VS2017_v141_x64_V11_small.7z
    TIMEOUT 120
    SOURCE_DIR ${DEPENDENCY_PATH}/OSG_3rdParty_vc141
    BINARY_DIR ${DEPENDENCY_BUILDPATH}/osg-3rdparty
    SUBBUILD_DIR ${DEPENDENCY_BUILDPATH}/osg-3rdparty-subbuild
    QUIET
  )
  
  FIND_PATH(OSG_3RDPARTY_DIR bin/zlib.dll
    PATHS ${osg-3rdparty_SOURCE_DIR}
    PATH_SUFFIXES x64  # Also search this subdirectory (assuming 64-bit libraries)
    DOC "Path to OSG 3rd-Party libraries directory"
    NO_DEFAULT_PATH
  )
  
  # Allow other dependencies to use 3rd-party libs (e.g. cURL etc.)
  SET(CMAKE_PREFIX_PATH ${CMAKE_PREFIX_PATH} "${OSG_3RDPARTY_DIR}")
  
ELSEIF(APPLE)
  # Setup FreeType on OSX
  MESSAGE(STATUS "Setting up Freetype...")
  SET(FREETYPE_SRC "${DEPENDENCY_PATH}/FreeType")
  SET(FREETYPE_DIR "${FREETYPE_SRC}/installed")
  SET(ENV{FREETYPE_DIR} "${FREETYPE_DIR}") # OSG searches for freetype in this env var

  FetchContent_Populate(freetype
    URL https://sourceforge.net/projects/freetype/files/freetype2/2.10.1/freetype-2.10.1.tar.xz
    TIMEOUT 120
    SOURCE_DIR ${FREETYPE_SRC}
    BINARY_DIR ${DEPENDENCY_BUILDPATH}/freetype
    SUBBUILD_DIR ${DEPENDENCY_BUILDPATH}/freetype-subbuild
    QUIET
  )
  
  # Configure FreeType
  EXECUTE_PROCESS(
    COMMAND ${CMAKE_COMMAND}
      ${freetype_SOURCE_DIR}
      ${COMMON_BUILD_OPTIONS}
      -DCMAKE_DISABLE_FIND_PACKAGE_BZip2:BOOL=TRUE
      -DCMAKE_DISABLE_FIND_PACKAGE_ZLIB:BOOL=TRUE
      -DCMAKE_DISABLE_FIND_PACKAGE_PNG:BOOL=TRUE
      -DCMAKE_DISABLE_FIND_PACKAGE_HarfBuzz:BOOL=TRUE
      -DCMAKE_INSTALL_PREFIX:PATH=${FREETYPE_DIR}
    WORKING_DIRECTORY ${freetype_BINARY_DIR}
    OUTPUT_QUIET ERROR_QUIET
    OUTPUT_FILE ${DEPENDENCY_LOGPATH}/FreeType_configure.log
    ERROR_FILE ${DEPENDENCY_LOGPATH}/FreeType_configure.log
    RESULT_VARIABLE FREETYPE_CONFIGURE_RESULT
  )
  
  IF(NOT (FREETYPE_CONFIGURE_RESULT EQUAL 0))
    MESSAGE(FATAL_ERROR "Freetype could not be configured. See details in log file ${DEPENDENCY_LOGPATH}/FreeType_configure.log")
  ENDIF()

  # Build & install FreeType
  EXECUTE_PROCESS(
    COMMAND ${CMAKE_COMMAND} --build . --target install --config Release ${UNIX_PARALLEL_OPTION}
    WORKING_DIRECTORY ${freetype_BINARY_DIR}
    OUTPUT_QUIET ERROR_QUIET
    OUTPUT_FILE ${DEPENDENCY_LOGPATH}/FreeType_build.log
    ERROR_FILE ${DEPENDENCY_LOGPATH}/FreeType_build.log
    RESULT_VARIABLE FREETYPE_BUILD_RESULT
  )
  
  IF(NOT (FREETYPE_BUILD_RESULT EQUAL 0))
    MESSAGE(FATAL_ERROR "Freetype could not be built. See details in log file ${DEPENDENCY_LOGPATH}/FreeType_build.log")
  ENDIF()
ENDIF()

###############################################################################
# OpenSceneGraph
###############################################################################

# OpenSceneGraph setup defaults
SET(OSG_SRC "${DEPENDENCY_PATH}/OpenSceneGraph-git")
SET(OSG_DIR "${OSG_SRC}/installed" CACHE PATH "Path to OpenSceneGraph install directory")
SET(ENV{OSG_DIR} "${OSG_DIR}")  # Override existing environment variable
SET(ENV{OSG_ROOT} "${OSG_DIR}") # Override existing environment variable (for when CMP0074 defaults to NEW)
SET(OSG_GITTAG "a827840ba")
SET(OPENFRAMESINTERFACE_OVERRIDE_OSG_GITTAG "" CACHE STRING "(Optional) Override the default OSG Git tag. Leave empty to use the default.")
IF(OPENFRAMESINTERFACE_OVERRIDE_OSG_GITTAG)
  SET(OSG_GITTAG ${OPENFRAMESINTERFACE_OVERRIDE_OSG_GITTAG})
ENDIF()

# OS-specific build options
IF(WIN32)
  SET(OSG_CONFIG_OPTIONS
    -DACTUAL_3RDPARTY_DIR:PATH=${OSG_3RDPARTY_DIR}
    -DBUILD_OSG_PLUGIN_GIF:BOOL=ON
    -DBUILD_OSG_PLUGIN_JPEG:BOOL=ON
    -DBUILD_OSG_PLUGIN_PNG:BOOL=ON
    -DBUILD_OSG_PLUGIN_TIFF:BOOL=ON
    -DWIN32_USE_MP:BOOL=ON
  )
ELSEIF(APPLE)
  SET(OSG_CONFIG_OPTIONS
    -DCMAKE_MACOSX_RPATH:BOOL=ON
    -DBUILD_OSG_PLUGIN_IMAGEIO:BOOL=ON # ImageIO provides image loading (e.g. gif,jpeg,png,tiff)
    -DCMAKE_CXX_FLAGS:PATH=-DGL_SILENCE_DEPRECATION
  )
ELSE() # Linux
  SET(OSG_CONFIG_OPTIONS
    -DBUILD_OSG_PLUGIN_GIF:BOOL=ON
    -DBUILD_OSG_PLUGIN_JPEG:BOOL=ON
    -DBUILD_OSG_PLUGIN_PNG:BOOL=ON
    -DBUILD_OSG_PLUGIN_TIFF:BOOL=ON
  )
ENDIF()

MESSAGE(STATUS "Setting up OpenSceneGraph [${OSG_GITTAG}], this could take a while...")

# Get repo commit before downloading
GetGitCommit(${OSG_SRC} COMMIT_BEFORE)

# Download OpenSceneGraph
FetchContent_Populate(openscenegraph
  GIT_REPOSITORY https://github.com/openscenegraph/OpenSceneGraph.git
  GIT_TAG ${OSG_GITTAG}
  SOURCE_DIR ${OSG_SRC}
  BINARY_DIR ${DEPENDENCY_BUILDPATH}/osg
  SUBBUILD_DIR ${DEPENDENCY_BUILDPATH}/osg-subbuild
  QUIET
)

# Get repo commit after downloading
GetGitCommit(${OSG_SRC} COMMIT_AFTER)

# Update OpenSceneGraph if repo changed or it hasn't been built yet
IF(NOT (COMMIT_BEFORE STREQUAL COMMIT_AFTER)
   OR NOT EXISTS ${OSG_DIR})

  # Configure OpenSceneGraph
  EXECUTE_PROCESS(
    COMMAND ${CMAKE_COMMAND}
      ${openscenegraph_SOURCE_DIR}
      ${COMMON_BUILD_OPTIONS}
      ${OSG_CONFIG_OPTIONS}
      -DBUILD_OSG_APPLICATIONS:BOOL=ON
      -DBUILD_OSG_DEPRECATED_SERIALIZERS:BOOL=OFF
      -DBUILD_OSG_EXAMPLES:BOOL=OFF
      -DBUILD_OSG_PLUGINS_BY_DEFAULT:BOOL=OFF
        -DBUILD_OSG_PLUGIN_3DS:BOOL=ON
        -DBUILD_OSG_PLUGIN_BMP:BOOL=ON
        -DBUILD_OSG_PLUGIN_DXF:BOOL=ON
        -DBUILD_OSG_PLUGIN_FREETYPE:BOOL=ON
        -DBUILD_OSG_PLUGIN_LOGO:BOOL=ON
        -DBUILD_OSG_PLUGIN_LWO:BOOL=ON
        -DBUILD_OSG_PLUGIN_OBJ:BOOL=ON
        -DBUILD_OSG_PLUGIN_OSG:BOOL=ON # Needed for osgEarth cache support
        -DBUILD_OSG_PLUGIN_RGB:BOOL=ON
        -DBUILD_OSG_PLUGIN_SHP:BOOL=ON
        -DBUILD_OSG_PLUGIN_STL:BOOL=ON
      -DOSG_PLUGIN_SEARCH_INSTALL_DIR_FOR_PLUGINS:BOOL=OFF
      -DOSG_TEXT_USE_FONTCONFIG:BOOL=OFF
      -DCMAKE_INSTALL_PREFIX:PATH=${OSG_DIR}
    WORKING_DIRECTORY ${openscenegraph_BINARY_DIR}
    OUTPUT_QUIET ERROR_QUIET
    OUTPUT_FILE ${DEPENDENCY_LOGPATH}/OpenSceneGraph_configure.log
    ERROR_FILE ${DEPENDENCY_LOGPATH}/OpenSceneGraph_configure.log
    RESULT_VARIABLE OSG_CONFIGURE_RESULT
  )

  IF(NOT (OSG_CONFIGURE_RESULT EQUAL 0))
    MESSAGE(FATAL_ERROR "OpenSceneGraph could not be configured. See details in log file ${DEPENDENCY_LOGPATH}/OpenSceneGraph_configure.log")
  ENDIF()
    
  # Build & install OpenSceneGraph
  EXECUTE_PROCESS(
    COMMAND ${CMAKE_COMMAND} --build . --target install --config Release ${UNIX_PARALLEL_OPTION}
    WORKING_DIRECTORY ${openscenegraph_BINARY_DIR}
    OUTPUT_QUIET ERROR_QUIET
    OUTPUT_FILE ${DEPENDENCY_LOGPATH}/OpenSceneGraph_build.log
    ERROR_FILE ${DEPENDENCY_LOGPATH}/OpenSceneGraph_build.log
    RESULT_VARIABLE OSG_BUILD_RESULT
  )

  IF(NOT (OSG_BUILD_RESULT EQUAL 0))
    FILE(REMOVE_RECURSE "${OSG_DIR}")
    MESSAGE(FATAL_ERROR "OpenSceneGraph did not build. See details in log file ${DEPENDENCY_LOGPATH}/OpenSceneGraph_build.log")
  ENDIF()

ENDIF()

###############################################################################
# Initialize CMake variables
###############################################################################

FIND_PACKAGE(OpenSceneGraph 3.6.5 QUIET REQUIRED COMPONENTS osg osgDB osgGA osgText)

###############################################################################
# Install libraries
###############################################################################

GetLibDirPath("${OSG_DIR}/${DEPEND_LIB_DIR}" OSG_LIB_DIR)
INSTALL(DIRECTORY ${OSG_LIB_DIR}/
  DESTINATION ${DEPEND_INSTALL_DIR}
  PATTERN "pkgconfig" EXCLUDE
  PATTERN "osg*${CMAKE_EXECUTABLE_SUFFIX}" EXCLUDE   # We will install osg applications ourselves
  PATTERN "present3D*" EXCLUDE
)
IF(OPENFRAMESINTERFACE_INSTALL_PREFIX)
  INSTALL(DIRECTORY ${OSG_LIB_DIR}/
    DESTINATION "${OPENFRAMESINTERFACE_INSTALL_PREFIX}/${DEPEND_INSTALL_DIR}"
    PATTERN "pkgconfig" EXCLUDE
	PATTERN "osg*${CMAKE_EXECUTABLE_SUFFIX}" EXCLUDE
	PATTERN "present3D*" EXCLUDE
  )
ENDIF()

# Install osg applications
SET(OSG_APP_PATTERNS PATTERN "osgconv*" PATTERN "osgviewer*")
INSTALL(DIRECTORY ${OSG_DIR}/bin/
  DESTINATION bin
  USE_SOURCE_PERMISSIONS
  FILES_MATCHING ${OSG_APP_PATTERNS}
)
IF(OPENFRAMESINTERFACE_INSTALL_PREFIX)
  INSTALL(DIRECTORY ${OSG_DIR}/bin/
    DESTINATION "${OPENFRAMESINTERFACE_INSTALL_PREFIX}/bin"
    USE_SOURCE_PERMISSIONS
    FILES_MATCHING ${OSG_APP_PATTERNS}
  )
ENDIF()

# Install additional OSG 3rd-party libraries (only applies to Windows)
IF(WIN32)
  SET(OSG_3RDPARTY_LIBS
    "${OSG_3RDPARTY_DIR}/bin/libpng16.dll"    # Used by OSG PNG loader
    "${OSG_3RDPARTY_DIR}/bin/tiff.dll"        # Used by OSG TIFF loader
    "${OSG_3RDPARTY_DIR}/bin/zlib.dll"        # Used by tiff lib
  )
  
  INSTALL(FILES 
    ${OSG_3RDPARTY_LIBS}
    DESTINATION ${DEPEND_INSTALL_DIR}
  )
  IF(OPENFRAMESINTERFACE_INSTALL_PREFIX)
    INSTALL(FILES
      ${OSG_3RDPARTY_LIBS}
      DESTINATION "${OPENFRAMESINTERFACE_INSTALL_PREFIX}/${DEPEND_INSTALL_DIR}"
    )
  ENDIF()
ENDIF()
